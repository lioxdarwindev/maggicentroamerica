<?php
/**
 * Plugin Name: Pregunta
 */

function pregunta_init() {
    $plugin_dir = basename(dirname(__FILE__));
    load_plugin_textdomain( 'pregunta-plugin', false, $plugin_dir . '\\lang\\' );
}
add_action('plugins_loaded', 'pregunta_init');

function create_pregunta_postype() {

    $labels = array(
        'name' => __('Preguntas', 'pregunta-plugin'),
        'singular_name' => __('Pregunta', 'pregunta-plugin'),
        'add_new' => __('Nueva Pregunta', 'pregunta-plugin'),
        'add_new_item' => __('Agregar nueva Pregunta', 'pregunta-plugin'),
        'edit_item' => __('Editar Pregunta', 'pregunta-plugin'),
        'new_item' => __('Nueva Pregunta', 'pregunta-plugin'),
        'view_item' => __('Ver Pregunta', 'pregunta-plugin'),
        'search_items' => __('Buscar Preguntas', 'pregunta-plugin'),
        'not_found' =>  __('No existen Preguntas', 'pregunta-plugin'),
        'not_found_in_trash' => __('Ninguna Pregunta en Basurero', 'pregunta-plugin'),
        'parent_item_colon' => '',
    );

    $args = array(
        'label' => __('Preguntas', 'pregunta-plugin'),
        'labels' => $labels,
        'public' => false,
        'can_export' => true,
        'show_ui' => true,
        'menu_position'     => 32,
        '_builtin' => false,
        'capability_type' => 'post',
        'menu_icon'         => plugin_dir_url(__FILE__).'images/icon.png',
        'hierarchical' => false,
        'rewrite' => array( "slug" => "pregunta" ),
        'supports'=> array('title', 'editor', 'comments'),
        'show_in_nav_menus' => true
    );

    register_post_type( 'pregunta', $args);
}

add_action( 'init', 'create_pregunta_postype' );

function create_pregunta_tags() {

    $labels = array(
        'name'              => __( 'Ask Tags' ),
        'singular_name'     => __( 'Ask Tag'),
        'search_items'      => __( 'Search Ask Tags' ),
        'all_items'         => __( 'All Ask Tags' ),
        'parent_item'       => __( 'Parent Ask Tag' ),
        'parent_item_colon' => __( 'Parent Ask Tag:' ),
        'edit_item'         => __( 'Edit Ask Tag' ),
        'update_item'       => __( 'Update Ask Tag' ),
        'add_new_item'      => __( 'Add New Ask Tag' ),
        'new_item_name'     => __( 'New Ask Tag Name' ),
        'menu_name'         => __( 'Ask Tags' ),
    );

    $args = array(
        'hierarchical'      => false,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'asktag' ),
    );

    register_taxonomy( 'asktag', 'pregunta', $args );

}

add_action('init', 'create_pregunta_tags');

function pregunta_title_placeholder( $title ){

    $screen = get_current_screen();

    if ( 'pregunta' == $screen->post_type ){
        $title = __('your question here', 'pregunta-plugin');
    }

    return $title;
}

add_filter( 'enter_title_here', 'pregunta_title_placeholder' );

function show_asks(  ) {


    $isCaptcha = get_option('pregunta_setting_captcha');

    if($isCaptcha)
    {
        require_once(dirname(__FILE__).'/captcha/captcha.php');

        $publickey = get_option( 'pregunta_setting_captcha_publickey' );
        $privatekey = get_option( 'pregunta_setting_captcha_privatekey' );
        $resp = null;
        $error = null;
    }

    ob_start();

    wp_enqueue_style( 'pregunta', plugins_url('pregunta.css?v=1.1',__FILE__) );

    if( 'POST' == $_SERVER['REQUEST_METHOD']
        && !empty( $_POST['action'] )
        && $_POST['post_type'] == 'pregunta' && $_POST['question'] != "")
    {
        if ($isCaptcha && $_POST["recaptcha_response_field"])
        {
            $resp = recaptcha_check_answer ($privatekey,
                $_SERVER["REMOTE_ADDR"],
                $_POST["recaptcha_challenge_field"],
                $_POST["recaptcha_response_field"]);

            if ($resp->is_valid) {

                $title =  $_POST['question'];

                $post = array(
                    'post_title'	=> $title,
                    'post_status'	=> 'draft',
                    'post_type'		=> 'pregunta'
                );

                $id = wp_insert_post($post);

                //echo "<div class='alert success'>".__('<b>Muchas Gracias!</b> La pregunta ha sido enviada para revisión.')."</div>";

                if(isset($_POST['receta_name']))
                {

                    add_post_meta($id, 'pregunta_receta_name', $_POST['receta_name']);
                }

                if(isset($_POST['receta_id']))
                {
                    add_post_meta($id, 'pregunta_receta_id', $_POST['receta_id']);
                }
                if(isset($_POST['username']))
                {
                    add_post_meta($id, 'pregunta_username', $_POST['username']);
                }
                if(isset($_POST['email']))
                {
                    add_post_meta($id, 'pregunta_email', $_POST['email']);
                }

                if(get_option('pregunta_setting_email') == true)
                {
                    $mailtext = __('New Ask Me Received', 'pregunta-plugin');

                    $admin_email = get_option('admin_email');
                    wp_mail( $admin_email,  $mailtext, "Ask Me: ".$title);
                }

            }
            else
            {
                $error = $resp->error;
                //echo "<div class='alert danger'>".__('<b>Error!</b> Captcha incorrecta.')."</div>";
            }
        }
        else if(!$isCaptcha)
        {
            $title =  $_POST['question'];

            $post = array(
                'post_title'	=> $title,
                'post_status'	=> 'draft',
                'post_type'		=> 'pregunta'
            );

            $id = wp_insert_post($post);

          //  echo "<div class='alert success'>".__('<b>Muchas Gracias!</b> La pregunta ha sido enviada para revisión.')."</div>";

            if(isset($_POST['username']))
            {
                add_post_meta($id, 'pregunta_username', $_POST['username']);
            }
            if(isset($_POST['email']))
            {
                add_post_meta($id, 'pregunta_email', $_POST['email']);
            }

            if(isset($_POST['receta_name']))
            {
                add_post_meta($id, 'pregunta_receta_name', $_POST['receta_name']);
            }
            if(isset($_POST['receta_id']))
            {
                add_post_meta($id, 'pregunta_receta_id', $_POST['receta_id']);
            }

            if(get_option('pregunta_setting_email') == true)
            {
                $mailtext = __('New Ask Me Received', 'pregunta-plugin');

                $admin_email = get_option('admin_email');
                wp_mail( $admin_email,  $mailtext, "Ask Me: ".$title);
            }
        }
        else
        {
         //   echo "<div class='alert danger'>".__('<b>Error!</b> debes llenar el Captcha.')."</div>";
        }
    }
    else if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['question'] == "")
    {
      //  echo "<div class='alert danger'>".__('<b>Error!</b> Debes ingresar una pregunta.')."</div>";
    }
    ?>

    <div id="pregunta">
        <form id="newask" name="newask" method="post" action="">


            <h3 id="reply-title">Deja una Pregunta</h3>

            <label for="qt_username" id="usernameLabel"><?php _e('Nombre', 'pregunta-plugin'); ?></label><br />
            <input type="text" id="qt_username" value="" tabindex="1" size="20" name="username" required />
            
            <label for="qt_email" id="emailLabel"><?php _e('Correo elctrónico', 'pregunta-plugin'); ?></label><br />
            <input type="email" id="qt_email" value="" tabindex="1" size="20" name="email" required />

            <label for="question" id="questionLabel"><?php _e('Pregunta:', 'pregunta-plugin'); ?></label><br />
            <input type="text" id="question" value="" tabindex="1" size="20" name="question" required />

            <?php
            if(get_option('pregunta_setting_user_response') == true)
            {
                echo display_userdatafields();
            }
            ?>

            <p><input type="submit" value="Enviar" tabindex="6" id="submit" name="submit" /></p>
            <div id='respuesta'></div>
            <input type="hidden" name="post_type" id="post_type" value="pregunta" />
            <input type="hidden" name="action" value="post" />
            <input type="hidden" name="receta_id" id="receta_id" value="<?php the_ID(); ?>" />
            <input type="hidden" name="receta_name" id="receta_name" value="<?php the_title();?>" />
          
            <?php wp_nonce_field( 'new-post' ); ?>
            <?php
            if($isCaptcha)
            {
                ?>
            <div id="captcha-div">
            <?php echo recaptcha_get_html($publickey, $error); ?>
            </div>
            <script>
                jQuery( "#question" ).focus(function() {
                    if ( jQuery( "#captcha-div" ).is( ":hidden" ) ) {
                        jQuery( "#captcha-div" ).slideDown( "slow" );
                    }
                });
            </script>
            <?php } ?>
        </form>

    <?php
    pregunta_output_normal();
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
add_shortcode('pregunta', 'show_asks');

function display_userdatafields()
{
    ob_start(); ?>

    <div id="userdatafields">
        <div id="usernameDiv">
            <div id="usernamelabel"><b><label for="username"><?php _e('Name', 'pregunta-plugin'); ?></label></b></div>
            <div id="usernameinput"><input type="text" id="username" value="" tabindex="1" size="20" name="username" /></div>
        </div>
        <div id="useremailDiv">
            <div id="useremaillabel"><b><label for="email"><?php _e('Email', 'pregunta-plugin'); ?></label></b></div>
            <div id="useremailinput"><input type="email" id="email" value="" tabindex="1" size="20" name="email" /></div>
            <div id="emailmsg"><?php _e('If you provide an Email you will receive a message, once your Question is Answered.', 'pregunta-plugin'); ?></div>
        </div>
    </div>
    <div id="extenduserdata"><button type="button"><?php _e("Advanced Options", "pregunta-plugin"); ?></button></div>
    <script>
		jQuery( "#extenduserdata" ).click(function() {
			if ( jQuery( "#userdatafields" ).is( ":hidden" ) ) {
				jQuery( "#userdatafields" ).slideDown( "slow" );
			} else {
				jQuery( "#userdatafields" ).slideUp();
			}
		});
	</script>

    <?php $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function pregunta_output_normal()
{
		global $wp_query;
		
        if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
        } else if ( get_query_var('page') ) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        $this_post_id = get_the_ID();
        $args = array(
            'post_type' => 'pregunta',
            'post_status' => 'publish',
            'paged' => $paged,
            'orderby' => 'date',
            'posts_per_page' => get_option( 'pregunta_setting_number_preguntas', 5 )
        );

        query_posts($args);

        while ( have_posts() ) : the_post(); 
          $customs = get_post_custom(get_the_ID());
          $receta_id = $customs["pregunta_receta_id"];


          if($this_post_id == $receta_id[0] ){
          ?>
            <div class="entry">
                <div class="question">
                    <strong><?php the_title(); ?></strong>
                    <?php
						$customs = get_post_custom(get_the_ID());
						$username = ($customs['pregunta_username'][0]);
						if ($username) {
							echo ' from ' . $username;	
						}
					?>
                    <span class="date">
                        <?php
                        $allterms = get_the_terms(get_the_ID(), "asktag");

                        if(!empty($allterms))
                        {
                            $i = 0;
                            foreach($allterms as $term)
                            {
                                echo "<strong>" . $term->name . "</strong>";
                                $i++;
                                if($i != count($allterms))
                                {
                                    echo ", ";
                                }
                            }
                            echo " - ";
                        }

                        ?>
                        <?php the_time('j. F Y'); ?>
                    </span>
                </div>

                <div class="answer">
                    <p><?php the_content(); ?></p>
                </div>
            </div>

            <hr />

        <?php } endwhile; ?>
        <?php pregunta_pagination($wp_query->max_num_pages); ?>
    </div> <!-- Ende pregunta Div -->
    <?php
    wp_reset_query();
}

function add_pregunta_columns($pregunta_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['date'] = __('Fecha', 'pregunta-plugin');
    $new_columns['title'] = __('Preguntar', 'pregunta-plugin');
    $new_columns['answer'] = __('Respuesta', 'pregunta-plugin');
    $new_columns['username'] = __('Username', 'pregunta-plugin');
    $new_columns['receta_name'] = __('Nombre Receta', 'pregunta-plugin');
    $new_columns['receta_id'] = __('Id Receta', 'pregunta-plugin');
    $new_columns['email'] = __('Email', 'pregunta-plugin');

    return $new_columns;
}

add_filter('manage_edit-pregunta_columns', 'add_pregunta_columns');

add_action('manage_pregunta_posts_custom_column', 'manage_pregunta_columns', 10, 2);

function manage_pregunta_columns($column_name, $id) {
    $customs = get_post_custom($id);

    switch ($column_name) {
        case 'id':
            echo $id;
            break;
        case 'username':
            if(isset($customs['pregunta_username']))
            {
                foreach( $customs['pregunta_username'] as $key => $value)
                    echo $value;
            }
            break;
        case 'receta_id':
            if(isset($customs['pregunta_receta_id']))
            {
                foreach( $customs['pregunta_receta_id'] as $key => $value)
                    echo $value;
            }
            break;    

            case 'receta_name':
      
            if(isset($customs['pregunta_receta_name']))
            {

                foreach( $customs['pregunta_receta_name'] as $key => $value)
                    echo $value;
            }
            break;    
        case 'email':
            if(isset($customs['pregunta_email']))
            {
                foreach( $customs['pregunta_email'] as $key => $value)
                    echo $value;
            }
            break;
        case 'answer':
            echo get_the_content($id);
            break;
        default:
            break;
    }
}



function pregunta_pagination($pages = '', $range = 5)
{
    $showitems = ($range * 2)+1;

    global $paged;

    if ( get_query_var('paged') ) {
        $paged = get_query_var('paged');
    } else if ( get_query_var('page') ) {
        $paged = get_query_var('page');
    } else {
        $paged = 1;
    }

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;

        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        ?>
        <div class="pregunta_pagination"><span><?php echo __('Page', 'pregunta-plugin'); ?> <?php echo $paged; ?> <?php echo __('of', 'pregunta-plugin');?> <?php echo $pages; ?></span>
            <?php
            if($paged > 2 && $paged > $range+1 && $showitems < $pages)
            { ?>

                <a href="<?php echo get_pagenum_link(1); ?>">&laquo;</a>

            <?php }

            if($paged > 1)
            { ?>
                <a href="<?php echo get_pagenum_link($paged - 1); ?>">&lsaquo;</a>;
            <?php }

            for ($i=1; $i <= $pages; $i++)
            {
                if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
                {
                    if ($paged == $i)
                    { ?>
                        <span class="current"><?php echo $i; ?></span>
                    <?php }
                    else
                    { ?>
                        <a href="<?php echo get_pagenum_link($i); ?>" class="inactive"><?php echo $i; ?></a>
                    <?php }
                }
            }

            if ($paged < $pages)
            { ?>
                <a href="<?php echo get_pagenum_link($paged + 1); ?>">&rsaquo;</a>
            <?php }
            if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages)
            { ?>
                <a href="<?php echo get_pagenum_link($pages); ?>">&raquo;</a>
            <?php }
            ?>
        </div>
        <?php
    }
}

function pregunta_stats() {
?>
    <h4>Preguntas</h4>
    <br />
    <ul>
	    <li class="post-count">
            <?php
            $type = 'pregunta';
            $args = array(
                'post_type' => $type,
                'post_status' => 'publish',
                'posts_per_page' => -1);

            $my_query = query_posts( $args );
            ?>

            <a href="edit.php?post_type=pregunta&post_status=publish"><?php echo count($my_query); ?> <?php _e('published', 'pregunta-plugin'); ?></a>
        </li>
        <li class="page-count">
            <?php
            $args = array(
                'post_type' => $type,
                'post_status' => 'draft',
                'posts_per_page' => -1);

            $my_query = query_posts( $args );
            ?>
            <a href="edit.php?post_type=pregunta&post_status=draft"><?php echo count($my_query); ?> <?php _e('open', 'pregunta-plugin'); ?></a>

        </li>
    </ul>
<?php
    wp_reset_query();
}

add_action('activity_box_end', 'pregunta_stats');

function pregunta_settings_init() {

    add_settings_section(
        'pregunta_setting_section',
        __('Settings de Preguntas', 'pregunta-plugin'),
        'pregunta_setting_section_callback',
        'reading'
    );

 	add_settings_field(
        'pregunta_setting_email',
        __('Preguntar por Correo', 'pregunta-plugin'),
        'pregunta_setting_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_email' );

    add_settings_field(
        'pregunta_setting_captcha',
        __('Usar Captcha', 'pregunta-plugin'),
        'pregunta_captcha_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_captcha' );

    add_settings_field(
        'pregunta_setting_captcha_publickey',
        __('Captcha Public Key', 'pregunta-plugin'),
        'pregunta_captcha_puk_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_captcha_publickey' );

    add_settings_field(
        'pregunta_setting_captcha_privatekey',
        __('Captcha Private Key', 'pregunta-plugin'),
        'pregunta_captcha_prk_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_captcha_privatekey' );

    add_settings_field(
        'pregunta_setting_number_preguntas',
        __('Numero de preguntas', 'pregunta-plugin'),
        'pregunta_number_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_number_preguntas' );

    add_settings_field(
        'pregunta_setting_user_response',
        __('Campos de Usuario', 'pregunta-plugin'),
        'pregunta_setting_user_response_callback',
        'reading',
        'pregunta_setting_section'
    );

    register_setting( 'reading', 'pregunta_setting_user_response' );
 }

 add_action( 'admin_init', 'pregunta_settings_init' );

function pregunta_setting_section_callback() {
     echo '<p>'.__("Configurar Preguntas", "pregunta-plugin").'</p>';
}

function pregunta_setting_callback() {
    echo '<input name="pregunta_setting_email" id="gv_thumbnails_insert_into_excerpt" type="checkbox" value="1" class="code" ' . checked( 1, get_option( 'pregunta_setting_email' ), false ) . ' />';
}

function pregunta_captcha_callback() {
    echo '<input name="pregunta_setting_captcha" type="checkbox" value="1" class="code" ' . checked( 1, get_option( 'pregunta_setting_captcha' ), false ) . ' />';
}

function pregunta_setting_user_response_callback() {
    echo '<input name="pregunta_setting_user_response" id="gv_thumbnails_insert_into_excerpt" type="checkbox" value="1" class="code" ' . checked( 1, get_option( 'pregunta_setting_user_response' ), false ) . ' />' . __('Preguntar por campos de Username y Correo ?.', 'pregunta-plugin');
}

function pregunta_captcha_prk_callback() {
    echo '<input name="pregunta_setting_captcha_privatekey" id="gv_thumbnails_insert_into_excerpt" type="text" class="code" value="' . get_option( 'pregunta_setting_captcha_privatekey' ) . '" />
        <p class="description">' . __('Get a key from <a href="https://www.google.com/recaptcha/admin/create" target="_blank">https://www.google.com/recaptcha/admin/create</a>', 'pregunta-plugin') . "</p>";
}

function pregunta_captcha_puk_callback() {
    echo '<input name="pregunta_setting_captcha_publickey" id="gv_thumbnails_insert_into_excerpt" type="text" class="code" value="' . get_option( 'pregunta_setting_captcha_publickey' ) . '" />
        <p class="description">' . __('Get a key from <a href="https://www.google.com/recaptcha/admin/create" target="_blank">https://www.google.com/recaptcha/admin/create</a>', 'pregunta-plugin') . "</p>";
}

function pregunta_number_callback() {
    echo '<input name="pregunta_setting_number_preguntas" id="gv_thumbnails_insert_into_excerpt" type="text" class="code" value="' . get_option( 'pregunta_setting_number_preguntas', 5 ) . '" />
        <p class="description">' . __('Cuantas preguntas por pagina.', 'pregunta-plugin') . "</p>";
}

add_action( 'admin_menu', 'add_user_menu_bubble' );

function add_user_menu_bubble() {

    global $menu;

    foreach ( $menu as $key => $value ) {
        if ( $menu[$key][2] == 'edit.php?post_type=pregunta' ) {

            $type = 'pregunta';
            $args = array(
                'post_type' => $type,
                'post_status' => 'draft',
                'posts_per_page' => -1);

            $my_query = query_posts( $args );
            if(count($my_query) > 0)
            {
                $menu[$key][0] .= '    <span class="update-plugins"><span class="plugin-count">' . count($my_query) . '</span></span> ';
            }
            wp_reset_query();
            return;
        }
    }

}

function publish_pregunta_hook($id)
{
    $customs = get_post_custom($id);
    if(isset($customs['pregunta_email']))
        wp_mail( $customs['pregunta_email'],  get_bloginfo('name').__(' - Pregunta - Respuesta Recibida', 'pregunta-plugin'), __('Tu pregunta ha sido Respondida!', 'pregunta-plugin'));
}

add_action( 'publish_pregunta', 'publish_pregunta_hook' );
?>