$document.ready(function($) {
    $document.on('submit', '.login_form', function(event) {
        event.preventDefault();
        var $this = $(this),
            google_recaptcha = zMAjaxLoginRegister.recaptcha_check_login($this),
            serialized_form = $this.serialize(),
            form_fields = 'input[type="password"], input[type="text"], input[type="email"], input[type="checkbox"], input[type="submit"]',
            data = {
                action: 'login_submit',
                security: $this.data('zm_alr_login_security')
            };
        $this.find(form_fields).attr('disabled', 'disabled');
        $.ajax({
            global: false,
            data: "action=login_submit&" + serialized_form + "&security=" + $this.data('zm_alr_login_security') + "&" + google_recaptcha,
            type: "POST",
            url: _zm_alr_settings.ajaxurl,
            success: function(msg) {
                if (jQuery.type(msg) == "string") {
                    msg = {
                        description: "Haz excedido el numero de intentos fallidos, intenta mas tarde.",
                        cssClass: "error-container",
                        code: "show_notice",
                        redirect_url: null
                    };
                }
                ajax_login_register_show_message($this, msg);
                $this.find(form_fields).removeAttr('disabled');
                if (msg.redirect_url != null) {
                    location.reload();
                }
            }
        });
    });
    $document.on('click', '.fb-login', function(event) {
        event.preventDefault();
        var $this = $(this);
        var $form_obj = $this.parents('form');
        FB.login(function(response) {
            var requested_scopes = ['public_profile', 'email'];
            var response_scopes = $.map(response.authResponse.grantedScopes.split(","), $.trim);
            var diff = $(requested_scopes).not(response_scopes).get();
            var granted_access = diff.length;
            if (!granted_access) {
                FB.api('/me', function(response) {
                    var fb_response = response;
                    $.ajax({
                        data: {
                            action: "facebook_login",
                            fb_response: fb_response,
                            security: $this.data('zm_alr_facebook_security')
                        },
                        global: false,
                        type: "POST",
                        url: _zm_alr_settings.ajaxurl,
                        success: function(msg) {
                            ajax_login_register_show_message($form_obj, msg);
                            zMAjaxLoginRegister.reload(msg.redirect_url);
                        }
                    });
                });
            } else {
                console.log('User canceled login or did not fully authorize.');
            }
        }, {
            scope: 'email',
            return_scopes: true
        });
    });
    if (_zm_alr_settings.login_handle.length) {
        if ($('body.logged-in').length) {
            $this = $(_zm_alr_settings.login_handle).children('a');
            $this.html(_zm_alr_settings.logout_text);
            $this.attr('href', _zm_alr_settings.wp_logout_url);
        } else {
            $document.on('click', _zm_alr_settings.login_handle, function(event) {
                event.preventDefault();
                zMAjaxLoginRegister.open_login();
                if (_zm_alr_settings.pre_load_forms === 'zm_alr_misc_pre_load_no') {
                    zMAjaxLoginRegister.load_login();
                }
            });
        }
    }
    $document.on('click', '.not-a-member-handle', function(e) {
        e.preventDefault();
        $('#ajax-login-register-login-dialog').dialog('close');
        zMAjaxLoginRegister.open_register();
        if (_zm_alr_settings.pre_load_forms === 'zm_alr_misc_pre_load_no') {
            zMAjaxLoginRegister.load_register();
        }
    });
});