	
				<?php
					//global $wp_query;
				if($tipo){
					if($tipo != "blog"){
						$tipo = trim($wp_query->query_vars['tipo']);
						wp_reset_query();
						$args = array(
								           'DisableFilterPais' => true,
									   'post_type' => 'blog',
									   'post_status' => 'publish',
									   'posts_per_page' => 4,
									    'tax_query' => array(
									
									        array(
									            'taxonomy' => 'pais',
									            'field' => 'slug',
									            'terms' => $_SESSION['pais']
									        ),
									         array(
									            'taxonomy' => 'categoriaBlog',
									            'field' => 'slug',
									            'terms' => $tipo
									        )
									    )
									);


									   $custom = new WP_Query( $args );

					}

				}

					
					
				
				
	
				// Start the Loop
				$catId = $wp_query->query_vars["cat"];
				$recIndex = 0;
				$arrIngredientes = array();

				if(!$custom){
                    wp_reset_query();
					if(is_category($queried_object)){
						if(!is_search()){
				        	$args = array('cat' => $wp_query->get_queried_object_id(), 'post_status' => 'publish');
						}
				    	else{
				    		$args = array('cat' => $wp_query->get_queried_object_id(), 'post_status' => 'publish', 's' => get_search_query());
				    	}
					}else{
						if(!is_search()){
							$args = array('category__not_in' => array(1, 390, 391, 392, 393), 'post_status' => 'publish');
						}
						else{
							$args = array('category__not_in' => array(1, 390, 391, 392, 393), 'post_status' => 'publish', 's' => get_search_query());
						}
					}
					$custom = new WP_Query( $args );
				}
				echo('<span class="mensaje">No se encontraron resultados.</span>');
				while ( $custom->have_posts() ) : $custom->the_post();

				$terms = wp_get_post_terms($post->ID,array("category","pais","personalidad","momento","course"));
				$categoria = wp_get_post_terms($post->ID,array("category"));
				$termslugs = '';
				$parent ='';

				//buscar categoris padre
				foreach($terms as $term) {

					
				    // climb up the hierarchy until we reach a term with parent = '0'
				   
				    if($term->parent != 0){
				    	$parent  = get_term_by( 'id',$term->parent, "category");

				    }
				
				}
				if($parent){

					if ( !in_array( $parent , $terms ) ) {
						array_push($terms,$parent); // agregar Padre de la categoria si no se encuentra
				    	 	
				   	}
				    	
				}
				
				
				foreach($terms as $term) {
					
					$termslugs .= " " . preg_replace('/\s+/', '',$term->slug);
					 $content = get_the_content();
				
					 if($term->parent == $catId){
					 	$catTitle = $term->name;

					 }
					 //term_is_ancestor_of($catId)
					 
					 		
				}



				//global $wp_query;
					
					
					$recipe = new WPURP_Recipe($post->ID);
					
			
					foreach( $recipe->ingredients() as $ingredient ) {
						$term =  get_term( $ingredient["ingredient_id"],"ingredient");
					
						// var_dump(WPURP_Taxonomy_MetaData::get( 'ingredient', $term->slug, 'group' ));

						  $termP = get_term_by( 'id', $term->parent, 'ingredient');

                    	$slug =  WPURP_Taxonomy_MetaData::get( 'ingredient', $term->slug, 'group' );
                    	
						
                    	if($slug){
							 $name =  $term->name;
                    		 $termslugs .= " " .trim($slug) ;
                    	}
                       
                        // logica para dejar ingredientes en filtros de recetas visibles
                       $arrIngredientes[$slug]["nombre"] = [$name];
                       $arrIngredientes[$slug]["slug"] = [$slug];
                   	   $arrIngredientes[$slug]["ids"][$recipe->ID()] = 1;
                      

                	}
                
					
					$fav = new WPURP_Favorite_Recipes();
  				
					//userphoto(get_user_by('id', $post->post_author)); //la imagen
				//userphoto_thumbnail(get_user_by('id', $post->post_author));// el thumb
				if($tipo){
                    $author_id=$post->post_author;
                echo('<a rel ="'.$post->ID.'" class="grid-item mix" href="'.get_permalink($post).'"><div class="marco"><img class="imagenTemp" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="'.get_field('thumbnail',$post->ID)["sizes"]["thumbBig"].'" /></div><span> <p class="termName"></p><p class="tituloRes">'.get_the_title( $post->ID ).'</p>
                    <div class="views_info"><span class="views">'.the_views(false).'</span><span class="fabs">'.$fav->cantidad_favorites($post->ID).'</span>
                    </div>
                    </span><div class="info"><div class="Aut_info"><p class="author_name">'.get_field('autor_2', $post->ID).'</p><p class="fecha">'. get_the_date().'</p></div>
            </div></a>');
                }else{
                 echo('<a rel ="'.$recipe->ID().'" class="grid-item mix '.$termslugs.'" href="'.get_permalink($post).'" data-fav="'.$fav->cantidad_favorites($recipe->ID()).'" data-reciente="'.get_the_date('Ymd').'"><div class="marco"><span class="overl"></span><img class="imagenTemp" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="'.$recipe->alternate_image_url("thumbBig").'" /></div><span> <p class="termName">'.$categoria[0]->name.'</p><p class="tituloRes">'.$recipe->title().'</p>
                    <div class="views_info"><span class="views">'.the_views(false).'</span><span class="fabs">'.$fav->cantidad_favorites($recipe->ID()).'</span><span class="shares">'.pssc_all($recipe->ID()).'</span>
                    </div>
                    </span><div class="info"><div class="Aut_info"><p class="author_name">'.get_field('autor_2', $post->ID).'</p><p class="fecha">'. get_the_date().'</p></div><div class="tiempo pure-u-md-4-24"><img src="'.get_bloginfo('template_directory').'/images/icons/clock.svg">    
            <p class="t">'.$recipe->cook_time().'</p><p>min</p></div>
            </div></a>');
                }
				 	$recIndex ++;

	
				endwhile;


				//print_r($arrIngredientes);
				?>
				<script type="text/javascript">
				// pass PHP variable declared above to JavaScript variable
				var arIng = <?php echo json_encode($arrIngredientes) ?>;
				</script>