<?php
/**
 * Template Name: Promociones
 *
 */
 get_header(); ?>

<div id="Container" class="content-area">
	<div id="main">
		<div class="pure-g">
		<?php
		$args = array( 'post_type' => 'promociones', 'posts_per_page' => 4 );
		$loop = new WP_Query( $args );
		$cont = 0;
						// Start the Loop.
		while ( $loop->have_posts() ) : $loop->the_post();
		if($cont == 0){
			echo("<div class='pure-u-1-1 pure-u-md-16-24 imagen-promo-prin'><img src='".get_field( 'imagen_principal',get_the_ID())["sizes"]["imagen-producto"]."'/></div>"); 

	
		
			?>
			<div class='pure-u-1-1 pure-u-md-8-24 contenidoPromo'>
			<?php the_content ();?>
			
				<?php if( get_field('link_promo') ): ?>
					<a href="<?php echo(get_field( 'link_promo',get_the_ID()));?>" target="_blank" class="btnurlpromo"><?php the_field('link_promo_text'); ?></a>
				<?php endif; ?>
				<?php if( get_field('reglamento') ): ?>
					<a href="<?php echo(get_field( 'reglamento',get_the_ID()));?>" target="_blank" class="reglamento"><?php the_field('reglamento_text'); ?></a>		
				<?php endif; ?>

			</div>

			<?php
			
		}else{

			echo("<a  href='".get_permalink()."'' class='pure-u-1-1 pure-u-md-1-3 subpromo sec'><img src='".get_field( 'imagen_thumbnail',get_the_ID())["sizes"]["thumbBig"]."'/></a>"); 

	
		
			?>
			
			<?php 
		}
		$cont   ;
		endwhile;
		?>
		</div>
	</div>
</div>



<?php get_footer(); ?>