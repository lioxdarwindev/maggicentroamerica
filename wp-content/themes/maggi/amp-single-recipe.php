<?php include 'amp/amp-header.php' ?>
<?php
global $post, $wp_query;
$amp = new AmpUtils();
$recipe = new WPURP_Recipe($post->ID);
$categoria = wp_get_post_terms($post->ID,array("category"));
$preguntas = do_shortcode('[pregunta]');
?>
    <div class="amp-container">
        <div class="amp-single-recipe">
            <div class=amp-single-recipe-header>
                <span class="overlay"></span>
                <?php
                    $banner_mobile = $amp->get_image_with_alt('banner_mobile', $recipe_post->ID, "banner-mobile");

                ?>
                <amp-img layout="responsive" width="640" height="480"
                         src="<?= $banner_mobile->src ?>" alt="<?= $banner_mobile->alt ?>"></amp-img>
                <amp-img width="155" height="44" class="ornament " alt="recetas"
                         src="<?= get_bloginfo('template_directory') . '/images/ornamento.png' ?>"></amp-img>

                <div class="amp-single-recipe-headline">

                    <h2><?= $categoria[0]->name; ?></h2>
                    <h1><?= $recipe->title(); ?></h1>
                    <div class="amp-single-recipe-servings">
                        <amp-img src="<?= get_bloginfo('template_directory') . '/images/icons/porciones.svg?>' ?>"
                                 width="29" height="29" alt="porciones"></amp-img>
                        <p><span class="highlight"><?= $recipe->servings_normalized() ?></span> porciones</p>
                    </div>


                </div>

            </div>
            <div class="amp-single-recipe-details">
                <div class="recipe-difficulty">
                    <div class="recipe-difficulty-level">
                        <div class="recipe-difficulty-level-indicator <?php echo(get_the_terms($recipe->ID(), 'course')[0]->slug); ?>">
                            <span><span></span></span>
                            <span><span></span></span>
                            <span><span></span></span>
                        </div>
                        <div class="recipe-difficulty-level-label">
                            <p><?php echo(get_the_terms($recipe->ID(), 'course')[0]->name); ?></p>
                        </div>
                    </div>


                </div>
                <div class="recipe-duration">
                    <amp-img  alt="duracion" src="<?= get_bloginfo('template_directory') ?>/images/icons/clock.svg" width="30"
                             height="27"></amp-img>
                    <span class="recipe-duration-time"><?= $recipe->cook_time() ?> </span>
                    <span class="recipe-duration-time-units">min</span>

                </div>
            </div>
            <div class="amp-single-recipe-social">
                <?php
                $fav = new WPURP_Favorite_Recipes();
                ?>
                <div class="amp-single-recipe-social-favs">
                    <span class="fa fa-heart"></span>
                    <span class='num'><?= $fav->cantidad_favorites($recipe->ID()) ?></span>
                </div>
                <div class="amp-single-recipe-social-icons">
                    <p>Comparte en:</p>
            </div>


        </div>

            <div class="amp-single-recipe-prep">
                <table class="amp-single-recipe-prep-table">
                    <tr>
                        <td>Preparación:</td>
                        <td><?= $recipe->prep_time()." ".$recipe->prep_time_text() ?></td>
                    </tr>
                    <tr>
                        <td>Inactivo:</td>
                        <td><?=$recipe->passive_time()." ".$recipe->passive_time_text();?></td>
                    </tr>
                    <tr>
                        <td>Cocinando:</td>
                        <td><?=$recipe->cook_time()." ".$recipe->cook_time_text()?></td>
                    </tr>
                </table>
            </div>

            <div class="separator">
                <amp-img alt="separador" src="<?php echo get_template_directory_uri() ?>/images/icons/home-icon-qch.svg" width="44"
                         height="44"></amp-img>
            </div>

            <div class="amp-single-recipe-ingredients">
                <h3 class="amp-section-title">
                    INGREDIENTES
                </h3>
                <div class="amp-single-recipe-ingredients-list">
                    <?php
                    $ingredient_list = new WPURP_Template_Recipe_Ingredients();

                    echo preg_replace('/(<[^>]+) style=".*?"/i', '$1',$ingredient_list->output( $recipe ));
                    ?>
                </div>


            </div>

            <div class="separator">
                <amp-img alt="preparación" src="<?php echo get_template_directory_uri() ?>/images/icons/preparacion.svg" width="44"
                         height="44"></amp-img>
            </div>
            <div class="amp-single-recipe-preparation">
                <h3 class="amp-section-title">
                    PREPARACIÓN
                </h3>
                <div class="amp-single-recipe-preparation-list">
                    <?php
                    $preparation_list = new WPURP_Template_Recipe_Instructions();
                    echo $preparation_list->output( $recipe );
                    ?>
                </div>
                <div class="amp-single-recipe-nutrition">
                    <?php $tip = get_field('tip_culinario',$recipe_post->ID);
                    if($tip){
                        ?>
                        echo(" <div ><h2>TIP CULINARIO</h2><p><?= $tip ?> </p></div>");

                        <?php
                    }

                    $infoNut = get_field('informacion_nutricional',$recipe_post->ID);
                    if($infoNut){
                        ?>
                        <div class="amp-single-recipe-nutrition-table"><?= $infoNut?></div>
                        <?php
                    }

                    ?>
                </div>
            </div>


    </div>


<?php include 'amp/amp-footer.php' ?>