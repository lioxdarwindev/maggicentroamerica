<?php get_header(); ?>
<div id="Container">

	<?php
	 global $wp_query;

	$slug = $wp_query->query_vars["producto"];
        if($slug == null){
            $slug = $wp_query->query_vars["ingredient"];
            if($slug != null){
                $term = get_term_by( 'slug', $slug, "producto");
                $url = get_term_link($term);
                echo '<input id="product_url" type="hidden" data-url="'.$url.'" />';
            }else{
                $url = home_url();
                echo '<input id="product_url" type="hidden" data-url="'.$url.'" />';
            }
        }
	$term = get_term_by( 'slug', $slug, "producto");	
	$parent_term = get_term( $term->parent, 'producto' );
	//var_dump($parent_term);


	//var_dump($term);
	?>

	<div id="main">
		<div class="pure-g">

			<div class="pure-u-1 pure-u-md-3-5">
				<div id="prodImage">
				<?php
				echo('<img class="" src="'.get_field( 'imagen_producto','producto_'.$term->term_id)["sizes"]["imagen-producto"].'" alt="">');
				?>
			</div>
			<div class="botonera">

					
						<?php 
						for ($i = 2; $i <= 5; $i++) {
						
						$imgP = get_field( 'image_presentacion_'.$i,'producto_'.$term->term_id);
							if($imgP){
								echo('<div class="thumb '.$i.'"><img class="" src="'.$imgP.'" alt=""></div>');
							}
						}
						?>

						
					
					
				</div>
		</div>


		<div id="prodInfo" class="pure-u-1 pure-u-md-2-5">
			<div class="titulo"><h1><?php echo($term->name);?></h1></div>
			<div class="tituloPadre"><h2><?php echo($parent_term->name);?></h2></div>
			<div class="descripcion"><?php echo(get_field( 'descripcion','producto_'.$term->term_id));?></div>
			<div class="infoPresentacion">
				<div>
					<div class="tit">
						Presentación:
					</div>
					<div>
						<?php echo(get_field( 'presentacion','producto_'.$term->term_id));?>
					</div>
				</div>	
				<div>
					<div class="tit">
						<?php echo(get_field( 'titulo_medida','producto_'.$term->term_id));?>
					</div>
					<div>
						<?php echo(get_field( 'medida','producto_'.$term->term_id));?>
					</div>
				</div>
				<div>
					<div class="tit">
						Porciones:
					</div>
					<div>
						<?php echo(get_field( 'porciones','producto_'.$term->term_id));?>
					</div>
				</div>
			</div>
			<h2><a class="infoNutTit" href="#">Información Nutricional<p class="arrow right"></p></a></h2>
			<div class="tablaProd">
			<?php 
			$nombres = ["","","",""];
			$cont = 0;
			$table = get_field( 'informacion_nutricional','producto_'.$term->term_id);

				if ( $table ) {

				    echo '<table border="0">';

				      

				            echo '<thead>';

				                echo '<tr>';

				                    foreach ( $nombres as $th ) {

				                        echo '<th>';
				                            echo $th;
				                        echo '</th>';
				                    }

				                echo '</tr>';

				            echo '</thead>';
				        

				        echo '<tbody>';

				            foreach ( $table['body'] as $tr ) {

				                echo '<tr>';

				                    foreach ( $tr as $td ) {
					                    if($td){	
					                        echo '<td>';
					                            echo $td['c'];
					                        echo '</td>';
					                    }	
				                    }

				                echo '</tr>';
				            }

				        echo '</tbody>';

				    echo '</table>';
				}

			?>
				<p class="notainfonut">(*) Los porcentajes de valores diarios están basados en una dieta recomendada de 2.000 kcal (FDA).</p>
			</div>	
				


			</div>

			<div class="recetasSugeridas pure-u-1">
			<h2 class="recTit">RECETAS SUGERIDAS</h2>
					<?php
						/*$args = array(
							'DisableFilterPais' => true,
						    'post_type' => 'recipe',
						    'post_status' => 'publish',
						   'posts_per_page' => 5,
						   'cache_results' => false, 
						    'tax_query' => array(
						        array(
						            'taxonomy' => 'producto',
						            'field' => 'slug',
						            'terms' => $slug
						        ),
						        array(
						            'taxonomy' => 'pais',
						            'field' => 'slug',
						            'terms' => $_SESSION['pais']
						        )
						    )
						);*/
						
						/*$c_args = array (
							'post_type'              => 'recipe',
							'nopaging'               => true,
							'posts_per_page'         => '5',
							'order'                  => 'DESC',
							'orderby'                => 'ID',
							'tax_query' => array(
						        array(
						            'taxonomy' => 'producto',
						            'field' => 'slug',
						            'terms' => $slug
						        ),
						    )
						);
						wp_reset_query();
						$query = new WP_Query( $c_args );
						$query -> set( "posts_per_page" , 5 );
						
						//$custom = new WP_Query( $args );
						   */
						 ?>	

<?php //echo "<pre>"; var_dump( $query ); echo "</pre>"; ?>
<?php  //echo "<pre>"; var_dump( get_post_types() ); echo "</pre>"; ?>

<?php 
/*
if ( $query->have_posts() ) {
	echo '<ul>';
	while ( $query->have_posts() ) {
		$query->the_post();
		echo '<li>' . get_the_title() . '</li>';
	}
	echo '</ul>';
	wp_reset_postdata();
}else{
	echo 'NO POSTS';
}*/
?>					



<?php 

	/**
		NOTE BY DANILO
		I am using a custom wpdb object in here because somehow, the WP_Query is not working as it should. I pass the arguments to the function and it is still not respecting it, so I have to make a really really cuestom query to see if I can get the values I want to.
		
		I believe that there is a plugin installed somewhere in the site that is overwritting 
		the custom query I want to execute, and I really don't have the time nor the will to check what is the plugin that is killing this functionality.
		
		The idea is to get the list of posts of type recipe and filter them by country and by
		the product that is being showed in this page. Please wish me luck.
	*/
	global $wpdb;
	/**
	We are using the limit in case we dont get enought recipes for this product
	*/
	$limit = 20;
	

	$query_string = "
	SELECT 
		term.term_id , 
		term.name , 
        term.slug , 
        tt.term_id , 
        tt.term_taxonomy_id,
		tr.object_id,
        p.*


	FROM {$wpdb->prefix}terms AS term 

	JOIN {$wpdb->prefix}term_taxonomy AS tt ON tt.term_id = term.term_id

	JOIN {$wpdb->prefix}term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id

	JOIN {$wpdb->prefix}posts AS p ON p.ID = tr.object_id


	WHERE 
			term.slug = '$slug'
			AND p.post_status = 'publish'
			AND p.post_type = 'recipe'
	ORDER BY RAND() 
	LIMIT $limit
	";
	
	$results = $wpdb->get_results( $query_string , OBJECT );

	//echo $query_string;
	
	//echo "<pre>"; var_dump( COUNT( $results ) ,  $results ); echo "</pre>";
	
	$ids = [];
	
	$posts = [];
	
	foreach( $results AS $result ){
		$ids[] = $result->ID;
		$posts[] = $result;
	}
	
	$ids = array_filter( $ids );
	
	//echo "<pre>";var_dump( $ids );echo "</pre>";
	
	$missing = $limit - COUNT( $ids );
	 
	if( count( $ids ) > 0 ){
			$query_string = "SELECT * FROM {$wpdb->prefix}posts WHERE ID NOT IN ( " . implode( ",", $ids ) . " ) AND post_type = 'recipe' AND post_status = 'publish' 
	 ORDER BY RAND() 
	 LIMIT $missing";
	}else{
			$query_string = "SELECT * FROM {$wpdb->prefix}posts WHERE post_type = 'recipe' AND post_status = 'publish' 
	 ORDER BY RAND() 
	 LIMIT $missing";
	}
	
	 
	
	$results = $wpdb->get_results( $query_string , OBJECT );
	foreach( $results AS $result ){
		$ids[] = $result;
		$posts[] = $result;
	}


	//echo "<pre>";var_dump( $ids );echo "</pre>";
	
	/*$args = array(
		'post__in' => $ids
	);*/

	//$posts = get_posts($args);
	
	// echo "<pre>";var_dump( $posts );echo "</pre>";
	// echo "<pre>";var_dump( get_field( "recipe_alternate_image" , $posts[0]->ID) );echo "</pre>";
	// echo "<pre>";var_dump( get_post_meta($posts[0]->ID) );echo "</pre>";
	// echo "<pre>";var_dump( wp_get_attachment_image( get_post_meta($posts[0]->ID) ) );echo "</pre>";
	
	
?>
<?php 

	foreach( $posts AS $post ){
		 echo "<pre>"; 
		 //var_dump( get_post_meta( $post->ID ) ); 
		 // var_dump( get_post_meta($post->ID, 'recipe_alternate_image') ); 
		 // var_dump( wp_get_attachment_image_src( get_post_meta($post->ID, 'recipe_alternate_image') ) );
		 echo "</pre>";
		 // banner_mobile
		 // recipe_alternate_image
		 
	}
	
	
	//echo "<pre>"; 
	// var_dump( wp_get_attachment_image_src( 2205 ) );
	//$banner_id = ;
	//var_dump( $banner_id ); 
	//var_dump( wp_get_attachment_image_src( (int)get_post_meta( 2288 , "banner_mobile" )[0] )[0]  );
	//echo "</pre>"; 
	
?>
						 

				</div>
 							

		</div>

		<div id="recetasGrid" class="grid">
								<?php // include  'category.php'; ?>
								<?php //get_template_part( 'loop', 'recetas' ); ?>
								
								<?php foreach( $posts AS $post ): ?>
								
								
								
								<a rel="<?php echo $post->ID ?>" class="grid-item mix" href="<?php echo $post->guid ?>"><div class="marco"><span class="overl"></span><img height="100px" class="imagenTemp loaded" src="<?php echo wp_get_attachment_image_src( (int)get_post_meta( $post->ID , "banner_mobile" )[0] )[0] ?>" data-src="<?php echo wp_get_attachment_image_src( (int)get_post_meta( $post->ID , "banner_mobile" )[0] )[0] ?>"></div><span> 
								<p class="tituloRes">
									<?php echo $post->post_title ?>
									<?php //var_dump( get_field('thumbnail',$post->ID) )?>
									<?php //var_dump(  get_post_meta($post->ID) ) ?>
								</p>
								</span><div class="info"><div class="Aut_info"><p class="author_name"><?php echo get_field('autor_2', $post->ID) ?></p><p class="fecha"><?php echo get_the_date() ?></p></div><div class="tiempo pure-u-md-4-24"><img src="<?php echo site_url() ?>/wp-content/themes/maggi/images/icons/clock.svg">    
						<p class="t">25</p><p>min</p></div>
						</div></a>
			
			
			<?php endforeach; ?>
			
		</div>
		
		</div>

<div id="grupos">
	<?php ///include("productos_interno.php");?>
</div>

	</div>


<?php get_footer(); ?>