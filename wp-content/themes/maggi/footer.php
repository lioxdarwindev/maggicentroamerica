<?php wp_footer(); ?>
<div class="registrate">
	<div class="pure-g intruccionesQCH ">
		<div class="pure-u-10-24"></div>
		<div class="pure-u-2-24 icon"><img src="<?=  get_bloginfo('template_directory')?>/images/icons/registrate.svg"/></div>
		<div class="pure-u-10-24"></div>
		<div  class="pure-u-1-1">
			<h3 class='boletinTitle'>REGÍSTRATE AL BOLETÍN</h3>
			<p>¡Suscríbete para recibir las últimas noticias y promociones!</p>
			
			<div class="boletin">
				<?php echo(do_shortcode('[contact-form-7 id="164" title="Boletin"]'));?>
			</div>

		</div>

	</div>
</div>

<div id="footer">
	<div class="footer_inner">
		<span class="logoFooter"> <img src="<?=  get_bloginfo('template_directory')?>/images/logo.svg"></span>
	<a href="<?php echo get_template_directory_uri(); ?>/terminos.pdf">Términos de Uso</a> 
	<a href="<?php echo get_template_directory_uri(); ?>/privacidad.pdf">Políticas de Privacidad</a>
	<a href="#">Aviso de Privacidad</a>
	<span class="social">
	<a class="fb soc hgrow" target="_blank" href="https://www.facebook.com/MaggiCentroamerica/"><?php echo file_get_contents(get_template_directory()."/images/icons/fb.svg"); ?></a>
	<a class="inst soc hgrow" href="https://www.instagram.com/maggicentroamerica/" target="_blank" href="#"><?php echo file_get_contents(get_template_directory()."/images/icons/inst.svg"); ?></a>
	<a class="yt soc hgrow" target="_blank" href="https://www.youtube.com/channel/UCla3XXd-Q386Sgr0aNe4rKA"><?php echo file_get_contents(get_template_directory()."/images/icons/yt.svg"); ?></a>
	</span>
	
	<p><?php echo date("Y") ?>. Todos los derechos reservados.</p>
	<p>Marcas Registradas usadas bajo licencia de su titular, Société des Produits Nestlé S.A;</p>
	<p>Case Postale 353, 1800 Vevey, Suiza</p>

	</div>
	



</div>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">x</span>
    <?php 
		echo(do_shortcode('[contact-form-7 id="165" title="HÁBLENOS"]'))	;
	?>
  </div>

</div>
<?php if ( is_user_logged_in() ): ?>
<script type="text/javascript">
	
	setInterval(function() {
			console.log( "Reload time" );
                  window.location = '?auto_logout';
				  
	}, 600000); 
	
</script>
<?php endif; ?>

<?php if (  isset($_GET["auto_logout"])  ): ?>
<link rel="stylesheet" type="text/css" media="all" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" />
<div id="dialog" title="Sesión Finalizada">
  <p>Su sesión ha sido finalizada automáticamente por falta de actividad.</p>
</div>
<script type="text/javascript">
  $( function() {
    $( "#dialog" ).dialog({
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
	
	$(window).scrollTop($('#dialog').offset().top);
	
  } );
  </script>
<?php endif; ?>
</body>
</html>