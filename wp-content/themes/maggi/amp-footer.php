<!-- <div class="amp-newsletter">
    <div class="separator">
    <amp-img src="<?php echo get_template_directory_uri() ?>/images/icons/registrate.svg" width="44"
             height="44"></amp-img>
</div>
<div class="amp-headline">
    <h2>REGÍSTRATE AL BOLETÍN</h2>
    <a href="<?= home_url('/') . 'suscribete/' ?>" class="btn btn-primary">¡Suscríbete para recibir las últimas
        noticias y promociones!</a>
</div>
</div> -->

<footer>
    <amp-img class="logo-footer" src="<?=  get_bloginfo('template_directory')?>/images/logo.svg" height="83" width="162"></amp-img>
    <div class="footer-container">
        <div class="footer-container-links">
            <a href="#">Términos de Uso</a>
            <a href="#">Políticas de Privacidad</a>
            <a href="#">Aviso de Privacidad</a>
        </div>
        <div class="footer-container-social">
            <a class="fb soc hgrow" target="_blank" href="https://www.facebook.com/MaggiCentroamerica/">
                <amp-img src="<?php echo get_bloginfo('template_directory')."/images/fb.png"; ?>" width="44" height="44"></amp-img>
            </a>
            <a class="inst soc hgrow" href="https://www.instagram.com/maggicentroamerica/" target="_blank">
                <amp-img src="<?php echo get_bloginfo('template_directory')."/images/inst.png"; ?>" width="44" height="44"></amp-img>
            </a>
            <a class="yt soc hgrow" target="_blank" href="https://www.youtube.com/channel/UCla3XXd-Q386Sgr0aNe4rKA">
                <amp-img src="<?php echo get_bloginfo('template_directory')."/images/yt.png"; ?>"  width="44" height="44"></amp-img>
            </a>
        </div>
        <div class="footer-container-disclaimer">
            <p>2016. Todos los derechos reservados.</p>
            <p>Marcas Registradas usadas bajo licencia de su titular, Société des Produits Nestlé S.A;</p>
            <p>Case Postale 353, 1800 Vevey, Suiza</p>
        </div>
        <!-- <div class="footer-container-brand">
            <a class="nestle" target="_blank" href="https://www.nestleagustoconlavida.com/">
                <amp-img  width="72" height="47" src="<?=  get_bloginfo('template_directory')?>/images/icons/nestle.svg"></amp-img></a>
        </div> -->
    </div>


</footer>
</body>
</html>