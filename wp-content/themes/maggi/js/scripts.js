var nombre;
var filtroHome = 1;
var filtros_home = '';
var momentos = [];
var ingredientes = [];
var lastFav = 0;
var filtroIngredientes = "";
var filtroTiempo = "";
var filtroDificultad = filtroPersonalidad = filtroMomento = notifica = "";


var off = 0;



jQuery(document).ready(function( $ ) {

//frame busting JavaScript as a protection against XFS.
 if(self == top) {
       document.documentElement.style.display = 'block'; 
   } else {
       top.location = self.location; 
   }

$product = $('#product_url');

if($product.length != 0){
    url = $('#product_url').attr('data-url');
    if(url != ''){
        window.location.replace(url);
    }
}


$("#resetpassform #pass1-text").hide();
$("#resetpassform #pass1").hide();
$( "#resetpassform .user-pass1-wrap .wp-pwd").prepend('<input type="password" id="passtemp" class="input hide-if-no-js strong" size="20" value="" autocomplete="off" aria-describedby="pass-strength-result">');

$( "#lostpasswordform").submit(function( event ) {
    if($("#user_login").val() == ''){
        $("#user_login").css("border-color","red");
        return false;
    }
});

$( "#resetpassform").submit(function( event ) {
	vali = false;
			err = "";
			pswd = $("#passtemp").val();
		  //validate letter
			// at least one number, one lowercase and one uppercase letter
			// at least six characters
			// taken from http://www.the-art-of-web.com/javascript/validate-password/
			var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{12,}/;
			vali = re.test(pswd);



		if(vali){
			$("#resetpassform #pass1").val($("#passtemp").val());
			$("#resetpassform #pass1-text").val($("#passtemp").val());
			$("#resetpassform").submit();
		}else{
			$(".description").html("La Contraseña debe contener letras, numeros , mayúsculas y ser de al menos 12 characteres.");
			
		}
		 event.preventDefault();
		
 
});

function mix(){
var $container = $('#recetasGrid'),
	lazyLoad = function($show){
		var $image = $show.find('img');

		$image.each(function(){
			var $img = $(this),
				src = $img.attr('data-src');
					
			// If an image hasn't already been loaded, swap the source and add loaded class
					
			if(!$(this).hasClass('loaded') && src){
				$img[0].src = src;
				$img.addClass('loaded');
			}
		});
	};

var numberOfPages = 24;
if($("#number_of_pages").length){
	numberOfPages = $("#number_of_pages").val();
}	

	$container.mixItUp({
						load:{
				  				sort:'fav:desc'
				  			},	
				  		callbacks: {

				  		onMixStart: function(state,futureState){

				  			var visibles = futureState.$show;
				  			var hide = futureState.$hide;
							$(".grid-item").removeClass('first-visible');
				  			// $(visibles[0]).addClass('first-visible');
				  			// if($(this).parent().attr("id") != "menuLateral"){   //Se comentan estas líneas por el nuevo diseño
				  				// $(visibles[1]).addClass('first-visible');
				  			// }
				  			
							lazyLoad(futureState.$show);
							
						}, 
						onMixEnd: function(state){
							if(filtroHome  ==  3){
								$("#dropIngredientes").html("");

								$.each( arIng, function( key, valueIng ) {

									$.each(valueIng.ids , function( key, value ) {
										
										if($('a[rel="'+key+'"]').is(':visible')){

											$("#dropIngredientes").append("<li><a href='#' rel='"+valueIng.slug+"'>"+valueIng.nombre+"</a></li>");
											  return false;
										}
										//console.log(valueIng.nombre);
									});
								
								});
								
							}

						} 
					},pagination: {
				        limit: numberOfPages,
				        generatePagers: true,
				        maxPagers: 4,
				        prevButtonHTML: '',
				        nextButtonHTML: ''

					},
					controls: {
				      toggleFilterButtons: true,
				      toggleLogic: 'and'
				    }
	}); 

}

function setMix(){

	 if ($('#recetasGrid').mixItUp('isLoaded')) {
		            $('#recetasGrid').mixItUp('destroy');
		            
	} 
	mix();

	// $(".FilterSelect").on('change', function(){

	//     $("#recetasGrid").mixItUp('filter', this.value);

	// });	        

}




	 // $('#Container').mixItUp();
	  $.ajaxSetup({cache:false});

      $('.post-link').click(function() {
      
       		var post_link = $(this).attr("rel");

             //$("#recetasGrid").html("content loading");
             $("#recetasGrid").load(post_link, function(){

           			setMix(post_link);
             });
            
        return false;
        });

setMix();

//$('.filter .post-link'){}

  $('#categorias a').click(function(e) { //acordion de subcatergorias
  	$(".mensaje").hide();
  	filtroIngredientes = "";
  	$(".ingredientesTit").html("Ingredientes");
	filtroTiempo = "";
	$(".tiempoTit").html("Tiempo de Comida");
	filtroDificultad ="";
	$(".dificultadTit").html("Dificultad");
  		$('#categorias a').removeClass('activo');
  		target = "#"+e.currentTarget.id;
  		$(target).addClass('activo');
  		$(target+"-2").addClass('activo');
  		$(target.replace("-2", "")).addClass('activo');
  		filtros_home = '';
  	    $('#recetasGrid').mixItUp('filter', 'none');
  	    $(".active").removeClass("active");
  		$("#subcategorias > div").removeClass("abierto");
  		setTimeout(function(){
  			
  			$(".sub-"+$(e.currentTarget).attr('id')).addClass("abierto");
  			$('#recetasGrid').mixItUp('filter', 'all');

  		}, 1000);

  		
  });

 $('#subcategorias div span').click(function(e) { //seleccion de subcatergorias
	// $("#subcategorias > div").removeClass("abierto");
	
});


$('.wrapper-dropdown').on('click', function(e){
	if ( $(this).hasClass('active') ) {
		$(this).removeClass('active flecha_abajo');
	} else {
		$('.wrapper-dropdown').removeClass('active flecha_abajo');
		$(this).addClass('active');
		var divh = $(this).outerHeight( true );
		if ( divh >= 300 ) {
			$(this).addClass('flecha_abajo');
		}
	}
	e.preventDefault();
});



	$('.home .wrapper-filter li').on('click', "a" , function(event){ //filtros
			
		//if(filtroHome < 3){
			//$("#f"+filtroHome).addClass('hidden');
			//filtroHome ++;
			//$("#f"+filtroHome).toggleClass('hidden');
			

		
		//}
			//filtros_home = filtros_home + "."+$(this).attr('rel');
			//console.log($("#f1").attr('rel') + $("#f2").attr('rel') + $("#f3").attr('rel'));
			//console.log(filtros_home);
			//$("#recetasGrid").mixItUp('filter', filtros_home);



		$(this).parent().parent().find("a").removeClass("active");
		$(this).addClass("active");
		//filtros_home = filtros_home + " ."+$(this).attr('rel');
		
	// var arr = [];
	// var fin ="";	
	//	console.log($(this).parent().parent());

		if($(this).data('clase') == 'personalidad'){
	
			$(".personalidadTit").html($(this).text());
			filtroPersonalidad =  $(this).attr('rel');
		}
		if($(this).data('clase') == 'momento'){
			$(".momentoTit").html($(this).text());
			filtroMomento =  $(this).attr('rel');
		}
		if($(this).data('clase') == 'ingredientes'){
			$(".ingredientesTit").html($(this).text());
			filtroIngredientes =  $(this).attr('rel');
		}	
		//console.log((filtroPersonalidad+filtroMomento+filtroIngredientes));

	if((filtros_home+filtroPersonalidad+filtroMomento+filtroIngredientes).length == 0){
		$("#recetasGrid").mixItUp('filter', 'all');
	}else{
		$("#recetasGrid").mixItUp('filter', filtros_home+filtroPersonalidad+filtroMomento+filtroIngredientes);
	}
	

	});

	$('.archive .wrapper-filter a , .single-recipe .wrapper-filter a').on('click', function(event){ //filtros\
		$(this).parent().parent().find("a").removeClass("active")
		$(this).addClass("active");
		//filtros_home = filtros_home + " ."+$(this).attr('rel');
		
	// var arr = [];
	// var fin ="";	
	//	console.log($(this).parent().parent());

		if($(this).data('clase') == 'ingredientes'){
	
			$(".ingredientesTit").html($(this).text());
			filtroIngredientes =  $(this).attr('rel');
		}
		if($(this).data('clase') == 'tiempo'){
			$(".tiempoTit").html($(this).text());
			filtroTiempo =  $(this).attr('rel');
		}
		if($(this).data('clase') == 'dificultad'){
			$(".dificultadTit").html($(this).text());
			filtroDificultad =  $(this).attr('rel');
		}	
		//console.log((filtros_home+filtroIngredientes+filtroTiempo+filtroDificultad).length);

	if((filtros_home+filtroIngredientes+filtroTiempo+filtroDificultad).length == 0){
		$("#recetasGrid").mixItUp('filter', 'all');
	}else{
		$("#recetasGrid").mixItUp('filter', filtros_home+filtroIngredientes+filtroTiempo+filtroDificultad);
	}
	

	// 	arr.push(filtros_home,filtroIngredientes,filtroTiempo,filtroDificultad);

	// 	$.each( arr, function( key, value ) {
	// 	//if(fin && key < arr.length && value){
	// 		//fin = fin + " , ";
	// 	//	fin = fin + " , ";
	// //	}
	// 		fin = fin + value;
	// 	});
		

		


		// if($(this).data('clase') == 'tiempo'){
		// 	if(filtros_home || filtroIngredientes){
		// 		filtros_home = filtros_home+ " , ";
		// 	}
		// 	filtroTiempo =  $(this).attr('rel');
		// }
		// if($(this).data('clase') == 'dificultad'){
		// 	filtroDificultad =  $(this).attr('rel');
		// }	
		
	


		//$("#recetasGrid").mixItUp('filter', fin);
	});

	$("#subcategorias .filter").click(function(e) {
   
    filtros_home = filtros_home +$(this).data( "filter");
   });


	$('.wrapper-sort a').on('click', function(event){ //sorting
		//console.log($(this).attr('rel'));
		$(this).parent().parent().find("a").removeClass("active")
		spanText = $(this).parent().parent().parent().find("span");
        spanText.text("Búsqueda por: " + $(this).text());
		$(this).addClass("active");
		$("#recetasGrid").mixItUp('sort',$(this).attr('rel'));

		
	});





////modal hablanos  ///
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
$('.hablanos').on('click', function(e){
	$('body').addClass("overflow-abierto"); 
	  $('nav').removeClass('slide-in');
	  modal.style.display = "block";
});

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
    $('body').removeClass("overflow-abierto");
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        $('body').removeClass("overflow-abierto");
    }
}

// categorias del Home

$(".homeCat #options a").hover(function(e) {

         $(".homeCat #options .tit").html($(this).attr("rel"));
      //   $(".homeCat #options .tit").animate({left:($(this).position().left - ($(this).width()/2))});
   	$(".homeCat #options .tit").offset({left:$(this).position().left - ($(".homeCat #options .tit").width() - $(this).width())/2});
   },function(e) {
   	    $(".homeCat #options .tit").offset({left:off});
         $(".homeCat #options .tit").html(textoTit);
   });

      $(".homeCat #options a").click(function(e) {
      
      	$(".personalidadTit").html("Personalidad");
      	$(".momentoTit").html("Momento");
      	$(".ingredientesTit").html("Ingredientes");
      	$(".seleccion .active").removeClass("active");
      	off = $(this).position().left - ($(".homeCat #options .tit").width() - $(this).width())/2;
        textoTit = $(this).attr("rel");
         $(".homeCat #options a").removeClass("sele");
        $(this).addClass("sele");
        $(".actual-category").empty().append("<p>"+textoTit+"</p>");
        $('html,body').animate({
         scrollTop: $("#main").offset().top
      });


    
        filtros_home = "."+$(this).attr('class').split(' ')[0];
		//$("#mainBusqueda > div").addClass('hidden');

      filtroHome = 1;
     //$("#f1").removeClass('hidden');
      $("#recetasGrid").mixItUp('filter', filtros_home);

        e.preventDefault();
      });


$(".homeCat #options a").hover(function(e) {

});
$('.busquedaAvanzadaLabel').on('click', function(event){ //sorting
		$(event.currentTarget).find("p").toggleClass('right down');
		$("#busquedaAvanzada").toggleClass("abierto");
		event.preventDefault();
		
});

$('.comentariosLabel').on('click', function(event){ //sorting
		$(event.currentTarget).find("p").toggleClass('right down');
		$("#comentarios").toggleClass("abierto");
		event.preventDefault();
});

$('.preguntasLabel').on('click', function(event){ //sorting
		$(event.currentTarget).find("p").toggleClass('right down');
		$("#preguntas").toggleClass("abierto");
		event.preventDefault();
});




$('.wpurp-recipe-favorite').on('click', function(event){ //boton de favorito
	//$(event.currentTarget).css( 'pointer-events', 'none' );
	num = parseInt($(".numFab .num").html());
	if($(event.currentTarget).find("i").hasClass("fa-heart-o")){
		if(lastFav <= 0){
				num = num + 1;
				lastFav = 1;
				$(".noti span").html(parseInt($(".noti span").html())+1);
		}
	
	
	}else{
		if(lastFav >= 0){
			num = num - 1;
			lastFav =  - 1;
			$(".noti span").html($(".noti span").html()-1);
		}
	}
	$(".numFab .num").html(num);


});
$('.infoNutTit').on('click', function(e){ //sorting
	$(this).toggleClass("abierto");
	$(".tablaProd").toggleClass("abierto");
	e.preventDefault();

});


$('.searchMov').on('click', function(e){ //sorting
	$(".busqueda").toggleClass("abierto");
	e.preventDefault();

});

$('.blog_print').click(function(){
     window.print();
});

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

// $('#newask #submit').click(function(e){
//     var value=$.trim($("#question").val());
//     var valueEmail=$.trim($("#qt_email").val());
//     var valueUsername=$.trim($("#qt_username").val());
// 	if(value.length<=0)
// 	{
// 		e.preventDefault();
// 		$("#newask #respuesta").html("<div class='alert danger'><b>Error!</b> Debe ingresar una pregunta.</div>");
// 	}else if(valueUsername.length<=0){
// 		e.preventDefault();
// 		$("#newask #respuesta").html("<div class='alert danger'><b>Error!</b> Debe ingresar su nombre.</div>");
// 	}else if(!validateEmail(valueEmail)){
//             e.preventDefault();
//             if(valueUsername.length<=0){
// 				$("#newask #respuesta").html("<div class='alert danger'><b>Error!</b> Debe ingresar su correo electrónico.</div>");
//             }else{
//                 $("#newask #respuesta").html("<div class='alert danger'><b>Error!</b> Debe ingresar un correo electrónico válido.</div>");
//             }
// 	}else{
// 		$("#newask #respuesta").html("<div class='alert success'><b>Muchas Gracias!</b> La pregunta está siendo enviada, por favor no recargue la página...</div>");
// 	}
// });

     //Billo//
    $( '#ajax-login-register-dialog, #ajax-login-register-login-dialog' ).dialog( "option",
         "appendTo",".dialogo" , "position", {

            my: "top",
            at: "bottom",
            of:$(".dialogo .log")

        } 
    );

// if ( _zm_alr_settings.login_handle.length ){
	        if ( $('body.logged-in').length ){

	            $this = $(".menu-item-119").children('a');
	            if(num_fav){
	            	notifica = "<p class='noti'><span>"+num_fav+"</span></p>";
	            	
	            }
	           // $this.html( _zm_alr_settings.logout_text ); //cambio billo.
	           $this.html( "<div id='usuario'><p>¡Hola "+nombre+"!</p>"+notifica+"</div><a id='btnCloseSession'  href='"+_zm_alr_settings.wp_logout_url+"'><p class='fa fa-power-off' aria-hidden='true'></p></a>" );
	           $this.attr( 'href', favUrl);
          

        }


// }


//// No necesario si se modifica el favorites del Plugin
//$(".wpurp-recipe-favorite .fa-heart-o , .wpurp-recipe-favorite .fa-heart").html('<img src="'+templateUrl+'/images/icons/close-fav.svg"><p></p>');

jQuery(document).ready(function($) {
	$('#commentform').validate({
	 
		onfocusout: function(element) {
		  this.element(element);
		},
		 
		rules: {
		  author: {
		    required: true,
		    minlength: 2
		  },
		 
		  email: {
		    required: true,
		    email: true
		  },
		 
		  comment: {
		    required: true,
		    minlength: 2
		  }
		},
		 
		messages: {
		  author: "Por favor ingrese su nombre",
		  email: "Por favor ingrese una dirección de correo válida",
		  comment: "Debe escribir un mensaje"
		},
		 
		errorElement: "div",
		errorClass: "alert danger",
		errorPlacement: function(error, element) {
		  //element.before(error);
		  error.insertAfter(element);
		}
	 
	});

	$('#newask').validate({
	 
		onfocusout: function(element) {
		  this.element(element);
		},
		 
		rules: {
		  qt_username: {
		    required: true,
		    minlength: 2
		  },
		 
		  qt_email: {
		    required: true,
		    email: true
		  },
		 
		  question: {
		    required: true,
		    minlength: 2
		  }
		},
		 
		messages: {
		  qt_username: "Por favor ingrese su nombre",
		  qt_email: "Por favor ingrese una dirección de correo válida",
		  question: "Debe escribir una pregunta"
		},
		 
		errorElement: "div",
		errorClass: "alert danger",
		errorPlacement: function(error, element) {
		  //element.before(error);
		  error.insertAfter(element);
		}
	 
	});

	$(".not-a-member-handle").click(function(e){
		e.preventDefault();
		window.location.replace("https://www.maggicentroamerica.com/newsite/registrate/");
	});

	/*MAGGI TV*/
	$(".videos_container > div:not(.pagination)").click(function(){
		$(".video_main iframe").attr("src",$(this).data("reference"));
		$(".social_videos").attr("data-id",$(this).data("id"));
		$(".social_videos .fb a").attr("href",$(this).data("share-fb"));
		$(".social_videos .tw a").attr("href",$(this).data("share-tw"));
		$(".social_videos .pin a").attr("href",$(this).data("share-pin"));
		var $container = $("html,body");
		var $scrollTo = $('.video_main');

		$container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0},300);
	})
	
	$(".social_videos a").click(function(e){
		var link = $(this).attr('href');
		e.preventDefault();
		window.open(link,"_blank");
	})

});

























 	
 });