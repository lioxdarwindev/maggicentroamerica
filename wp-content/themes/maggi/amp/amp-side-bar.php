<amp-sidebar id='sidebar'
             layout="nodisplay"
             side="left">
    <span class="amp-close-image"
          on="tap:sidebar.close"
          role="button"
          tabindex="1"
    > </span>

    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
</amp-sidebar>