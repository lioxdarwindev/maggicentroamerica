<!doctype html>
<html ⚡>
<head>
    <meta charset="utf-8">
    <title><?php wp_title(''); ?></title>
    <?php do_action( 'wpseo_head' );  ?>

    <?php
    global $wp;
    $current_url = str_replace("amp", "", home_url(add_query_arg(array(),$wp->request)));
    ?>
    <link rel="canonical" href="<?php echo $current_url ?>" />
    <?php
    $amp = new AmpUtils();
    $amp->add_scripts();
    ?>

    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <?php

        $amp->add_micro_data();

    ?>

    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <?php
        include 'amp-styles.php';
    ?>
</head>
<body>
<amp-analytics type="googleanalytics" id="analytics1">
    <script type="application/json">
        {
            "vars": {
                "account": " UA-58528578-2"
            },
            "triggers": {
                "trackPageview": {
                    "on": "visible",
                    "request": "pageview"
                }
            }
        }
    </script>
</amp-analytics>

<?php include 'amp-side-bar.php' ?>
<?php include 'amp-top-bar.php' ?>


