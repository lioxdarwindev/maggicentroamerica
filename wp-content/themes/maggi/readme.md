# Implementar AMP

1. Copiar el contenido de la carpeta maggi-amp dentro de `/wp-content/themes/maggi`.
2. Agregar a functions.php la siguientes lineas al final del archivo, esto definira las rutas del amp

```php

include 'amp-functions.php';
//define the paths for amp support
$amp_fn = new AmpUtils();
$amp_fn->init();
```

3. En el archivo header.php, linea 50 agregar el siguiente codigo para agregar el meta tag de amp a las paginas, del sitio


```php
        <?php
        global $wp;
        $current_url = home_url(add_query_arg(array(),$wp->request));
        ?>
        <link rel="amphtml" href="<?= $current_url ?>/amp/" />
```
