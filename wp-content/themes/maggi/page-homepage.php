<?php
/**
 * Template Name: Homepage
 *
 */
 get_header(); ?>
<!-- ams -->
	<div class="content-area">
		<div class="homeCat">
<?php

	$my_query = null;
	$my_query = new WP_Query( array('post_type' => 'homebanner' , 'name' => 'banner-home' , 'posts_per_page=1'));

	if( $my_query->have_posts() ) {
  		while ($my_query->have_posts()) : $my_query->the_post();
  		$homePost = $post; 
  		?>
  		<img class="desktop" src="<?= get_field('banner',$post->ID)["sizes"]["banner-home"]?>"/>
  		<img class="mobile" src="<?= get_field('banner_mobile',$post->ID)["sizes"]["banner-mobile"]?>"/>
                <h1 class="hero-text">Encuentra aquí deliciosas <span class="hero-here">ideas</span><br> que van con tu cocina</h1>  		
  	<?php
 endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>

<div id="options">
	<span>
		<a rel="RICO Y SALUDABLE" class="rico-y-saludable" href="">
			<?php echo file_get_contents(get_template_directory()."/images/icons/rico.svg", false, $context); ?>
		</a>
		<a rel="RÁPIDO Y FÁCIL" class="rapido-y-facil" href="">
			<?php echo file_get_contents(get_template_directory()."/images/icons/rapido.svg", false, $context); ?>
		</a>
		<a rel="OCASIONES ESPECIALES" class="ocasiones-especiales" href="">
			<?php echo file_get_contents(get_template_directory()."/images/icons/ocasiones.svg", false, $context); ?>
		</a>
		<a rel="COCINANDO CON LOS NIÑOS" class="cocinanado-con-los-ninos" href="">
			<?php echo file_get_contents(get_template_directory()."/images/icons/ninos.svg", false, $context); ?>
		</a>
		<a rel="RECETAS DE NUESTRA TIERRA" class="recetas-de-nuestra-tierra" href="">
			<?php echo file_get_contents(get_template_directory()."/images/icons/nuestraTierra.svg", false, $context); ?>
		</a>

		<h3 class="tit"></h3>
	</span>
</div>
</div>



	<div id="Container">
	<div id="main">
	<div class="pure-g intruccionesQCH">
	<div class="pure-u-10-24"></div>
	<div class="pure-u-2-24 icon"><img src="<?=  get_bloginfo('template_directory')?>/images/icons/home-icon-qch.svg"/></div>
	<div class="pure-u-10-24"></div>
	<div  class="pure-u-1-1">
		<h2>¿QUÉ COCINARÉ HOY?</h2>
		<p>Selecciona las opciones para buscar tu receta ideal</p>
	</div>

	</div>
	


	<div class="pure-g" id="mainBusqueda">
		<div id="f1" class="pure-u-1-1 pure-u-md-1-3">
			<div class="number">
					01
					<p class='in'>SOY</p>
			</div>	
			<div class="seleccion">
			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">


					<span class='personalidadTit'>Personalidad</span>
					  <ul class="dropdown">
					  <li> <a data-clase="personalidad" rel="" href="#">Todos</a></li>
							<?php 
							$persTerm = get_terms( 'personalidad', array(
							    'hide_empty' => true,
							) );

							foreach($persTerm as $term) {
								echo('<li> <a data-clase="personalidad" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
							}
							?>
					   </ul>
			</div>
			</div>
		</div>
		<div id="f2" class="pure-u-1-1 pure-u-md-1-3">
			<div class="number">
					02
				<p class='in'>PARA</p>
			</div>	
			<div class="seleccion">
			<div  class="wrapper-dropdown wrapper-filter " tabindex="1">
				<span class='momentoTit'>Momento</span>
				  <ul class="dropdown">
				   <li> <a data-clase="momento" rel="" href="#">Todos</a></li>
						<?php 
						$persTerm = get_terms( 'momento', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="momento" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
				</div>
			</div>
			</div>
			<div id="f3" class="pure-u-1-1 pure-u-md-1-3">
			<div class="number">
					03
					<p class='in bu'>BUSCAR</p>
			</div>	
			<div class="seleccion">
			<div id="f3" class="wrapper-dropdown wrapper-filter" tabindex="1">
				<span class='ingredientesTit'>Ingredientes</span>
				  <ul id="dropIngredientes" class="dropdown">
				   <li> <a data-clase="ingredientes" rel="" href="#">Todos</a></li>
						<?php 
						$persTerm = get_terms( 'ingredient', array(
						    'hide_empty' => true,
						) );

						$uniquePids = array_unique(array_map(function ($i) { return $i['group']; }, WPURP_Taxonomy_MetaData::get( 'ingredient',"", 'group' )));

					sort($uniquePids, SORT_NATURAL | SORT_FLAG_CASE);
						foreach($uniquePids as $term) {
							
						
						
							if($term)
						
								echo('<li> <a data-clase="ingredientes" rel=".'.preg_replace('/\s+/', '',$term).'" href="#">'.$term.'</a></li>');
							
							
						}
						?>
				   </ul>
				</div>
			</div>
			</div>

			
		
	

	</div>

	<div class="actual-category">
	</div>
	<input id="number_of_pages" type="hidden" value="9" />

	<?php if (function_exists('get_most_viewed')): ?>
	    <div id="recetasGrid" class="grid">
	  
	        <?php //$v = get_most_viewed("recipe"); ?><!-- template en wp-postviews plugin buscar Billo-->
	        <?php //include 'category.php' ?>
	        <?php get_template_part( 'loop', 'recetas' ); ?>
	    </div>
	    <div class="pager-list">
						 <!-- Pagination buttons will be generated here -->
		</div>
	<?php endif; ?>
	<div id="suscribete">
             <h2 class="suscribete">Conviértete en un experto del sabor</h2>
             <p>Encuentra aquí ideas que van con tu cocina</p>
	<img src="<?=  get_field('suscribete',$homePost->ID)["sizes"]["suscribete"]?>"/>
	<span class="relSus"><a href="#" class="registro hgrow">Regístrate</a></span>
	<div></div>	
	</div>
		
	</div>
	</div>
</div>	

<?php get_footer(); ?>