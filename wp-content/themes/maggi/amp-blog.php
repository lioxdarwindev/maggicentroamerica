<?php include 'amp/amp-header.php' ?>

    <?php if ( is_single()  ) : ?>
        <div id="content">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h1 class="item"><span class="fn"><?php the_title(); ?></span></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>

<?php include 'amp/amp-footer.php' ?>