<?php include 'amp/amp-header.php' ?>


    <div class="amp-container">
        <div class="amp-header-image">
            <?php
            $amp = new AmpUtils();
            $my_query = null;
            $my_query = new WP_Query(array('post_type' => 'homebanner', 'posts_per_page=1'));
            $homePost = 0;
            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) : $my_query->the_post();
                    $homePost = $post;
                    $image = $amp->get_image_with_alt('banner_mobile', $post->ID, 'banner-mobile');
                    ?>
                    <amp-img layout="responsive" width="667" height="500" alt="<?=  $image->alt ?>"
                             src="<?= $image->src ?>"></amp-img>
                    <?php
                endwhile;
            }
            wp_reset_query();  // Restore global post data stomped by the_post().
            ?>
            <div id="options" class="amp-options">
                <div class="amp-options-container">
                    <a rel="RICO Y SALUDABLE" class="rico-y-saludable" href="">
                        <amp-img width="62" height="62" alt="rico"
                                 src="<?php echo get_bloginfo('template_directory') . "/images/icons/rico.svg"; ?>"></amp-img>


                    </a>
                    <a rel="RÁPIDO Y FÁCIL" class="rapido-y-facil" href="">
                        <amp-img width="62" height="62" alt="rapido"
                                 src="<?php echo get_bloginfo('template_directory') . "/images/icons/rapido.svg"; ?>"></amp-img>
                    </a>
                    <a rel="OCASIONES ESPECIALES" class="ocasiones-especiales" href="">
                        <amp-img width="62" height="62" alt="ocasiones"
                                 src="<?php echo get_bloginfo('template_directory') . "/images/icons/ocasiones.svg"; ?>"></amp-img>
                    </a>
                    <a rel="COCINANDO CON LOS NIÑOS" class="cocinanado-con-los-ninos" href="">
                        <amp-img width="62" height="62" alt="niños"
                                 src="<?php echo get_bloginfo('template_directory') . "/images/icons/ninos.svg"; ?>"></amp-img>
                    </a>
                    <a rel="RECETAS DE NUESTRA TIERRA" class="recetas-de-nuestra-tierra" href="">
                        <amp-img width="62" height="62"
                                 alt="nuestra tierra"
                                 src="<?php echo get_bloginfo('template_directory') . "/images/icons/nuestraTierra.svg"; ?>"></amp-img>
                    </a>
                </div>
                <!--		<h3 class="tit"></h3>-->
            </div>
        </div>

        <div class="separator">
            <amp-img alt="separator" src="<?php echo get_template_directory_uri() ?>/images/icons/home-icon-qch.svg" width="44"
                     height="44"></amp-img>
        </div>
        <div class="amp-headline">
            <h2>¿QUÉ COCINARÉ HOY?</h2>
            <!--                        <p>Selecciona las opciones para buscar tu receta ideal</p>-->
        </div>

        <?php
        $my_query = null;
        $my_query = new WP_Query(array('post_type' => 'recipe', 'posts_per_page' => '5',));

        if ($my_query->have_posts()) {
            while ($my_query->have_posts()) :
                $my_query->the_post();
                $recipe = new WPURP_Recipe($post->ID);
                $categoria = wp_get_post_terms($post->ID, array("category"));
                $favorited = get_user_meta($recipe->ID(), 'wpurp_favorited', true);
                $favoriteCount = ($favorited[0] > "") ? count($favorited) : 0;
                $counter++;
                $amp->print_recipe($counter, $recipe, $post, $categoria, $favoriteCount);
            endwhile;
        }
        wp_reset_query();  // Restore global post data stomped by the_post().
        ?>
        <!-- <div class="amp-subscribe-container">
            <?php // $subscribe_image = $amp->get_image_with_alt('suscribete', $homePost->ID,"suscribete")
            ?>
            <amp-img class="amp-subscribe-image" width="960" height="480" layout="responsive"
                     src="../wp-content/uploads/2016/06/Banner_ComunidadMobile.png" alt="<?= $subscribe_image->alt ?>"></amp-img>
            <a href="<?= home_url('/') . 'registrate/' ?>" class="btn btn-primary">Regístrate</a>
        </div> -->
    </div>


<?php include 'amp/amp-footer.php' ?>