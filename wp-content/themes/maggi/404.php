<html>
	<head>
		<!-- Google Tag Manager -->
		<script async="" src="https://www.google-analytics.com/analytics.js"></script><script id="facebook-jssdk" src="//connect.facebook.net/es_ES/sdk.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-PXFN2V"></script><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PXFN2V');</script>
		<!-- End Google Tag Manager -->

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="https://maggicentroamerica.com/xmlrpc.php">
	        <link rel="stylesheet" type="text/css" media="all" href="https://maggicentroamerica.com/wp-content/themes/maggi/reset.css">
		<link rel="stylesheet" type="text/css" media="all" href="https://maggicentroamerica.com/wp-content/themes/maggi/style.css">
		<!--[if lt IE 9]>
		<script src="https://maggicentroamerica.com/wp-content/themes/maggi/js/html5.js"></script>
		<![endif]-->

	<title>404 Pagina no encontrada</title>
		
	<!-- All in One SEO Pack 2.3.13.2 by Michael Torbert of Semper Fi Web Design[,] -->
	<link rel="canonical" href="https://maggicentroamerica.com/">
	<!-- /all in one seo pack -->
	<link rel="dns-prefetch" href="//www.yummly.com">
	<link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link rel="dns-prefetch" href="//yui-s.yahooapis.com">
	<link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
	<link rel="dns-prefetch" href="//s.w.org">
	<meta property="og:title" content="El sabor que los trae a casa #1"><meta property="og:description" content=""><meta property="og:url" content="https://maggicentroamerica.com/sabor-los-trae-casa-1/"><meta property="og:image" content="https://maggicentroamerica.com/wp-content/uploads/2017/03/Captura-de-pantalla-2014-10-22-a-las-17.49.53-300x175-300x175.png"><meta property="og:type" content="article"><meta property="og:site_name" content="Maggi">

	<script type="text/javascript">
	/* <![CDATA[ */
	var _zm_alr_settings = {"ajaxurl":"https:\/\/maggicentroamerica.com\/wp-admin\/admin-ajax.php","login_handle":"","register_handle":"","redirect":"295","wp_logout_url":"https:\/\/maggicentroamerica.com\/?logout=1&redirect=https:\/\/maggicentroamerica.com","logout_text":"Cerrar sesi\u00f3n","close_text":"Cerrar","pre_load_forms":"zm_alr_misc_pre_load_no","logged_in_text":"Ya estas conectado","registered_text":"Ya estas registrado","dialog_width":"265","dialog_height":"auto","dialog_position":{"my":"center top","at":"center top+5%","of":"body"}};
	/* ]]> */
	</script>

		<script type="text/javascript">if (self === top) {var antiClickjack = document.getElementById("antiClickjack");antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}</script>	

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-58528578-1', 'auto');
			ga('send', 'pageview');
		</script>

	</head>

	<body>
		<header id="masthead" class="site-header body_404" itemscope="itemscope" role="banner">
			<div class="header-main wrap">
				<div id="menu-top">
					<a class="logo" href="https://stag.maggicentroamerica.com/"><img src="https://maggicentroamerica.com/wp-content/themes/maggi/images/logo.svg"></a>
				</div>
			</div>
		</header>

		<div class="page_404">
			<img class="atun_404" src="https://maggicentroamerica.com/wp-content/themes/maggi/images/atun.png">
			<img class="cebollin_404" src="https://maggicentroamerica.com/wp-content/themes/maggi/images/cebollin.png">
			<div class="center_404">
				<img class="" src="https://maggicentroamerica.com/wp-content/themes/maggi/images/404.png">
				<h2>Intenta nuevamente refrescando el sitio desde tu navegador o regresa a nuestra página de <a href="https://stag.maggicentroamerica.com/">inicio</a></h2>
			</div>
			<img class="tomates_404" src="https://maggicentroamerica.com/wp-content/themes/maggi/images/tomates.png">
			<img class="papas_404" src="https://maggicentroamerica.com/wp-content/themes/maggi/images/papas.png">
		</div>

	</body>
</html>



