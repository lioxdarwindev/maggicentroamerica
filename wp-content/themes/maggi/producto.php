<?php get_header(); ?>
<div id="Container">

	<?php
	 global $wp_query;
	$slug = $wp_query->query_vars["producto"];
	$term = get_term_by( 'slug', $slug, "producto");	
	$parent_term = get_term( $term->parent, 'producto' );
	//var_dump($parent_term);


	//var_dump($term);
	?>

	<div id="main">
		<div class="pure-g">

			<div class="pure-u-1 pure-u-md-3-5">
				<div id="prodImage">
				<?php
				echo('<img class="" src="'.get_field( 'imagen_producto','producto_'.$term->term_id)["sizes"]["imagen-producto"].'" alt="">');
				?>
			</div>
			<div class="botonera">
					<div class="thumb">
						<?php echo('<img class="" src="'.get_field( 'image_presentacion_2','producto_'.$term->term_id).'" alt="">');?>	
					</div>
					<div class="thumb">
						<?php echo('<img class="" src="'.get_field( 'imagen_presentacion_3','producto_'.$term->term_id).'" alt="">');?>	
					</div>
				</div>
		</div>


		<div id="prodInfo" class="pure-u-1 pure-u-md-2-5">
			<div class="titulo"><?php echo($term->name);?></div>
			<div class="tituloPadre"><?php echo($parent_term->name);?></div>
			<div class="descripcion"><?php echo(get_field( 'descripcion','producto_'.$term->term_id));?></div>
			<div class="infoPresentacion">
				<div>
					<div class="tit">
						Presentación
					</div>
					<div>
						<?php echo(get_field( 'presentacion','producto_'.$term->term_id));?>
					</div>
				</div>	
				<div>
					<div class="tit">
						<?php echo(get_field( 'titulo_medida','producto_'.$term->term_id));?>
					</div>
					<div>
						<?php echo(get_field( 'medida','producto_'.$term->term_id));?>
					</div>
				</div>
				<div>
					<div class="tit">
						Porciones:
					</div>
					<div>
						<?php echo(get_field( 'porciones','producto_'.$term->term_id));?>
					</div>
				</div>
			</div>
			<div class="tablaProd">
			<?php 
			$nombres = ["","Calorías","Calorías de Grasa","%VD(*)"];
			$cont = 0;
			$table = get_field( 'informacion_nutricional','producto_'.$term->term_id);

				if ( $table ) {

				    echo '<table border="0">';

				      

				            echo '<thead>';

				                echo '<tr>';

				                    foreach ( $nombres as $th ) {

				                        echo '<th>';
				                            echo $th;
				                        echo '</th>';
				                    }

				                echo '</tr>';

				            echo '</thead>';
				        

				        echo '<tbody>';

				            foreach ( $table['body'] as $tr ) {

				                echo '<tr>';

				                    foreach ( $tr as $td ) {

				                        echo '<td>';
				                            echo $td['c'];
				                        echo '</td>';
				                    }

				                echo '</tr>';
				            }

				        echo '</tbody>';

				    echo '</table>';
				}

			?>
				
			</div>
				


			</div>

			<div class="recetasSugeridas">
					<?php
						$args = array(
							'DisableFilterPais' => true,
						    'post_type' => 'recipe',
						    'post_status' => 'publish',
							'posts_per_page' => 2,
						    'tax_query' => array(
						        array(
						            'taxonomy' => 'producto',
						            'field' => 'slug',
						            'terms' => $slug
						        ),
						        array(
						            'taxonomy' => 'pais',
						            'field' => 'slug',
						            'terms' => $_SESSION['pais']
						        )
						    )
						);
						


						   $custom = new WP_Query( $args );
						 ?>	
						    
						   <div id="recetasGrid" class="grid">
								<?php // include  'category.php'; ?>
								<?php get_template_part( 'loop', 'recetas' ); ?>
						   </div>

				</div>


		</div>
		
		</div>

	</div>


<?php get_footer(); ?>