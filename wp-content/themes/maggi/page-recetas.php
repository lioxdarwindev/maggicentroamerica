<?php
/**
 * Template Name: Recetas
 *
 */
 get_header(); ?>

 <!-- XXX -->

<div id="Container">
	<div id="main">

	<div id="banner" class="banner-recipes">

		<span class="hid"> 
			<span class="banrep">
				<img class="desktop owl-lazy imgbanner" data-src="https://maggicentroamerica.com/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-1-960x150.png"/>
				<img class="mobile owl-lazy imgbanner" data-src="https://maggicentroamerica.com/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-2-267x150.jpg"/>
				<span class="overl">	
				</span>
				<img class="owl-lazy ornament" data-src="<?php echo get_bloginfo('template_directory').'/images/ornamento.png' ?>">
			</span>
					
		</span>

	</div>
		<div id="categorias" class="pure-g">
			<div class="pure-u-1-1 pure-u-md-1-1 revisaMensaje">
				<h3 class="boletinTitle">ENCUENTRA AQUÍ DELICIOSAS RECETAS <br> QUE VAN CON TU COCINA</h3>
			</div>
			<?php 

			$cat = $_GET['cat'];
			$mainCategories = get_terms( 'category', array( 'parent' => 0  , 'hide_empty' => false , 'exclude' => '1, 390, 391, 392, 393') );
			?>


			<?php
			//echo '<div class="pure-u-1-2 pure-u-md-1-6"><a class="post-link cat_link"  href="#" rel=""><img src="'.get_bloginfo('template_directory').'/images/flecha_cat.svg"/><p>Categorías</p></a></div>';
			foreach( $mainCategories as $category ) {
				

					 $term_link = get_term_link( $category );
					 $activo = "";
				
						if($cat == $category->slug){

							$activo = "activo";
						}
				 	echo '
				 		<div class="pure-u-1-2 pure-u-md-1-5">
				 			<a id="'.$category->slug.'"   href="'.esc_url( $term_link ) .'" rel="">'.file_get_contents(get_template_directory()."/images/".$category->slug.".svg").'<p>' . $category->name . '</p></a></div>';
			
			}
			?>
			
		</div>

	
</div>
<?php get_footer(); ?>