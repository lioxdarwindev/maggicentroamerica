<?php get_header(); ?>
<div id="Container">
	<div id="main">

		<div id="banner" class="banner-recipes">

			<span class="hid"> 
				<span class="banrep">
				<img class="desktop owl-lazy imgbanner" data-src="https://maggicentroamerica.com/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-1-960x150.png"/>
				<img class="mobile owl-lazy imgbanner" data-src="https://maggicentroamerica.com/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-2-267x150.jpg"/>
					</span>
					<img class="owl-lazy ornament" data-src="<?php echo get_bloginfo('template_directory').'/images/ornamento.png' ?>">
				</span>
						
			</span>

		</div>

		<div id="categorias" class="pure-g">
			<div class="pure-u-1-1 pure-u-md-1-1 revisaMensaje">
				<h3 class="boletinTitle ams_red"><?php single_cat_title(''); ?></h3>
			</div>
		</div>

		<input id="number_of_pages" type="hidden" value="9" />
		
	    <div id="recetasGrid" class="grid">
	        <?php get_template_part( 'loop', 'recetas' ); ?>
	    </div>

	    <div class="pager-list">
						 <!-- Pagination buttons will be generated here -->
		</div>

	</div>
	
</div>
<?php get_footer(); ?>