<div id="categorias" class="pure-g">
			<?php 
			$mainCategories = get_terms( 'category', array( 'parent' => 0  , 'hide_empty' => false , 'exclude' => '1') );
			?>


			<?php
			//echo '<div class="pure-u-1 pure-u-md-1-2"><a class="post-link cat_link"  href="#" rel=""><img src="'.get_bloginfo('template_directory').'/images/flecha_cat.svg"/><p>Categorías</p></a></div>';
			foreach( $mainCategories as $category ) {
					 $term_link = get_term_link( $category );
				 	echo '<div class="pure-u-1 pure-u-md-1-2"><a id="'.$category->slug.'" class="post-link"  href="#" rel="'.esc_url( $term_link ) .'">'.file_get_contents(get_bloginfo('template_directory')."/images/".$category->slug.".svg").'<p>' . $category->name . '</p></a></div>';
			}
			?>
		</div>


		<div id="subcategorias" class="controls">
			<?php 
			
			
			foreach( $mainCategories as $category ) {
				//var_dump($category->term_id);
				echo '<div class="sub-'.$category->slug.'">';

				
				 $subcategories = get_terms( 'category', array( 'parent' => $category->term_id, 'hide_empty' => true ) );
				
				foreach( $subcategories as $subcategory ) {
				 echo( '<span class="filter" data-filter=".'.$subcategory->slug.'">'.$subcategory->name.'</span>');
			
				}
				// var_dump($subcategory);
				echo '</div>';
				 	//echo '<a href="#" rel="' . $category->slug . '">' . $category->name . '</a>';
			}
			?>

		</div>	


		<a class="busquedaAvanzadaLabel" href="#">Búsqueda Avanzada<p class="arrow right"></p></a>
		<div id="busquedaAvanzada">

		
			<!-- SORT -->
			<div class="wrapper-dropdown wrapper-sort" tabindex="1">
			
				<span>Búsqueda Por:</span>
				  <ul class="dropdown">
				  		<li><a rel="reciente:desc" href="#">Recientes</a></li>
						<li><a rel="fav:desc" href="#">Populares</a></li>
				   </ul>
			</div>
			<!-- ingredientes -->
			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">

				<span class="ingredientesTit">Ingredientes</span>
				  <ul class="dropdown">
				   <li> <a data-clase="ingredientes" rel="" href="#">Todos</a></li>
						<?php 
						$persTerm = get_terms( 'ingredient', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="ingredientes" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
			</div>


			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">
				<span class="tiempoTit">Tiempos de Comida</span>
				  <ul class="dropdown">
				  <li> <a data-clase="tiempo" rel="" href="#">Todos</a></li>
						<?php 
						$persTerm = get_terms( 'momento', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="tiempo" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
			</div>


			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">
			
				<span class="dificultadTit">Dificultad</span>
				  <ul class="dropdown">
				   <li> <a data-clase="dificultad" rel="" href="#">Todas</a></li>
						<?php 
						$persTerm = get_terms( 'course', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="dificultad" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
			</div>
			
		</div>
        	<div id="recetasGrid" class="grid">
		<?php
			wp_reset_query();
			$args = array(
							'DisableFilterPais' => true,
						    'post_type' => 'recipe',
						    'post_status' => 'publish',
						   'posts_per_page' => 4,
						    'tax_query' => array(
						
						        array(
						            'taxonomy' => 'pais',
						            'field' => 'slug',
						            'terms' => $_SESSION['pais']
						        )
						    )
						);


						   $custom = new WP_Query( $args );

			include  'category.php';
		?>
		</div>
		<div class="pager-list">
						 <!-- Pagination buttons will be generated here -->
		</div>
           