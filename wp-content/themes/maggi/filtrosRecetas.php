<div id="recetasGrid" class="grid">
		<?php
			wp_reset_query();
			$args = array(
							'DisableFilterPais' => true,
						    'post_type' => 'recipe',
						    'post_status' => 'publish',
						   'posts_per_page' => 4,
						    'tax_query' => array(
						
						        array(
						            'taxonomy' => 'pais',
						            'field' => 'slug',
						            'terms' => $_SESSION['pais']
						        )
						    )
						);


						   $custom = new WP_Query( $args );

			//include  'category.php';
			get_template_part( 'loop', 'recetas' );
		?>
		</div>
		<div class="pager-list">
						 <!-- Pagination buttons will be generated here -->
		</div>
<div id="categoriasSide" class="pure-g">
			<?php 
			$mainCategories = get_terms( 'category', array( 'parent' => 0  , 'hide_empty' => false , 'exclude' => array( 1, 390 )) );
			?>


			<?php
			//echo '<div class="pure-u-1 pure-u-md-1-2"><a class="post-link cat_link"  href="#" rel=""><img src="'.get_bloginfo('template_directory').'/images/flecha_cat.svg"/><p>Categorías</p></a></div>';
			foreach( $mainCategories as $category ) {
					 $term_link = get_term_link( $category );
				 	echo '<div class="pure-u-1 pure-u-md-1-2"><a id="'.$category->slug.'"  href="'.esc_url( $term_link ) .'?post_type=recipe&cat='.$category->slug.'" rel="'.esc_url( $term_link ) .'">'.file_get_contents(get_bloginfo('template_directory')."/images/".$category->slug.".svg").'<p>' . $category->name . '</p></a></div>';
			}
			?>
		</div>

        	
           