<?php get_header(); 

?>
<div id="Container">
	<div id="main">
		<div class="pure-g">
			<div class="back-saboT pure-u-1-1 pure-u-md-1-2">	
				<?php

				    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

				     echo('<img class="Sabo-principal" src="'.get_field('imagen_usuario_principal',$curauth)["sizes"]["saboreador-big"].'"/>');
				  $play = get_field('video',$curauth);
				  if($play){
				  	echo('<a href="'.$play.'" class="vp-a play"><img src="'.get_bloginfo('template_directory').'/images/icons/play.svg"></a>');
				  }
				?>
				
					<img class='sabo_titulo' src="<?=  get_bloginfo('template_directory')?>/images/saboreadores-titulo2.png">
			</div>	
			<div class="pure-u-3-4 pure-u-md-1-2 info-der">
				<span>
				<?php

					echo('<img src="'.get_field('imagen_titulo_usuario',$curauth)["sizes"]["saboreador-big"].'"/>');
					echo('<p>'.get_field('descripcion_corta_bajo_titulo',$curauth).'</p>');
					echo("<span class='social'>".do_shortcode ('[feather_share skin="wheel"]')."</span>");
				?>
				</span>
			</div>
			<div class="pure-u-1-1 desc sep">
			<?php 
				the_author_meta('description');
			?>	
			</div>	
			<div class="pure-u-1-1 pure-u-md-1-2 resum">
				<div class="edad gback"><span class="icon red"> <img src="<?=  get_bloginfo('template_directory')?>/images/icons/cake.svg"/></span><span class="datos"><p>Edad</p><p><?php echo(get_field('edad',$curauth));?></p></span></div>
				<div class="pais"><span class="icon yellow"> <img src="<?=  get_bloginfo('template_directory')?>/images/icons/pais.svg"/></span><span class="datos"><p>Especialidad</p><p><?php echo(get_field('especialidad',$curauth));?></p></span></div>	
			</div>
			<div class="pure-u-1-1 pure-u-md-1-2 resum">
				<div class="experiencia gback"><span class="icon red"><img src="<?=  get_bloginfo('template_directory')?>/images/icons/exp.svg"/></span><span class="datos"><p>Experiencia</p><p><?php echo(get_field('experiencia',$curauth));?></p></span></div>
				<div class="profesion"><span class="icon yellow"><img src="<?=  get_bloginfo('template_directory')?>/images/icons/prof.svg"/></span ><span class="datos"><p>Profesión</p><p><?php echo(get_field('profesion',$curauth));?></p></span></div>
			</div>



			<?php 

			wp_reset_query();
				$args = array(
				    'author'        =>  $curauth->ID,
				    'orderby'       =>  'post_date',
				    'order'         =>  'DESC'
				    );

				$posts = new WP_Query($args);
				$posts =($posts->posts);
				
				
				

				 $frases = preg_split('/<br[^>]*>/i', get_field('frases',$curauth));
				 if(get_field('frases',$curauth)){
			//	var_dump($recipe);
			?>

			<div class="pure-u-1-1 sep">
				 <div  class="pure-g intruccionesQCH">
						<div class="pure-u-9-24 pure-u-md-10-24 linea"></div>
						<div class="pure-u-6-24 pure-u-md-2-24 icon"><img src="<?php echo(get_field('imagen_frases',$curauth)["sizes"]["saboreador-big"]);?>"></div>
						<div class="pure-u-9-24 pure-u-md-10-24  linea"></div>
						<div class="pure-u-1-1 sep">
							<div id="frases">
						<?php
							
							 foreach ($frases as &$value) {
							  	$frase = explode(",",$value);
							   echo("<div>"."<h3>".$frase[0] ."</h3>"."<p>".$frase[1]."</p>"."</div>");
							}
							?>
							
							</div>
							
						</div>

					</div>

			</div>
			<?php 
			}
			if($posts){
				$recipe = new WPURP_Recipe($posts[0]->ID); 
				$categoria = wp_get_post_terms($posts[0]->ID,array("category"));
			?>
				<div id="receta_athor_prin" class="pure-g">
					<div class='pure-u-1-1 pure-u-md-2-3 postVideo'>
						<a href="<?php echo($post->guid)?>">
						 <?php 
						 $custom = get_post_custom($post_id);

							$video = $custom["url_video_youtube"];
					

						echo("<img  src='".$recipe->featured_image_url("imagen-producto")."'/>");
						if($video[0] ){
							echo("<span class='play'>".file_get_contents(get_bloginfo('template_directory')."/images/icons/play.svg")."</span>");
						}
						$fav = new WPURP_Favorite_Recipes();
						?>
						
						</a>
					</div>
					<div class='pure-u-1-1 pure-u-md-1-3 info'>

						<p class="tituloCat"><?php echo($categoria[0]->name);?></p>
						<p class="titulo"> <b> <?php echo($recipe->title());?> </b> </p>
						<div class="notas"><?php echo($recipe->notes());?></div>
						<div class="views_info"><span class="views"><?php echo(the_views(false)); ?></span><span class="fabs"><?php echo($fav->cantidad_favorites($recipe->ID()));?></span>

				 		</div><a class="ver_receta_btn" href="<?php echo($post->guid)?>">Ver Receta Completa</a>
					</div>
					<?php 
			for ($i = 1; $i <= sizeof($posts)-1; $i++) {
				//$im =  wp_get_attachment_image_src( get_post_thumbnail_id( $posts[i]->ID ),"author-recipe-small" );
				$recipeU = new WPURP_Recipe($posts[$i]->ID); 
				$imagen = $recipeU->alternate_image_url("thumbBig");
				$custom = get_post_custom($post_id);
				$video = $custom["url_video_youtube"];
				$vcus ="";
				if($video[0]){$vcus = "<span class='playThumb'>".file_get_contents(get_bloginfo('template_directory')."/images/icons/play.svg")."</span>";}
			   	echo("<div class='pure-u-1-2 pure-u-md-1-8 resOct postVideo'><a href='".$posts[$i]->guid."'><img  src='".$imagen."'/>".$vcus."</a></div>");
			   	if($video ){
						
				}
			}
			?>
				</div>
		
			<?php

			 $custom = new WP_Query( $args );
						 ?>	
				</div>	    
				<div id="recetasGrid" class="grid">
					
					<?php // include  'category.php'; ?>
					<?php  get_template_part( 'loop', 'recetas' ); ?>


				</div>
				<?php }?>
				<div class="pure-u-1-1 sep">
				 <div  class="pure-g intruccionesQCH">
						<div class="pure-u-10-24"></div>
						<div class="pure-u-2-24 icon"><img src="https://tellinginteractive.com/demos/maggi/wp-content/themes/maggi/images/icons/sabo.svg"></div>
						<div class="pure-u-10-24"></div>
						<div class="pure-u-1-1 sep">
							<h3>ESTILOS EN LA COCINA</h3>
							</br>
							<p>¿Cuál saboreador te gustaría conocer?</p>
						</div>

					</div>

			</div>
				<div>
			
					<?php  $args = array(
							'role'         => 'author',
							'exclude'      => array($curauth->ID),
						 ); 
						$autores = get_users( $args ); 

						foreach ( $autores as $autor ) {
							
									
								echo('<div class="back-sabo pure-u-1-1 pure-u-md-1-2"><a href="'.get_author_posts_url($autor->id,$autor->user_nicename).'"><div class="tp">');
								echo('<span class="pure-u-1-2"><img src="'.get_field('imagen_usuario_principal',$autor)["sizes"]["saboreador-big"].'"/></span>');
								echo('<div class="pure-u-1-2"><img class="t" src="'.get_bloginfo('stylesheet_directory').'/images/saboreadores-titulo2.png"/></div></div>');
								echo('<div class="pure-u-1-1 titu"> <img src="'.get_field('imagen_titulo_usuario',$autor)["sizes"]["saboreador-big"].'"/><span><img src="'.get_bloginfo('template_directory').'/images/icons/flecha-der.svg"></span></div>');
								echo('<div class="pure-u-1-1 desc"> <p>'.get_the_author_meta('description',$autor->id).'</p> </div>');

								echo('</a></div>');
							
						}

					?>
		

		</div>	
	</div>
</div>
<script>jQuery(document).ready(function( $ ) {
			setMenu($,"#menu-item-103");
		});
</script>

<?php get_footer(); ?>