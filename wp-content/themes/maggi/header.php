<?php 
	$username = 'maggi_newsite';
	$password = 'N3w-$I+E._Accs';
	 
	$context = stream_context_create(array(
	    'http' => array(
	        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
	    )
	));

//loop paises
	$Paises = ("var paisS = '".htmlspecialchars($_SESSION['pais'])."';var ddData = [");
			$terms = get_terms( 'pais' , array( 'hide_empty' => false));

		            foreach ( $terms as $term ) {
		            	$Paises .=("{text:'', value:'".$term->slug."', imageSrc: '".get_bloginfo('template_directory')."/images/flags/".$term->slug.".svg'");
		            	if($_SESSION['pais'] == $term->slug) {$Paises .= ',selected:true';
						}
						$Paises .=("},");
		            }
	$Paises .= ("];
		
		");

	$user_id = get_current_user_id();
	  

	 if( $user_id !== 0 ) {
            $favorites = get_user_meta( $user_id, 'wpurp_favorites', true );
            $favorites = is_array( $favorites ) ? $favorites : array();
           
     } 
?>

<!DOCTYPE html Cache-Control:private;>
<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<html class="no-js magisite">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PXFN2V');</script>
	<!-- End Google Tag Manager -->

	<title></title>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png"/>

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/reset.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=2.15" />
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->

	<?php wp_head(); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style id="antiClickjack">body{display:none !important;}</style><script type="text/javascript">if (self === top) {var antiClickjack = document.getElementById("antiClickjack");antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}</script>	

	<script type="application/ld+json">
{
"@type": "Organization",
"name": "Maggi® Centroamérica",
"legalName" : "Maggi®",
"url": "https://maggicentroamerica.com",
"logo": "https://maggicentroamerica.com/wp-content/themes/maggi/images/logo.svg",
"description": "Creemos juntos deliciosos platillos con ingredientes disponibles en tu país. Mantengamos la tradición de cocinar en casa con el sabor único de Maggi®.",
"contactPoint": {
"@type": "ContactPoint",
"contactType": "customer support",
"telephone": "+507 800-0000",
"email": "servicios.consumidor@pa.nestle.com"
},
"sameAs": [
"https://www.instagram.com/maggicentroamerica/",
"https://www.facebook.com/MaggiCentroamerica/",
"https://www.youtube.com/channel/UCla3XXd-Q386Sgr0aNe4rKA"
]}
</script>
	<meta name="google-site-verification" content="TiN3mqAa8vEGJ-VwkeWT0zqfv2McAKhJEoF1ujoNlA8" />

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-58528578-1', 'auto');
		ga('send', 'pageview');
	</script>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PXFN2V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header id="masthead"class="site-header" itemscope="itemscope" role="banner">

<div class="header-main wrap">
	<div id="menu-top">
			<a class="logo" href="<?php echo get_home_url();?>"> <img src="<?=  get_bloginfo('template_directory')?>/images/logo.svg"></a>

			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<form class="PSelectM mobile" action="" method="POST">
						<script type="text/javascript">
						var templateUrl = '<?= get_bloginfo("template_url"); ?>';
							<?php //loop paises
								echo($Paises);
							?>
						</script>
						<div class="pais" name="pais"></div>
				</form>

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); 

				?>	
				
			</nav>
			
			<div class="menu-sec">
			
			<form class="PSelect desktop" action="" method="POST">
			<script type="text/javascript">

				 var favUrl = '<?= get_page_link(161); ?>';
				

				<?php //loop paises
					echo($Paises);
				?>
			</script>
			<div class="pais" name="paiss"></div>
			
			</form>
			
				<a class="fb soc" target="_blank" href="https://www.facebook.com/MaggiCentroamerica/">
					<?php echo file_get_contents(get_template_directory()."/images/icons/fb.svg", false); ?>
				</a>
				<a class="inst soc" href="https://www.instagram.com/maggicentroamerica/" target="_blank" href="#">
					<?php echo file_get_contents(get_template_directory()."/images/icons/inst.svg", false); ?>
				</a>
				<a class="yt soc" target="_blank" href="https://www.youtube.com/channel/UCla3XXd-Q386Sgr0aNe4rKA">
					<?php echo file_get_contents(get_template_directory()."/images/icons/yt.svg", false); ?>
				</a>

			</div>
	</div>	

	<span class="desktop mid-show">
		<?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu_class' => 'nav-menu', 'menu_id' => 'secundary-menu' ) ); ?>
		
	</span>

<span class="desktop mid-hide" id="search-contact">
	<?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu_class' => 'nav-menu', 'menu_id' => 'secundary-menu' ) ); ?>
	<div id="hablanos" class="desktop hablanos"><?php
		echo file_get_contents(get_bloginfo('template_directory')."/images/icons/hablenos.svg", false, $context); ?>Háblenos</div>
</span>
<span class="dialogo"><span class="log"></span></span>
<div id="searchContainer">
  <div class="busqueda"><?php get_search_form(); ?></div>
</div>
<script>
						<?php 
							 global $current_user;
							 get_currentuserinfo();

								if($current_user->user_firstname){
									echo('nombre = "'.$current_user->user_firstname .'";');
									
								}else{
										
									if($current_user->user_login){
									echo('nombre = "'.$current_user->user_login .'";'	);
									}
								}
							 echo('num_fav = '.count($favorites));
      						
						?>	
</script>

</div>
<div class="menuIco responsive-nav-icon"></div>
<div class="searchMov mobile"></div>
</header>