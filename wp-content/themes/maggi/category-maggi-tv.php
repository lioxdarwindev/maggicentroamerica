<?php include("header.php"); ?>
	<section>
		<div class="video_header">
			<?php

				$my_query = null;
				$my_query = new WP_Query( array('post_type' => 'homebanner' , 'name' => 'banner-maggi-tv' ,'posts_per_page=1'));

				if( $my_query->have_posts() ) {
			  		while ($my_query->have_posts()) : $my_query->the_post();
			  		$homePost = $post; 
			  		?>
			  		<img class="desktop" src="<?=  get_field('banner',$post->ID)["sizes"]["banner-home"]?>"/>
			  		<img class="mobile" src="<?=   get_field('banner_mobile',$post->ID)["sizes"]["banner-mobile"]?>"/>
			         <h1>¿Cómo preparar tus recetas favoritas?</h1>	
				  	<?php
				 	endwhile;
					}
				wp_reset_query();  // Restore global post data stomped by the_post().
			?>
		</div>
        <div class="video_container">
        	<div class="row">
		        <div class="tabs_search">
		        	<div class="tab comerciales_link <?php echo get_queried_object()->cat_name=="Comerciales"?"selected":'' ?>"><a href="<?php echo get_category_link(391); ?>">Comerciales</a></div>
		        	<div class="tab recetas_link <?php echo get_queried_object()->cat_name=="Recetas"?"selected":'' ?>"><a href="<?php echo get_category_link(393); ?>">Recetas</a></div>
		        	<div class="tab promociones_link <?php echo get_queried_object()->cat_name=="Promociones"?"selected":'' ?>"><a href="<?php echo get_category_link(392); ?>">Promociones</a></div>
		        </div>
		        <div class="search_videos">
	        		<form method="get" action="<?php echo 'http://'.filter_var($_SERVER[HTTP_HOST], FILTER_SANITIZE_STRING).filter_var($_SERVER[REQUEST_URI], FILTER_SANITIZE_STRING);  ?>">
	        			<span></span>
			        	<input type="search" name="search_term" id="search_term" placeholder="Búsqueda por blog" autocomplete="off"/>
	        		</form>
		        </div>
	        </div>
	        <?php
	        	$posts_per_page = -1;	// cantidad de videos mostrados
	        	if(get_search_query() == ""){
		        	$query = new WP_Query(array(
		            	"cat" => get_queried_object()->cat_ID,
		            	"posts_per_page" => $posts_per_page,
		            	'orderby'           => 'date',
    					'order'             => 'ASC'
		        	));
	        	}else{
	        		$query = new WP_Query(array(
		            	"cat" => get_queried_object()->cat_ID,
		            	"posts_per_page" => $posts_per_page,
		            	"s" => get_search_query(),
		            	'orderby'           => 'date',
    					'order'             => 'ASC'
		        	));
	        	}
				$array_videos=array();
				$max_posts = 15;
				if( $query->have_posts() ) {
					$total_posts = 0;
					while ( $query->have_posts() ) {
						$query->the_post();

						$array_videos[$total_posts]["content"] = get_the_content();
						$thumb_id = get_post_thumbnail_id();
				        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
						$array_videos[$total_posts]["thumb"] = $thumb_url[0];
						$array_videos[$total_posts]["title"] = get_the_title();
						$array_videos[$total_posts]["id"] = get_the_ID();
						$total_posts++;
					}
				}

				$current_pointer = !$_GET["page"] ? 0 : intval($_GET["page"]-1) * 3;
				$main_video = $array_videos[$current_pointer];
	        ?>
	        <div class="video_main">
	        	<iframe width="100%" height="315" src="//www.youtube.com/embed/os8IDZlCTG4" frameborder="0" allowfullscreen></iframe>
	        </div>
	        <div class="social_videos" data-id="<?php echo $main_video["id"] ?>">
	        	<h3>Comparte en</h3>
	        	<div class="fb">
			        <a class="synved-social-button synved-social-button-share synved-social-size-48 synved-social-resolution-single synved-social-provider-facebook nolightbox" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode("https:".$main_video["content"]);?>">
	        	          <img class="synved-share-image synved-social-image synved-social-image-share" src="https://maggicentroamerica.com/wp-content/plugins/social-media-feather/synved-social/image/social/regular/96x96/facebook.svg" />
	        	    </a>
	        	</div>
	        	<div class="tw">
		        	<a class="synved-social-button synved-social-button-share synved-social-size-48 synved-social-resolution-single synved-social-provider-twitter nolightbox" target="_blank" href="http://twitter.com/share?url=<?php echo "http:".$main_video["content"];  ?>&text=<?php echo $main_video["title"]; ?>">
		        		<img class="synved-share-image synved-social-image synved-social-image-share" src="https://maggicentroamerica.com/wp-content/plugins/social-media-feather/synved-social/image/social/regular/96x96/twitter.svg" />
		        	</a>
	        	</div>
	        	<div class="pin">
		        	<a class="synved-social-button synved-social-button-share synved-social-size-48 synved-social-resolution-single synved-social-provider-pinterest nolightbox" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo "http:".$main_video["content"];  ?>/&media=<?php echo $main_video["thumb"]?>&description=<?php echo $main_video["title"]; ?>">
		        		<img class="synved-share-image synved-social-image synved-social-image-share" src="https://maggicentroamerica.com/wp-content/plugins/social-media-feather/synved-social/image/social/regular/96x96/pinterest.svg" />
		        	</a>
	        	</div>
	        </div>
	        <div class="videos_container">
	        	<?php
	        		for($i=$current_pointer;$i<count($array_videos);$i++) { 
	        			if($max_posts!=0):	
	        		?>
						<div data-id="<?php echo $array_videos[$i]["id"] ?>" data-share-pin="https://pinterest.com/pin/create/button/?url=<?php echo "http:".$array_videos[$i]["content"];  ?>/&media=<?php echo $array_videos[$i]["thumb"]?>&description=<?php echo $array_videos[$i]["title"]; ?>" data-share-tw="http://twitter.com/share?url=<?php echo "http:".$array_videos[$i]["content"];  ?>&text=<?php echo $array_videos[$i]["title"]; ?>" data-share-fb="https://www.facebook.com/sharer/sharer.php?u=<?php echo $array_videos[$i]["content"];?>" data-reference="<?php echo $array_videos[$i]["content"]; ?>">
							<?php echo "<img src=\"".$array_videos[$i]["thumb"]."\">"; ?>
						</div>
					<?php 
						$max_posts--;
						endif;
					}
					$max_posts=15;
					if( (count($array_videos)/$max_posts) > 1 ){
						echo "<div class=\"pagination\">";
						$pagination_number = ceil((count($array_videos)/$max_posts));
						$current = 1;
							if(isset($_GET['page'])){
								if($_GET['page'] > 6){
									$i=($_GET["page"]-6)+1;
								}else{
									$i=1;
								}
								$valor=$_GET['page']+1;
								if($valor > $pagination_number){
									$valor=$pagination_number;
									}
							}else{
									$i=1;$valor=2;
							}
							
							$contador=1;
							
							if($valor > 6){
							
								$result=$valor-2;
								if($result > 5){
									echo "<a href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$result."\"><</a>";				
									}
							}
								
							for($i;$i <= $pagination_number;$i++){
								
							  if($contador < 7){
								  if($_GET["page"] && $i==$_GET["page"]){
									echo "<a class=\"current_pag\" href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$i."\"><span>".$i."</span></a>";
								}else{								
									echo "<a href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$i."\"><span>".$i."</span></a>";
								}
							  }
							  $contador++;
							}
							
							if($pagination_number > 6 && $valor < $pagination_number  ){
								echo "<a href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$valor."\">></a>";				
							}
							/*
						while( $pagination_number > 0 ){
							if(cleanRequest($_GET["page"]) && $current==cleanRequest($_GET["page"])){
								echo "<a class=\"current_pag\" href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$current."\">".$current."</a>";
							}else{								
								echo "<a href=\"".get_category_link(get_queried_object()->cat_ID)."?page=".$current."\">".$current."</a>";
							}
							$pagination_number--;
							$current++;
						}*/
						echo "</div>";
					}
				?>
	        </div>	
        </div>
	</section>
<?php include("footer.php") ?>