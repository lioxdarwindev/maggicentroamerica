
var textoTit = '';
function setMenu($,clase){
  $(clase).addClass("current-menu-item");
}

jQuery(document).ready(function( $ ) {
	// Insert Responsive Navigation Icon, Close Icon, and Overlay
	// If you have access to your HTML, you should put this directly into your markup.
	

  $('.close-search').on('click', function(){
      $("#searchContainer").removeClass("menuCerrado");
      $(".busqueda").removeClass("abierto");
      $(".buscar_icon").show();
     });



	$('<div class="responsive-nav-close" />').appendTo('nav');
	

	// Navigation Slide In
	$('.responsive-nav-icon').click(function() {
		$('nav').addClass('slide-in');
    $('body').addClass("overflow-abierto");
		//$('#overlay').show();
		return false;
	});

	// Navigation Slide Out
	$('.responsive-nav-close').click(function() {
		$('nav').removeClass('slide-in');
    $('body').removeClass("overflow-abierto");
		
	//	$('#overlay').hide();
		return false;
	});


 var owl = $("#banner");

 owl.on('loaded.owl.lazy',function(e){
  setTimeout(function(){
     $(".hid p, .hid .pure-g").show("slow");

  }, 500);
    });

var boolean = true;
 if(owl.hasClass("banner-recipes")){
  boolean = false;
 }
 
   
    owl.owlCarousel({
        items:1,
        navigation:true,
        lazyLoad:true,
        nav: boolean,
        navText: [
          "<i class='fa fa-chevron-left'></i>",
          "<i class='fa fa-chevron-right'></i>"
        ],
        pagination: false,
        dots: false

    });   

 
 if($("#frases").length){
    var owlFrases = $("#frases");
    owlFrases.owlCarousel({
         items:1,
         singleItem: true,
        pagination: true,
        dots:true
    });
 }


  
 $('.ProductoGroup .productos-carousel').owlCarousel({
    margin:10,
    responsiveClass:true,
    scrollPerPage : true,
    loop:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            slideBy: 1
        },
        600:{
            items:3,
            nav:true,
            slideBy: 3
        },
        1000:{
            items:5,
            nav:true,
            slideBy: 5
        }
    },
      navText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>"
      ],
 
}); 





   $('.PSelect .pais , .PSelectM .pais').ddslick({
      data: ddData,
      width: 200,
      imagePosition: "left",
      selectText: "",
      onSelected: function (data) {  
     	var parentForm = data.selectedItem.parent().parent().parent();
            if (paisS != $(parentForm).find(".dd-option-selected input").val()) {
         		parentForm.submit();
        	 }
      }
  });

   $(".ProductoGroup .inner , .ProductoGroup > span > img").click(function(e) {

    padre = $(e.currentTarget).parents('.ProductoGroup');

    
    if($(padre).hasClass("open")){
       $(".ProductoGroup").removeClass("open");
    }else{  
        $(".ProductoGroup").removeClass("open");
        $(padre).addClass("open");
    }
    
      e.preventDefault();
    
    
   });

     $(".buscar_icon").click(function(e) {
      $(".buscar_icon").hide();
     $("#searchContainer").addClass("menuCerrado");
    // $(".buscar_icon").before('<li class="busqueda"><form><input type="text" name="search"><input type="submit" value="Submit"></form></li>'); 
     });









});