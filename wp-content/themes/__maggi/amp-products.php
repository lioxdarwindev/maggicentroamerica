<?php include 'amp/amp-header.php' ?>

    <div class="amp-container">
        <div class="amp-products">
            <?php
            $nuevos = array();
            $mainProductosT = get_terms('producto', array(
                'parent' => 0,
                'hide_empty' => false,
                'exclude' => 85
            ));

            $mainProductos = productos_pais($mainProductosT);
            //        print_r($mainProductos);

            foreach ($mainProductos as $mainProducto) {
                $productosT = get_terms('producto', array('parent' => $mainProducto->term_id, 'hide_empty' => false));
                $productos = productos_pais($productosT);
                if (sizeof($productos)) {
                    ?>
                    <div class="amp-products-group">
                        <amp-accordion>
                            <section>
                                <?php $imagen_mobile_producto = $amp->get_image_with_alt('imagen_mobile_producto', 'producto_' . $mainProducto->term_id, "banner-category-mobile")?>
                                <h2 class="amp-products-group-hero">
                                    <amp-img class="amp-products-group-image" width="375" height="183"
                                             layout="responsive"
                                             alt="<?= $imagen_mobile_producto->alt ?>"
                                             src="<?= $imagen_mobile_producto->src ?>">
                                    </amp-img>
                                    <div class="amp-products-group-count">
                                        <p><?= sizeof($productos) . " productos " ?></p>
                                        <a href="" class='amp-products-group-expand'>
                                            <p class='fa fa-chevron-down'></p>
                                        </a>
                                    </div>
                                </h2>
                                <amp-carousel width="400"
                                              height="300"
                                              layout="responsive"
                                              type="slides"
                                              class="amp-products-carousel">


                                    <?php

                                    foreach ($productos as $producto) {
                                        $Nuevo_temp = "";
                                        if (get_field('nuevo', 'producto_' . $producto->term_id)) {
                                            $Nuevo_temp = get_bloginfo('template_directory') . '/images/icons/nuevo_prod.svg';
                                            array_push($nuevos, $producto);
                                        }


                                        ?>
                                        <a href="<?= get_term_link($producto) ?> "
                                           class="amp-products-carousel-item">
                                            <?php
                                            if ($Nuevo_temp != "") {
                                                ?>
                                                <amp-img  alt="nuevo producto" width="243" height="403"  layout="responsive" class="amp-products-carousel-item-new" src="<?= $Nuevo_temp ?>" ></amp-img>
                                                <?php
                                            }
                                            $imagen_producto = $amp->get_image_with_alt('imagen_producto', 'producto_' . $producto->term_id, "medium_large");
                                            if ($imagen_producto->src){
                                            ?>
                                            <amp-img class="" width="243" height="403"
                                                     src="<?= $imagen_producto->src ?>" alt="<?= $imagen_producto->alt ?>"></amp-img>
                                            <?php }?>
                                            <p><?= $producto->name ?></p>
                                        </a>

                                        <?php

                                    }
                                    ?>

                                </amp-carousel>
                            </section>
                        </amp-accordion>
                    </div>


                    <?php
                }

                //novedades


            }


            $mainProducto = get_term_by('name', 'novedades', 'producto');


            ?>
            <div class='amp-products-group'>
                <?php

                ?>
                <amp-accordion>
                    <section>
                        <h2 class="amp-products-group-hero">
                            <?php
                                $product_image = $amp->get_image_with_alt('imagen_mobile_producto', 'producto_' . $mainProducto->term_id,"banner-category-mobile");
                                if ($product_image->src){
                            ?>
                            <amp-img class="amp-products-group-image" width="375" height="305" layout="responsive" alt="<?= $product_image->alt ?>"
                                     src="<?= $product_image->src ?>">
                            </amp-img>
                                    <?php } ?>
                            <div class="amp-products-group-count">
                                <p><?= sizeof($nuevos) ?> productos</p>
                                <a href="" class='amp-products-group-expand'>
                                    <p class='fa fa-chevron-down'></p>
                                </a>
                            </div>
                        </h2>

                        <amp-carousel width="400"
                                      height="300"
                                      layout="responsive"
                                      type="slides"
                                      class="amp-products-carousel">

                            <?php

                            foreach ($nuevos as $producto) {
                                $Nuevo_temp = get_bloginfo('template_directory') . "/images/icons/nuevo_prod.svg";
                            } ?>


                            <a href="<?= get_term_link($producto) ?> "
                               class="amp-products-carousel-item">
                                <?php
                                if ($Nuevo_temp != "") {
                                    ?>
                                    <amp-img  width="243" height="403" alt="nuevo producto"  layout="responsive" class="amp-products-carousel-item-new" src="<?= $Nuevo_temp ?>"  ></amp-img>
                                    <?php
                                }
                                $product_image1 = $amp->get_image_with_alt('imagen_producto', 'producto_' . $producto->term_id, "medium_large");
                                if ($product_image1->src){
                                ?>

                                <amp-img width="243" height="403" layout="responsive"
                                         src="<?= $product_image1->src ?>"  alt="<?= $product_image1->alt ?>"></amp-img>
                                <?php } ?>
                                <p><?= $producto->name ?></p>
                            </a>
                        </amp-carousel>
                    </section>
                </amp-accordion>
            </div>
        </div>
    </div>



<?php include 'amp/amp-footer.php' ?>