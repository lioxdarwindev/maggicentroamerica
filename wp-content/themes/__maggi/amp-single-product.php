<?php include 'amp/amp-header.php' ?>
<?php
global $post, $wp_query;
$amp = new AmpUtils();
$slug = $wp_query->query_vars["producto"];
$term = get_term_by( 'slug', $slug, "producto");
$parent_term = get_term( $term->parent, 'producto');
$amp = new AmpUtils();


?>
    <div class="amp-container">
    <div class="amp-single-product">
        <div class="amp-single-product-header">
            <?php $imagen_producto = $amp->get_image_with_alt('imagen_producto', 'producto_' . $term->term_id, 'imagen-producto') ?>
            <amp-img src="<?= $imagen_producto->src ?>" alt="<?= $imagen_producto->alt?>"
                     width="243" height="404"></amp-img>
        </div>
        <div class="amp-single-product-thumb">
            <?php
            for ($i = 2; $i <= 5; $i++) {
                $imgP = get_field('image_presentacion_' . $i, 'producto_' . $term->term_id);
                $image = $amp->get_image_with_alt('image_presentacion_' . $i, 'producto_' . $term->term_id);
                if ($imgP) {
                    ?>
                    <div class="amp-single-product-thumb-item">
                        <amp-img width="62" height="110" src="<?= $image->src ?>" alt="<?= $image->alt ?>"></amp-img>
                    </div>
                    <?php
                }
            }
            ?>

        </div>

        <div class="amp-single-product-info">
            <h1 class="amp-single-product-info-title">
                <?php echo($term->name);?>
            </h1>

            <h2 class="amp-single-product-info-subtitle">
                <?php echo($parent_term->name);?>
            </h2>
            <div class="amp-single-product-info-description">
                <?php echo(get_field( 'descripcion','producto_'.$term->term_id));?>
            </div>
            <div class="amp-single-product-info-presentation">
                <div class="amp-single-product-info-presentation-row">
                    <div class="amp-single-product-info-presentation-col"><h5>Presentación:</h5></div>
                    <div class="amp-single-product-info-presentation-col"><?php echo(get_field( 'presentacion','producto_'.$term->term_id));?></div>
                </div>
                <div class="amp-single-product-info-presentation-row">
                    <div class="amp-single-product-info-presentation-col"><h5><?php echo(get_field( 'titulo_medida','producto_'.$term->term_id));?></h5></div>
                    <div class="amp-single-product-info-presentation-col"><?php echo(get_field( 'medida','producto_'.$term->term_id));?></div>
                </div>
                <div class="amp-single-product-info-presentation-row">
                    <div class="amp-single-product-info-presentation-col"><h5>Porciones:</h5></div>
                    <div class="amp-single-product-info-presentation-col"><?php echo(get_field( 'porciones','producto_'.$term->term_id));?></div>
                </div>
            </div>

            <div class="amp-single-product-info-nutritional">
                <h5>Información Nutricional
<!--                    <span class="arrow right"></span>-->
                </h5>
                <div class="amp-single-product-info-nutritional-table">
                    <?php
                    $nombres = ["","","",""];
                    $cont = 0;
                    $table = get_field( 'informacion_nutricional','producto_'.$term->term_id);

                    if ( $table ) {

                        echo '<table border="0">';



                        echo '<thead>';

                        echo '<tr>';

                        foreach ( $nombres as $th ) {

                            echo '<th>';
                            echo $th;
                            echo '</th>';
                        }

                        echo '</tr>';

                        echo '</thead>';


                        echo '<tbody>';

                        foreach ( $table['body'] as $tr ) {

                            echo '<tr>';

                            foreach ( $tr as $td ) {
                                if($td){
                                    echo '<td>';
                                    echo $td['c'];
                                    echo '</td>';
                                }
                            }

                            echo '</tr>';
                        }

                        echo '</tbody>';

                        echo '</table>';
                    }

                    ?>
                </div>

                <p class="amp-single-product-info-nutritional-disclaimer">(*) Los porcentajes de valores diarios están basados en una dieta recomendada de 2.000 kcal (FDA).</p>
            </div>

        </div>


        <div class="amp-single-product-recipes">
            <h3 class="amp-section-title">
                RECETAS SUGERIDAS
            </h3>

            <div class="amp-single-product-recipes-list">
                <?php
                $my_query = null;
                $args = array(
                    'DisableFilterPais' => true,
                    'post_type' => 'recipe',
                    'post_status' => 'publish',
                    'posts_per_page' => 2,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'producto',
                            'field' => 'slug',
                            'terms' => $slug
                        ),
                        array(
                            'taxonomy' => 'pais',
                            'field' => 'slug',
                            'terms' => $_SESSION['pais']
                        )
                    )
                );

                $my_query = new WP_Query($args);
                if ($my_query->have_posts()) {

                    while ($my_query->have_posts()) :
                        $my_query->the_post();
                        $homePost = $post;
                        $recipe = new WPURP_Recipe($post->ID);
                        $categoria = wp_get_post_terms($post->ID, array("category"));
                        $favorited = get_user_meta($recipe->ID(), 'wpurp_favorited', true);
                        $favoriteCount = ($favorited[0] > "") ? count($favorited) : 0;
                        $counter++;
                        $amp->print_recipe($counter, $recipe, $post, $categoria, $favoriteCount);
                    endwhile;
                }
                wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
            </div>


        </div>


    </div>


<?php include 'amp/amp-footer.php' ?>