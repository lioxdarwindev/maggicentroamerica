 <?php
        /* Template Name: Custom Search */        
        get_header(); 
    $tipo != "blog"
        ?>       
<div id="Container" class="content-area">
    <div id="main">

 <div class="recipe">
        <div class="banner pure-g">
          
        </div>

        <div class="wpurp-container">
            
            <div class='pure-g'>
                <ul id="blog-archive" class='pure-u-1-1 pure-u-md-2-3'>
                     <h1 class='resTit'>Resultados <?php echo "$s"; ?>: </h1>       
                     <?php if ( have_posts() ) : while ( have_posts() ) : the_post();   
                $fav = new WPURP_Favorite_Recipes();
                    
                ?>
                    <li>
                        <a class="blog-a" href="<?php the_permalink(); ?>">
                        <div>
                            <div class="blog-post-th"><img src="<?php echo(get_field('thumbnail',$post->ID)["sizes"]["blog-thumb"]);?>"/></div>
                            <div class="views_info"><span class="views"><?php echo(the_views(false)); ?></span><span class="fabs"><?php echo($fav->cantidad_favorites($post->ID)); ?></span>
                    </div>
                            
                        </div>  
                            <div class=blog_info>
                                <h2><?php the_title(); ?></h2>
                                <div class="resumen"><?php the_excerpt(); ?></div>
                                <div class="info"><img src="<?php echo(get_field('thumbnail_saboreador',get_user_by('id', $post->post_author))["sizes"]["thumb-producto"]);?>"/><div class="Aut_info"><p class="author_name">'<?php echo(get_author_name());?></p><p class="fecha"><?php echo(get_the_date());?></p></div>
                            </div>
                            </div>

                        </a>
                    </li>
               
        <?php endwhile; ?>
    <?php endif; ?>
         </ul>
         <div id="menuLateral" class='pure-u-1-1 pure-u-md-1-3 menuBlog'>
                <h3>Entradas por mes:</h3>
                <ul class='entradasPorMes'>
                    <?php 
                    wp_get_archives(array(
                        'limit'=>20, 
                        'post_type'=>'blog', 
                    
                    )); 

                ?>
                </ul>


            
                 </div>  
                 </div>  
            </div> <!-- Fin menuLateral -->



           </div>   
        </div><!-- contentarea --> 

        </div>  
        <?php get_footer(); ?>