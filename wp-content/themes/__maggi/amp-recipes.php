<?php include 'amp/amp-header.php' ?>


    <div class="amp-container">
        <div class="amp-recipes-banner">
                <?php
                wp_reset_query();

                $args = array(
                    'post_type' => 'recipe',
                    'orderby' => 'menu_order',
                    'sort_id' => '214',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-categoria',
                            'field' => 'name',
                            'terms' => 'Slider Categoria'

                        ),
                        array(
                            'taxonomy' => 'pais',
                            'field' => 'slug',
                            'terms' => $_SESSION['pais'],
                        )
                    )


                );

                $my_query = new WP_Query($args);
                if($my_query->have_posts()){


                ?>
                    <amp-carousel width="400"
                                  height="300"
                                  layout="responsive"
                                  type="slides"
                                  class="amp-products-carousel">

            <?php
                while ($my_query->have_posts()) {

                    $my_query->the_post();
                    $categoria = wp_get_post_terms($post->ID, array("category"));
                    $recipe = new WPURP_Recipe($post->ID);

                    ?>

                    <a class="amp-single-recipe-slide" href="<?= $recipe->link() ?>">
                        <div class="amp-single-recipe-header recipes">

                            <span class="overlay"></span>
                            <?php
                                $banner_mobile_image = $amp->get_image_with_alt('banner_mobile', $recipe_post->ID, "banner-mobile");
                                if ($banner_mobile_image->src){
                                ?>
                                <amp-img layout="responsive" width="640" height="480"
                                         src="<?= $banner_mobile_image->src ?>" alt="<?=$banner_mobile_image->alt ?>"></amp-img>
                            <?php } ?>
                            <amp-img width="155" height="44" class="ornament " alt="recetas"

                                     src="<?= get_bloginfo('template_directory') . '/images/ornamento.png' ?>"></amp-img>

                            <div class="amp-single-recipe-headline">
                                <h2><?= $categoria[0]->name; ?></h2>
                                <h1><?= $recipe->title(); ?></h1>
                                <div class="amp-single-recipe-servings">
                                    <amp-img
                                            src="<?= get_bloginfo('template_directory') . '/images/icons/porciones.svg?>' ?>"
                                            alt="porciones"
                                            width="29" height="29"></amp-img>
                                    <p><span class="highlight"><?= $recipe->servings_normalized() ?></span> porciones
                                    </p>
                                </div>


                            </div>

                        </div>
                        <div class="amp-single-recipe-details">
                            <div class="recipe-difficulty">
                                <div class="recipe-difficulty-level">
                                    <div class="recipe-difficulty-level-indicator <?php echo(get_the_terms($recipe->ID(), 'course')[0]->slug); ?>">
                                        <span><span></span></span>
                                        <span><span></span></span>
                                        <span><span></span></span>
                                    </div>
                                    <div class="recipe-difficulty-level-label">
                                        <p><?php echo(get_the_terms($recipe->ID(), 'course')[0]->name); ?></p>
                                    </div>
                                </div>


                            </div>
                            <div class="recipe-duration">
                                <amp-img alt="duracion" src="<?= get_bloginfo('template_directory') ?>/images/icons/clock.svg"
                                         width="30"
                                         height="27"></amp-img>
                                <span class="recipe-duration-time"><?= $recipe->cook_time() ?> </span>
                                <span class="recipe-duration-time-units">min</span>

                            </div>
                        </div>
                    </a>
                    <?php
                }

                ?>
            </amp-carousel>
                    <?php } ?>
        </div>
        <div class="amp-category">
            <?php
            $cat = $_GET['cat'];
            $mainCategories = get_terms('category', array('parent' => 0, 'hide_empty' => false, 'exclude' => '1'));
            foreach ($mainCategories as $category) {
                $term_link = get_term_link($category);
                $activo = "";
                if ($cat == $category->slug) {
                    $activo = "activo";
                }

                ?>
                <div class="amp-category-item">
                    <a id="'<?= $category->slug ?>" class="amp-category-item-link <?= $activo ?>"
                       href="<?= $term_link ?>">
                        <amp-img width="40" height="56" class="amp-category-image" alt="<?=  $category->slug ?>"
                                 src="<?= get_bloginfo('template_directory') . "/images/" . $category->slug . ".svg" ?>"></amp-img>
                        <p><?= $category->name ?> </p>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>

        <?php
        $my_query = null;
        $my_query = new WP_Query(array('post_type' => 'recipe', 'posts_per_page' => '5',));
        $amp = new AmpUtils();

        if ($my_query->have_posts()) {
            while ($my_query->have_posts()) :
                $my_query->the_post();
                $homePost = $post;
                $recipe = new WPURP_Recipe($post->ID);
                $categoria = wp_get_post_terms($post->ID, array("category"));
                $favorited = get_user_meta($recipe->ID(), 'wpurp_favorited', true);
                $favoriteCount = ($favorited[0] > "") ? count($favorited) : 0;
                $counter++;
                $amp->print_recipe($counter, $recipe, $post, $categoria, $favoriteCount);
            endwhile;
        }
        wp_reset_query();  // Restore global post data stomped by the_post().
        ?>

    </div>


<?php include 'amp/amp-footer.php' ?>