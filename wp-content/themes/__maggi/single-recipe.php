<?php get_header(); 
    
?>
<div id="Container" class="content-area">
    <div id="main">
        <?php 
        while ( have_posts() ) : the_post();
        $recipe = new WPURP_Recipe($post->ID); 
        $categoria = wp_get_post_terms($post->ID,array("category"));
        $preguntas = do_shortcode('[pregunta]');

        endwhile;


       

        ?>
             <div class="recipe">
        <div class="banner pure-g">
        <span class="pure-u-1 hid">
        <span class="overl"></span>
            <?php 

            echo('<img class="desktop" src="'.$recipe->featured_image_url("banner").'"/><img class="mobile" src="'.get_field('banner_mobile',$post->ID)["sizes"]["banner-mobile"].'"/><span><img class="ornament "src="'.get_bloginfo('template_directory').'/images/ornamento.png"></span>
                    <h2 class="termName">'.$categoria[0]->name.'</h2><h1 class="tituloRes">'.$recipe->title().'</h1><span class="porciones"><img class="" src="'.get_bloginfo('template_directory').'/images/icons/porciones.svg"><p class="yell">'.$recipe->servings_normalized().'</p><p>porciones</p></span>');?>
        </span>
            <div class="breadcrumbs pure-u-md-16-24 desktop" typeof="BreadcrumbList" vocab="http://schema.org/">
                <?php if(function_exists('bcn_display'))
                {
                    bcn_display();
                }?>
            </div>

            <div class="dificultad pure-u-14-24 pure-u-md-4-24" >
            
                
            <div class="recDif <?php echo(get_the_terms( $recipe->ID,'course')[0]->slug);?>">
                <span><span></span></span>
                <span><span></span></span>
                <span><span></span></span>   
                <p><?php echo(get_the_terms( $recipe->ID,'course')[0]->name);?></p>
            </div>
           
            </div>

            <div class="tiempo pure-u-10-24 pure-u-md-4-24">
            <img src="<?=  get_bloginfo('template_directory')?>/images/icons/clock.svg">    
            <p class='t'><?php echo($recipe->cook_time());?></p><p>min</p></div>

        </div>

        <div class="wpurp-container">
            <div class="social">

                <?php
                   $fav = new WPURP_Favorite_Recipes();
                   
                   echo("<div class='numFab'><span class='fa fa-heart'></span><span class='num'>".$fav->cantidad_favorites($recipe->ID())."</span></div>");

                    $Fav_button = new WPURP_Template_Recipe_Favorite();
                    echo $Fav_button->output( $recipe );

                    // $Face_button = new WPURP_Template_Facebook();
                    // echo $Face_button->output( $recipe );
                    
                    // $Twi_button = new WPURP_Template_Twitter();
                    // echo $Twi_button->output( $recipe );

                    // $Pin_button = new WPURP_Template_Pinterest();
                    // echo $Pin_button->output( $recipe );
                    ?>
                    <div class='sh'>
                      <p>Comparte en:</p>
                    <?php
                    echo(do_shortcode ('[feather_share skin="wheel"]'));
                    echo('<a class="whats" href="whatsapp://send?text='.get_permalink().'"><img src="'.get_bloginfo('template_directory').'/images/whatsapp.svg"></a>');
                    $print_button = new WPURP_Template_Recipe_Print_Button();
                    echo $print_button->output( $recipe );
                   

              $video = $recipe->custom_field('url_video_youtube');
                ?>
                    </div>
            </div>
            <div class='pure-g'>
                <div id="recetaContent" class='pure-u-1-1 pure-u-md-2-3'>
                <?php if($video){?>
                <div class="video-container"><iframe  src="//www.youtube.com/embed/<?php echo($video);?>?rel=0" 
        frameborder="0" allowfullscreen></iframe></div>
                <?php }?>
                
                <div class='pure-g'>
                    <div class='mini-info pure-u-1-2'>
                        <div><span>Preparación:</span><span><?php echo $recipe->prep_time()." ".$recipe->prep_time_text(); ?></span></div>
                        <div><span>Inactivo:</span><span><?php echo $recipe->passive_time()." ".$recipe->passive_time_text(); ?></span></div>
                        <div><span>Cocinando:</span><span><?php echo $recipe->cook_time()." ".$recipe->cook_time_text(); ?></span></div>
                    </div>
                    <div class="notas pure-u-1-2">
                   <?php echo($recipe->notes());?>
                        
                    </div>    
                </div>        
                    
                    <div  class="pure-g intruccionesQCH">
                        <div class="pure-u-10-24"></div>
                        <div class="pure-u-2-24 icon"><img src="http://tellinginteractive.com/demos/maggi/wp-content/themes/maggi/images/icons/home-icon-qch.svg"></div>
                        <div class="pure-u-10-24"></div>
                        <div class="pure-u-1-1">
                            <h2 class="recTit">INGREDIENTES</h2>
                        </div>

                    </div>

                    <div class="ingredientes">        
                       
                        <?php
                            $ingredient_list = new WPURP_Template_Recipe_Ingredients();

                            echo preg_replace('/(<[^>]+) style=".*?"/i', '$1',$ingredient_list->output( $recipe ));
                        ?>
                    </div>

                    <div  class="pure-g intruccionesQCH">
                        <div class="pure-u-10-24"></div>
                        <div class="pure-u-2-24 icon"><img src="http://tellinginteractive.com/demos/maggi/wp-content/themes/maggi/images/icons/preparacion.svg"></div>
                        <div class="pure-u-10-24"></div>
                        <div class="pure-u-1-1">
                            <h2 class="recTit">PREPARACIÓN</h2>
                        </div>

                    </div>

                     <div class="preparacion">        
                      
                        <?php
                            $preparation_list = new WPURP_Template_Recipe_Instructions();
                            echo $preparation_list->output( $recipe );
                        ?>
                    </div>
                   
                    <?php $tip = get_field('tip_culinario',$post->ID);
                        if($tip){
                            echo(" <div class='tip'><h2 class='recTit'>TIP CULINARIO</h2><p>".$tip."</p></div>");
                        }

                        $infoNut = get_field('informacion_nutricional',$post->ID);
                        if($infoNut){
                            echo(" <div class='infoNut'>".$infoNut."</div>");
                        }

                    ?>
   
            

                

                    <div class="social">

                <?php
                   $fav = new WPURP_Favorite_Recipes();
                   
                   echo("<div class='numFab'><span class='fa fa-heart'></span><span class='num'>".$fav->cantidad_favorites($recipe->ID())."</span></div>");

                    $Fav_button = new WPURP_Template_Recipe_Favorite();
                    echo $Fav_button->output( $recipe );

                    // $Face_button = new WPURP_Template_Facebook();
                    // echo $Face_button->output( $recipe );
                    
                    // $Twi_button = new WPURP_Template_Twitter();
                    // echo $Twi_button->output( $recipe );

                    // $Pin_button = new WPURP_Template_Pinterest();
                    // echo $Pin_button->output( $recipe );
                    ?>
                    <div class='sh'>
                      <p>Comparte en:</p>
                    <?php
                    echo(do_shortcode ('[feather_share skin="wheel"]'));
                    echo('<a class="whats" href="whatsapp://send?text='.get_permalink().'"><img src="'.get_bloginfo('template_directory').'/images/whatsapp.svg"></a>');
                    $print_button = new WPURP_Template_Recipe_Print_Button();
                    echo $print_button->output( $recipe );
                   

              $video = $recipe->custom_field('url_video_youtube');
                ?>
                    </div>
            </div>


            <div class="ams-comentarios-preguntas">
                <a class="comentariosLabel" href="#">Comentarios<p class="arrow right"></p></a>
                <div id="comentarios">
                    <?php                       
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) {
                            comments_template();
                        }
                        //echo do_shortcode ('[askme] ');
                    ?>
                </div> 
            </div>
            <div class="ams-comentarios-preguntas">
                <a class="preguntasLabel" href="#">Preguntas<p class="arrow right"></p></a>
                <div id="preguntas"> 
                    <?php echo($preguntas);?>
                </div>
            </div>




        </div>
        <div id="menuLateral" class='pure-u-1-3 desktop'>
         <?php 
            include  'filtrosRecetas.php';
            ?>
                    
    </div> <!-- Fin menuLateral -->
            </div>
    
            
        </div>
    </div>
    
    </div>
</div>
<?php get_footer(); ?>      