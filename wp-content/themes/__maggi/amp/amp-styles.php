<style amp-custom>

    @font-face {
        font-family: 'roboto';
        src: url('<?php echo get_template_directory_uri()?>/amp/fonts/roboto-bold-webfont.ttf') format('truetype');
        font-weight: 900;
        font-style: normal;
    }

    @font-face {
        font-family: 'roboto';
        src: url('<?php echo get_template_directory_uri()?>/amp/fonts/roboto-medium-webfont.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: 'FontAwesome';
        src: url('<?php echo get_template_directory_uri()?>/amp/fonts/fontawesome-webfont.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    body {
        font-family: 'roboto';
    }

    body * {
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .amp-container {
        /*padding-bottom: 100px;*/
    }

    .amp-close-image:after{
        content: "\f00d";
    }
    .amp-close-image{
        font-family:FontAwesome;
        font-size:20px;
        color: #FF0000;
        position: absolute;
        right:10px;
        top:10px;
    }
    .menu-principal-container{
        padding-top: 25px;
    }
    .hm-menu-icon:before {
        content: "\f0c9";
    }
    .hm-menu-icon{
        font-family:FontAwesome;
        font-size:28px;
        color: #000;
        position: relative;

    }

    /*top bar*/

    .amp-top-bar {
        height: 64px;
        display: flex;
        width: 100%;
        position: relative;
    }

    .amp-top-bar-item.hm-menu {
        flex: 1;
        background: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .amp-top-bar-search-form form{
        width:100%;
    }
    .amp-top-bar-search-form input[type="submit"]{
        flex: 1;
        border: none;
        color: white;
        width: 70px;
        background-color: #ff3333;
        text-align: center;
        padding: 15px 0;
        font-size: 16px;
        -webkit-appearance: button;
        cursor: pointer;
    }
    .amp-top-bar-search-form input[type="text"]{
        flex: 3;
        width: calc(100% - 90px);
        height: 52px;
        background: #f4f4f4;
        border: none;
        padding-left: 20px;
    }
    .amp-top-bar-search-form form {
        display: flex;
    }
    .amp-top-bar-search-form{
        position: absolute;
        bottom:-73px;
        z-index: 10;
        right:0;
        background: #fff;
        width:95%;
        padding:10px;
        display: none;
    }

    .amp-top-bar-item-search-btn{
        text-decoration: none;
    }
    .amp-top-bar-item-search-btn:before{
        content: "\f002";
        color: #000;

    }


    .amp-top-bar div:target{
        display: block;
    }
    .amp-top-bar-item.search{
        flex: 1;
        color: #333333;
        font-family: FontAwesome;
        font-size: 28px;
        position: relative;
        background: #ffffff;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .amp-top-bar-item.logo {
        flex: 4;
        display: flex;
        align-items: center;
        justify-content: center;
        padding-top:30px;
        z-index: 1;
    }

    .amp-top-bar-item {
    }

    amp-sidebar {
        background: #fff;
        width: 90vw;
    }

    amp-sidebar ul {
        list-style: none;
        padding-left: 10%;
    }

    amp-sidebar ul li {
        padding: 20px 0;
        width: 90%;
    }

    amp-sidebar ul li a {
        color: #666666;
        text-decoration: none;
        position: relative;
        font-size: 17px;
    }

    amp-sidebar ul li a:before {
        content: " ";
        background-repeat: no-repeat;
        margin: 0 13px;
        background-position: center;
        width: 18px;
        height: 25px;
        position: relative;
        display: inline-block;
        vertical-align: top;
        top: 0;
    }

    amp-sidebar ul li.login a {
        color: #fff;
    }

    amp-sidebar ul li.login {
        background: #ff3333;
    }

    amp-sidebar ul li.menu_los_saboreadores a:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/menu-sabo-ccc.svg");
    }

    amp-sidebar ul li.menu_recetas a:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/menu-recetas-ccc.svg");
    }

    amp-sidebar ul li.menu-item.menu_promociones a:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/menu-promos-ccc.svg");
    }

    amp-sidebar ul li.menu-item.menu_productos a:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/menu_productos-ccc.svg");
    }

    amp-sidebar ul li.menu-item.menu_blog a:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/menu-blog-ccc.svg");
    }

    amp-sidebar ul li.menu-item.hablanos a:before {
        background-image: url(<?php echo get_template_directory_uri()?>/images/icons/hablenos.svg);
        width: 18px;
    }

    .amp-header-image {
        position: relative;
    }

    .amp-options {
        osition: absolute;
        bottom: 64px;
        height: 64px;
        width: 100%;
    }

    .amp-options-container {
        display: flex;
        position: relative;
        top: -84px;
    }

    .amp-options a img {
        max-width: 20%;
    }

    .amp-options a {
        flex: 1;
        text-align: center;
        height: 100%;
    }

    .separator {
        border-bottom: 1px solid #ccc;
        position: relative;
    }

    .separator amp-img {
        left: 0;
        right: 0;
        margin: 0 auto;
        position: absolute;
        bottom: -22px;
        background: #fff;
        padding: 0 20px;
    }

    footer {
        background: #FFF200;
        position: relative;
        padding-top: 80px;
        margin-top: 80px;
    }

    footer .logo-footer {
        position: absolute;
        top: -36px;
        left: 0;
        right: 0;
        margin: 0 auto;
    }

    .footer-container-links {
        display: flex;
        align-items: center;
        justify-content: space-around;

    }

    .footer-container-links a {
        text-decoration: none;
        font-size: 0.8rem;
        color: #000;
        text-align: center;
    }

    .footer-container-social {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 60%;
        margin: 50px auto;

    }

    .footer-container-social .st0 {
        fill: #ff3333 ;
    }

    .footer-container-social a {
        flex: 1;
        display: block;
        text-align: center;
    }

    .footer-container-disclaimer {

    }

    .footer-container-disclaimer p {
        text-decoration: none;
        font-size: 0.75rem;
        color: #000;
        text-align: center;
        width: 85%;
        margin: 0 auto;
        padding: 20px 0;
    }

    .footer-container-brand {
        background: #fff;
        padding: 20px;
        display: flex;
        text-align: center;
        justify-content: center;
    }

    /*headline*/
    .amp-headline {

    }

    .amp-headline h2 {

        height: 32px;
        text-align: center;
        font: normal normal bold normal 24px / normal roboto;
        margin: 0px;
        padding: 35px;

    }

    .amp-headline p {
        height: 22px;
        text-align: center;
        text-size-adjust: 100%;

        font: normal normal normal normal 16px / normal roboto;
        margin: 0px;
    }

    /*recipes grid*/

    .fabs:before {
        background-image: url("images/icons/fabs.svg");
        width: 18px;
    }

    .shares:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/shares.svg");
        width: 18px;
    }

    .views:before {
        background-image: url("<?php echo get_template_directory_uri()?>/images/icons/views.svg");
        width: 22px;
    }

    .fabs:before {
        background-image: url(<?php echo get_template_directory_uri()?>/images/icons/fabs.svg);
        width: 18px;
    }


    .recipe-container {
        position: relative;
        display: block;
        width: 94%;
        padding: 3%;
        margin: 0 auto;
    }
    .recipe-container-frame{
        overflow: hidden;
        position: relative;
        border-radius: 20px;
    }


    .recipe-container .recipe-container-info {
        position: absolute;
        bottom: 8%;
        left: 5%;
        z-index: 1;
    }

    .recipe-title {
        color: #fff ;
    }

    .recipe-category, .recipe-title {
        font-size: 14px;
        font-weight: 100;
        color: #fff200;
        font-family: 'roboto';
        margin: 0;
        padding: 0;
        line-height: 20px;
    }
    .single-recipe{

    }
    .recipe-container-activity {
        padding-top: 26px;
        display: flex;
    }

    .recipe-container-activity span {
        padding-right: 30px;
        color: #fff;
    }

    .recipe-container-activity span:before {
        content: " ";
        background-repeat: no-repeat;
        margin: 0 13px 0 0;
        background-position: center;
        height: 25px;
        position: relative;
        display: inline-block;
        vertical-align: top;

    }

    .recipe-container-activity .views:before {
        background-image: url(<?php echo get_template_directory_uri()?>/images/icons/views.svg);
        width: 22px;
    }

    .recipe-container-author {
        display: flex;
        padding: 3%;

    }

    .recipe-duration-time-units {
        font-size: 15px;
    }

    .recipe-duration-time {
        font-size: 21px;
    }

    .recipe-duration {
        /*flex: 2;*/
        background: #ff3333;
        color: #fff;
        display: flex;
        text-align: center;
        padding: 13px 13px ;
        box-sizing: border-box;
        width: 120px;
        justify-content: space-around;

    }

    .recipe-author-info-date {
        font-size: 12px;
        color: #909090;
        padding: 0;
        margin: 0;
    }

    .recipe-author-info-name {
        color: #ff3333;
        font-size: 12px;
        padding: 0;
        margin: 0;
    }

    .recipe-author-info {
        padding-left: 10px;
    }

    .recipe-author {
        display: flex;
        align-items: flex-end;
        flex: 3;
    }

    .recipe-container a {
        display: block;
    }

    .recipe-container {
    }

    .single-recipe.list {
        display: flex;

    }

    .single-recipe.list .recipe-container {
        flex: 1;
    }

    .single-recipe.list .recipe-container-author {
        flex: 1;
        align-items: flex-start;
        margin-top: 75px;
        padding: 0;

    }

    .single-recipe.list .recipe-duration {
        display: none;
    }

    .single-recipe.list .recipe-container-info {
        color: #000;
        top: 10%;
        left: 100%;
        width: 90%;

    }

    .single-recipe.list .recipe-container-info .recipe-category {
        display: none;
    }
    .amp-single-recipe-slide{
        text-decoration: none;
    }
    .single-recipe.list .recipe-container-info .recipe-title {
        color: #000 ;
        padding-right:10px;
    }

    .single-recipe.list .recipe-container-info .recipe-container-activity {
        display: none;
    }

    /*recipe page*/

    .amp-category {
        display: flex;
        flex-flow: row wrap;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .amp-category-item p {
        margin: 0;
        /*padding: 23px 11px;*/
    }

    .amp-category-item-link {
        border: 1px solid #ccc;
        text-align: center;
        font-size: 13px;
        font-weight: bold;
        color: #333333;
        text-decoration: none;
        display: block;
        padding: 23px 11px;
    }

    .amp-category-item:nth-child(2n) {
        margin-right: 0;
    }

    .amp-category-item {
        /*width: 50%;*/
        margin-right: 5px;
        flex: 1;

        flex-basis: 48%;
        text-align: center;
        margin-bottom: 10px;

    }

    .amp-single-recipe-header .ornament {
        position: absolute;
        top: 15px;
        left: 25px;

    }

    .amp-single-recipe-headline h1 {
        font-size: 17px;
        font-weight: normal;
        color: #fff;
    }

    .amp-single-recipe-headline p {
        color: #fff;
    }

    .amp-single-recipe-headline .highlight {
        color: #fff200;
    }

    .amp-single-recipe-servings p {
        display: inline-block;
        vertical-align: middle;
        margin: 0;
        /*align-self: flex-start;*/
    }

    .amp-single-recipe-servings amp-img {
        display: inline-block;
        vertical-align: top;
    }

    .amp-single-recipe-servings {

        width: 100%;
        /*align-items:center;*/
    }

    .amp-single-recipe-headline h2 {
        color: #fff200;
        font-weight: normal;
        font-size: 20px;
    }

    .amp-single-recipe-headline {
        position: absolute;
        bottom: 30px;
        left: 45px;
        z-index: 2;
    }

    .amp-single-recipe-header.recipes {
        margin-top:100px;

    }
    .amp-single-recipe-header {
        position: relative;
    }

    .overlay {
        left: 0;
        width: 100%;
        position: absolute ;
        height: 100%;
        display: block;
        z-index: 1;
        border-right:20px;
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 40%, rgba(0, 0, 0, 0.62) 100%);
    }

    .amp-single-recipe-details {
        display: flex;
    }

    .recipe-difficulty-level-indicator {
        flex: 4;
    }
    .recipe-difficulty-level{
        display: flex;
        align-items:center;
        width: 100%;
    }
    .recipe-difficulty-level-label p{
        color: #ff3333;
        padding: 3px 5px;
    }
    .recipe-difficulty-level-label {
        flex: 1;

    }

    .recipe-difficulty {
        flex: 3;
        display: flex;
        padding-left: 20px;
    }

    .recipe-difficulty-level-indicator > span > span {
        width: 6px;
        height: 6px;
        background: #ccc;
        margin: 4px;
        color: #ff3333;
    }

    .recipe-difficulty-level-indicator span {
        display: inline-block;
        float: left;
        margin: 2px;
    }

    .recipe-difficulty-level-indicator > span {
        width: 15px;
        height: 15px;
        border: 2px solid #ccc;

    }

    .recipe-difficulty-level-indicator.facil > span:first-child {
        border-color: #ff3333;
    }

    .recipe-difficulty-level-indicator.facil > span:first-child > span {
        background-color: #ff3333;
    }

    .recipe-difficulty-level-indicator.medio > span:first-child, .recipe-difficulty-level-indicator.medio > span:first-child + span {
        border-color: #ff3333;
    }

    .recipe-difficulty-level-indicator.medio > span:first-child > span, .recipe-difficulty-level-indicator.medio > span:first-child + span > span {
        background-color: #ff3333;
    }

    .recipe-difficulty-level-indicator.dificil > span {
        border-color: #ff3333;
    }

    .recipe-difficulty-level-indicator.dificil > span > span {
        background-color: #ff3333;
    }

    .amp-single-recipe- {

    }

    .amp-single-recipe-container {

    }

    .single-recipe.list {
    }

    .single-recipe.list {
    }

    .amp-single-recipe-social-favs .fa-heart{
        color: #ff3333;
        padding: 0px 30px 0px 8px;
    }
    .amp-single-recipe-social-favs{
        border-radius: 10px;
        width: 87px;
        height: 19px;
        border: 1px solid #ccc;
        padding: 18px;
        margin: 30px auto;
    }

    .fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .fa-heart:before {
        content: "\f004";
    }
    .amp-single-recipe-prep{
        margin-bottom: 40px;
    }
    .amp-single-recipe-prep-table tr td{
        padding:5px;
    }
    .amp-single-recipe-prep-table tr td:first-child{
        color: #ff3333;
    }
    .amp-single-recipe-prep-table{
        background: #f4f4f4;
    }
    .amp-single-recipe-ingredients{
        margin-top:40px;
    }

    .amp-section-title:after{
        content: "";
        background: #fff200;
        position: relative;
        bottom: -10px;
        margin: 10px auto 20px auto;
        display: block;
        height: 5px;
        width: 120px;
    }
    .amp-section-title{
        padding: 6px;
        margin: 0;
        font-size: 19px;
        text-align: center;
    }

    .amp-single-recipe-ingredients-list ul  .wpurp-recipe-ingredient:nth-child(2n+1) {
        background: #f4f4f4;
    }

    .amp-single-recipe-ingredients-list ul .wpurp-recipe-ingredient:before {
        background: #ff0000;
    }

    .amp-single-recipe-ingredients-list ul.wpurp-recipe-ingredients {
        margin-left: 24px;
    }

    .amp-single-recipe-ingredients-list ul .wpurp-recipe-ingredient {
        position: relative;
        display: block;
        padding: 10px;
        text-decoration: none;
        transition: all .3s ease-out;
    }
    .amp-single-recipe-ingredients-list ul .wpurp-recipe-ingredient:nth-child(2n+1):before {
        background: #fff200;
    }
    .amp-single-recipe-ingredients-list ul.wpurp-recipe-ingredients {
        padding-left: 1.6em;
    }
    .amp-single-recipe-ingredients-list ul .wpurp-recipe-ingredient:before {
        color: white;
        content: "\f058";
        counter-increment: li;
        position: absolute;
        left: -2.6em;
        margin-top: -0.5em;
        font-family: FontAwesome;
        height: 2.32em;
        width: 2.6em;
        line-height: 2.6em;
        text-align: center;
        /* font-weight: bold; */
        font-size: 1.2em;
    }
    .amp-single-recipe-ingredients-list ul.wpurp-recipe-ingredients a {
        color: #ff3333;
    }
    .amp-single-recipe-preparation{
        margin-top:40px;
    }
    .amp-single-recipe-preparation-list .wpurp-recipe-instruction {
        border-bottom: none;
        color: #666666;
    }
    .amp-single-recipe-preparation-list li.wpurp-recipe-instructions {
        margin: 0;
    }
    .amp-single-recipe-preparation-list li.wpurp-recipe-instruction span {
        width: calc(85% - 1.2em);
    }


    .amp-single-recipe-preparation-list ol{
        padding: 0;
    }
    .amp-single-recipe-preparation-list .wpurp-recipe-instructions li{
        overflow: hidden;
        width: 100%;
        list-style: none;
        counter-increment: item;
        padding-top: 5px;
        padding-bottom: 15px;
        margin-bottom: 10px;
    }
    .amp-single-recipe-preparation-list .wpurp-recipe-instructions li:before {
        margin-right: 10px;
        content: counter(item);
        background: #cccccc;
        color: white;
        padding: 10px;
        font-size: 22px;
        width: 4%;
        text-align: center;
        display: inline-block;
        float: left;
    }
    .amp-single-recipe-nutrition-table{
        border-top: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
        background: #f4f4f4;
        display: inline-block;
        width: 90%;
        padding: 5%;
    }
    .amp-single-recipe-nutrition-table table td{
        width: 48%;
    }
    .amp-single-recipe-nutrition-table table td:first-child {
        float: left;
        color: #ff3333;
    }
    .amp-single-recipe-nutrition-table table td:first-child+ td {
        float: right;
    }
    .amp-single-recipe-nutrition-table table tr{
        padding: 10px;
    }
    .amp-single-recipe-nutrition-table table{
        width: 100%;
    }

    /*products*/

    .amp-products-group{}
    .amp-products-group-hero{
        position: relative;
        padding: 0;
    }

    .amp-products-group-expand:before{
        content: "\f078";
    }
    .amp-products-group-expand{
        height: 60px;
        width: 60px;
        max-width: 60px;
        background: #FF3333;
        font-family:FontAwesome;
        color: #fff;
        display: block;
        text-decoration: none;
        text-align: center;
        font-size:24px;
        line-height: 60px;
        flex:1;


    }
    section[expanded] .show-more {
        display: none;
    }
    section:not([expanded]) .show-less {
        display: none;
    }


    .amp-products-group-count{
        position: absolute;
        bottom: 20px;
        left: 10%;
        display: flex;
        width: 90%;
        align-items: flex-end;
    }
    .amp-products-group-count p{
        color: #fff;
        font-size: 16px;
        flex:4;
        padding: 0;
        margin: 0;

    }
    .amp-products-group-count{}
    .amp-products-group-count{}
    .amp-products-group-count{}

    .amp-products-carousel{}
    .amp-carousel-button-prev:before{
        content: "\f053";
    }
    .amp-carousel-button-next:before{
        content: "\f054";
    }

    .amp-carousel-button-next, .amp-carousel-button-prev{
        background: none;
        font-family:FontAwesome;
        font-size: 20px;
        opacity: 1;
        display: block;
        visibility: visible;
    }
    .amp-products-carousel-item amp-img > img { object-fit: contain; }
    .amp-products-carousel-item{
        text-decoration: none;
    }
    .amp-products-carousel-item p{
        margin:0;
        color: #333333;
        font-size: 13px;
        text-align: center;

    }
    .amp-products-carousel-item{
        margin: 20px 0;
        display: flex;
        justify-content:center;
        align-items: center;
        flex-direction:column;
        padding: 20px 0;

    }
    .amp-products-carousel-item-new{
        width: 80px;
        height: 16px;
        position: absolute;
        top: 0;
        left: 0;
    }
    amp-img > img { object-fit: contain; }
    .amp-products-group .amp-carousel-slide {
        width: 60%;
        border: 1px solid #ccc;
        max-height: 80%;
        position: relative;
    }
    .amp-recipes-banner .amp-carousel-button-next, .amp-recipes-banner .amp-carousel-button-prev{
        color: #fff;
    }
    .amp-recipes-banner .amp-single-recipe-header .ornament{
        top:25%;
    }
    .amp-recipes-banner .amp-carousel-slide {
        width: 100%;
        border: 1px solid #ccc;
        height: calc(100% + 300px);
        position: relative;

    }
    .amp-single-product{}
    .amp-single-product-header{
        background:#fff200;
        display: flex;
        align-items: center;
        justify-content: center;
        height:60vh;
    }
    .amp-single-product-header amp-img{  }
    .amp-single-product-thumb{
        display: flex;
        margin-top: -20px;

    }
    .amp-single-product-thumb-item{
        width: 120px;
        height: 120px;
        background: #fff;
        border: 1px solid #ccc;
        text-align: center;
        margin: 0 10px;
    }

    .amp-single-product-info-title{
        width: 100%;
        background: red;
        text-align: center;
        font-size: 20px;
        color: white;
        padding: 31px 0;
        margin-top: 20px;
    }
    .amp-single-product-info-description p{
        font-weight: lighter;
        color: #666;
        font-size: 16px;
        line-height: 21px;
    }
    .amp-single-product-info-description{
        padding:20px;
    }
    .amp-single-product-info-subtitle{
        padding: 15px;
        margin: 0;
        font-size: 18px;
        text-align: center;
    }
    .amp-single-product-info-presentation{
        padding: 20px;
        background-color: #f4f4f4;
        border-top: 2px solid #ccc;
        border-bottom: 2px solid #ccc;

    }
    .amp-single-product-info-presentation-row{
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
        margin-bottom:10px;
    }
    .amp-single-product-info-presentation-col{
        flex:1;
        min-width:0;
        font-size:16px;
    }
    .amp-single-product-info-presentation-col:last-child{
        text-align: right;
    }
    .amp-single-product-info-presentation-col:first-child h5{
        color: #ff3333;
        font-size:16px;
        padding: 0;
        margin: 0;
    }
    .amp-single-product-info-nutritional h5{
        padding: 10px;
        height: 23px;
        font-size: 16px;
        color: #666666;
        margin: 5px 0;
    }
    .amp-single-product-info-nutritional .arrow.right{
        font-size:20px;
        display: inline-block;
        color: #000;
        margin-left: 30px;

    }
    .amp-single-product-info-nutritional .arrow.right:before{
        font-family:FontAwesome;
        content: "\f054";

    }
    .amp-single-product-info-nutritional{
        /*padding: 20px;*/
    }
    .amp-single-product-info-nutritional-table{}
    .amp-single-product-info-nutritional-table td, .amp-single-product-info-nutritional-table th {
        padding: 13px;
    }
    .amp-single-product-info-nutritional-table table{
        width: 100%;
        border:none;
        border-collapse: collapse;
    }
    .amp-single-product-info-nutritional-table thead{
        background: #333333;
        color: white;
    }

    .amp-single-product-info-nutritional-table td:first-child{
        width: 50%;
    }
    .amp-single-product-info-nutritional-disclaimer{
        font-size: 13px;
        padding: 25px 10px;
        text-align: center;
    }
    .amp-single-product-info-nutritional-table{}
    .btn{
        height: 58px;
        display: block;
        text-align: center;
        line-height:58px;
        color: #fff;
        position: relative;
        text-decoration: none;
    }
    .btn-primary{
        background: #ff3333;
    }
    .amp-subscribe-container{
        margin:20px 0 20px 0 ;
        display: flex;
        flex-direction:column;
    }
    .amp-subscribe-container .amp-subscribe-image{
        flex:4;
    }
    .amp-subscribe-container .btn{
        flex:1;
    }

    .amp-newsletter{
        margin:50px 0;
    }


</style>