<div class="amp-top-bar">
    <div class="amp-top-bar-item hm-menu">
        <span class="hm-menu-icon" tabindex="0" role="button" on='tap:sidebar.toggle'></span>
    </div>
    <div class="amp-top-bar-item logo">
        <amp-img src="<?=  get_bloginfo('template_directory')?>/images/logo.svg" height="100" width="120"></amp-img>
    </div>
    <div class="amp-top-bar-item search " tabindex="1">
        <a href="#search" class="amp-top-bar-item-search-btn"></a>
    </div>
    <div id="search" class="amp-top-bar-search-form">
        <form method="GET" id="searchform" action="<?php echo esc_url( home_url( '/' ) )  ?>"
              target="_top">
                <input type="text" class="data-input" value="" name="s" id="s" placeholder="¿Quiero cocinar?">
                <input type="submit" id="searchsubmit" value="Buscar">
        </form>
    </div>

</div>
