<?php get_header(); ?>
<div id="Container" class="content-area">
    <?php 
    while ( have_posts() ) : the_post();
        $recipe = new WPURP_Recipe($post->ID);
        $categoria = wp_get_post_terms($post->ID, array("category"));
    ?>
    <div id="blog_header" style="background-image: url(<?php echo($recipe->featured_image_url("banner")); ?>);">
        <!-- <p class="termName"> <?php echo($categoria[0]->name); ?></p> -->
        <h1 class="tituloRes"> <?php echo($recipe->title()); ?></h1>
    </div>
    <div id="main" class='blog_single'>
    	<div class="recipe">
            <div class="banner pure-g">
                <div class="breadcrumbs pure-u-1-1 pure-u-md-16-24" typeof="BreadcrumbList" vocab="http://schema.org/">
        			<?php 
                        if(function_exists('bcn_display')){
        			        bcn_display();
        			    }
                    ?>
    			</div>
                <div class="pure-u-1-2 pure-u-md-4-24" >
    	            <div class="info">
                        <img src="<?php echo(get_field('thumbnail_saboreador',get_user_by('id', $post->post_author))["sizes"]["thumb-producto"]);?>"/><div class="Aut_info"><p class="author_name"><?php echo(get_author_name());?></p></div>
    				</div>
                </div>
                <div class="pure-u-1-2 pure-u-md-4-24">
                    <p class="fechaBlog"><?php echo(get_the_date());?></p>
                </div>
            </div>
            <div class="wpurp-container">
                <div class='pure-g'>
                    <div id="blogContent" class='pure-u-1-1 pure-u-md-2-3'>
                        <div class="social">
                            <?php
                                $fav = new WPURP_Favorite_Recipes();
                                echo("<div class='numFab'><span class='fa fa-heart'></span><span class='num'>".$fav->cantidad_favorites($post->ID)."</span></div>");
                                $Fav_button = new WPURP_Template_Recipe_Favorite();
                                echo $Fav_button->output( $recipe );

                                // $Face_button = new WPURP_Template_Facebook();
                                // echo $Face_button->output( $recipe );
                                
                                // $Twi_button = new WPURP_Template_Twitter();
                                // echo $Twi_button->output( $recipe );

                                // $Pin_button = new WPURP_Template_Pinterest();
                                // echo $Pin_button->output( $recipe );
                            ?>
                            <div class='sh'>
                                <p>Comparte en:</p>
                                <?php
                                echo(do_shortcode ('[feather_share skin="wheel"]'));
                                // $print_button = new WPURP_Template_Recipe_Print_Button();
                                // echo $print_button->output( $recipe );
                                echo "<a href='' class='blog_print'>".file_get_contents(get_bloginfo('template_directory')."/images/print.svg")."</a>";
                                $video = $recipe->custom_field('url_video_youtube');
                                ?>
                            </div>
                        </div>
                  		<?php the_content ();?>
                        <div class="social">
                            <?php
                                $fav = new WPURP_Favorite_Recipes();
                                echo("<div class='numFab'><span class='fa fa-heart'></span><span class='num'>".$fav->cantidad_favorites($post->ID)."</span></div>");
                                $Fav_button = new WPURP_Template_Recipe_Favorite();
                                echo $Fav_button->output( $recipe );

                                // $Face_button = new WPURP_Template_Facebook();
                                // echo $Face_button->output( $recipe );
                                
                                // $Twi_button = new WPURP_Template_Twitter();
                                // echo $Twi_button->output( $recipe );

                                // $Pin_button = new WPURP_Template_Pinterest();
                                // echo $Pin_button->output( $recipe );
                            ?>
                            <div class='sh'>
                                <p>Comparte en:</p>
                                <?php
                                echo(do_shortcode ('[feather_share skin="wheel"]'));
                                // $print_button = new WPURP_Template_Recipe_Print_Button();
                                // echo $print_button->output( $recipe );
                                echo "<a href='' class='blog_print'>".file_get_contents(get_bloginfo('template_directory')."/images/print.svg")."</a>";
                                $video = $recipe->custom_field('url_video_youtube');
                                ?>
                            </div>
                        </div>
                        <div class="pure-u-1-1 pure-u-md-1-1" >
                            <div class="info"><img src="<?php echo(get_field('thumbnail_saboreador',get_user_by('id', $post->post_author))["sizes"]["thumb-producto"]);?>"/><div class="Aut_info"><p class="author_name">'<?php echo(get_author_name());?></p><p class="fecha"><?php echo(get_the_date());?></p></div>
                            </div>
                        </div>
              			<div id="comentarios">
    						<?php						
    							// If comments are open or we have at least one comment, load up the comment template.			
    							if (comments_open() || get_comments_number()) {			
    								comments_template();
    							}
    						?>
    					</div> 	
                    </div>
                    <div id="menuLateral" class='pure-u-1-1 pure-u-md-1-3'>
                    <div>   
                      <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" onload="document.myform.reset(); console.log('andres madrigal soto');">
                     
<input type="text" class="bbusqueda" name="s" placeholder="Buscar:" />

                      <input type="hidden" name="post_type" value="blog" /> <!-- // hidden 'products' value -->
                      <input type="submit" class="bboton" alt="Search" value="Buscar" />
                    </form>
                   </div>
                        <?php include  'filtrosBlog.php'; ?>
                    </div> <!-- Fin menuLateral -->
                </div>
            </div>
        </div>
        <?php 	endwhile; ?>
    </div>
</div>
<?php get_footer(); ?>		