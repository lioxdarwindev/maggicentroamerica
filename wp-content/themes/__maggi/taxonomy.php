<?php get_header(); ?>
<div id="Container">

	<?php
	 global $wp_query;

	$slug = $wp_query->query_vars["producto"];
        if($slug == null){
            $slug = $wp_query->query_vars["ingredient"];
            if($slug != null){
                $term = get_term_by( 'slug', $slug, "producto");
                $url = get_term_link($term);
                echo '<input id="product_url" type="hidden" data-url="'.$url.'" />';
            }else{
                $url = home_url();
                echo '<input id="product_url" type="hidden" data-url="'.$url.'" />';
            }
        }
	$term = get_term_by( 'slug', $slug, "producto");	
	$parent_term = get_term( $term->parent, 'producto' );
	//var_dump($parent_term);


	//var_dump($term);
	?>

	<div id="main">
		<div class="pure-g">

			<div class="pure-u-1 pure-u-md-3-5">
				<div id="prodImage">
				<?php
				echo('<img class="" src="'.get_field( 'imagen_producto','producto_'.$term->term_id)["sizes"]["imagen-producto"].'" alt="">');
				?>
			</div>
			<div class="botonera">

					
						<?php 
						for ($i = 2; $i <= 5; $i++) {
						
						$imgP = get_field( 'image_presentacion_'.$i,'producto_'.$term->term_id);
							if($imgP){
								echo('<div class="thumb '.$i.'"><img class="" src="'.$imgP.'" alt=""></div>');
							}
						}
						?>

						
					
					
				</div>
		</div>


		<div id="prodInfo" class="pure-u-1 pure-u-md-2-5">
			<div class="titulo"><h1><?php echo($term->name);?></h1></div>
			<div class="tituloPadre"><h2><?php echo($parent_term->name);?></h2></div>
			<div class="descripcion"><?php echo(get_field( 'descripcion','producto_'.$term->term_id));?></div>
			<div class="infoPresentacion">
				<div>
					<div class="tit">
						Presentación:
					</div>
					<div>
						<?php echo(get_field( 'presentacion','producto_'.$term->term_id));?>
					</div>
				</div>	
				<div>
					<div class="tit">
						<?php echo(get_field( 'titulo_medida','producto_'.$term->term_id));?>
					</div>
					<div>
						<?php echo(get_field( 'medida','producto_'.$term->term_id));?>
					</div>
				</div>
				<div>
					<div class="tit">
						Porciones:
					</div>
					<div>
						<?php echo(get_field( 'porciones','producto_'.$term->term_id));?>
					</div>
				</div>
			</div>
			<h2><a class="infoNutTit" href="#">Información Nutricional<p class="arrow right"></p></a></h2>
			<div class="tablaProd">
			<?php 
			$nombres = ["","","",""];
			$cont = 0;
			$table = get_field( 'informacion_nutricional','producto_'.$term->term_id);

				if ( $table ) {

				    echo '<table border="0">';

				      

				            echo '<thead>';

				                echo '<tr>';

				                    foreach ( $nombres as $th ) {

				                        echo '<th>';
				                            echo $th;
				                        echo '</th>';
				                    }

				                echo '</tr>';

				            echo '</thead>';
				        

				        echo '<tbody>';

				            foreach ( $table['body'] as $tr ) {

				                echo '<tr>';

				                    foreach ( $tr as $td ) {
					                    if($td){	
					                        echo '<td>';
					                            echo $td['c'];
					                        echo '</td>';
					                    }	
				                    }

				                echo '</tr>';
				            }

				        echo '</tbody>';

				    echo '</table>';
				}

			?>
				<p class="notainfonut">(*) Los porcentajes de valores diarios están basados en una dieta recomendada de 2.000 kcal (FDA).</p>
			</div>	
				


			</div>

			<div class="recetasSugeridas pure-u-1">
			<h2 class="recTit">RECETAS SUGERIDAS</h2>
					<?php
						$args = array(
							'DisableFilterPais' => true,
						    'post_type' => 'recipe',
						    'post_status' => 'publish',
						   'posts_per_page' => -1,
						    'tax_query' => array(
						        array(
						            'taxonomy' => 'producto',
						            'field' => 'slug',
						            'terms' => $slug
						        ),
						        array(
						            'taxonomy' => 'pais',
						            'field' => 'slug',
						            'terms' => $_SESSION['pais']
						        )
						    )
						);


						   $custom = new WP_Query( $args );
						 ?>	
						    
						 

				</div>
 							

		</div>

		<div id="recetasGrid" class="grid">
								<?php

									include  'category.php';
								?>
		</div>
		
		</div>

<div id="grupos">
	<?php ///include("productos_interno.php");?>
</div>

	</div>


<?php get_footer(); ?>