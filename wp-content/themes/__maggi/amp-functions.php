<?php

class AmpUtils
{
    public function print_recipe($counter, $recipe, $post, $categoria, $favoriteCount = 0)
    {
        ?>
        <div class="single-recipe <?= ($counter > 2) ? 'list' : ''; ?>">
            <a class="recipe-container" href="<?= get_permalink($post) ?>">

                <div class="recipe-container-frame">
                    <span class="overlay"></span>
                    <amp-img class="recipe-image" layout="responsive"
                             src="<?= $recipe->alternate_image_url("thumbBig") ?>"
                             width="460" height="420" alt="<?=$recipe->title() ?>"></amp-img>
                </div>
                <div class="recipe-container-info">
                    <h4 class="recipe-category"><?= $categoria[0]->name ?></h4>
                    <h3 class="recipe-title"><?= $recipe->title() ?></h3>
                    <div class=" recipe-container-activity">
                        <span class="views"><?= the_views(false) ?> </span>
                        <span class="fabs"><?= $favoriteCount ?></span>
                        <span class="shares"><?= pssc_all($recipe->ID()) ?></span>
                    </div>
                </div>
            </a>

            <div class="recipe-container-author">
                <div class="recipe-author">
                    <div class="recipe-author-thmb">
                        <?php
                        $author_image = $this->get_image_with_alt('thumbnail_saboreador', get_user_by('id', $post->post_author), 'thumb-producto');
                        ?>
                        <amp-img
                                src="<?= $author_image->src ?> "
                                alt="<?= $author_image->alt?>"
                                width="40" height="40"></amp-img>
                    </div>
                    <div class="recipe-author-info">
                        <?php //$author_id = $recipe->get_author_id(); ?>
                        <p class="recipe-author-info-name"><?= get_the_author_meta('display_name', $author_id) ?></p>
                        <p class="recipe-author-info-date"><?= get_the_date() ?></p>
                    </div>
                </div>
                <div class="recipe-duration">
                    <amp-img alt="duracion" src="<?= get_bloginfo('template_directory') ?>/images/icons/clock.svg" width="30"
                             height="27"></amp-img>
                    <span class="recipe-duration-time"><?= $recipe->cook_time() ?> </span>
                    <span class="recipe-duration-time-units">min</span>

                </div>
            </div>
        </div>
        <?php
    }

    public function init()
    {

        //add amp query variables
        function setup_filter_query_vars($query_vars)
        {
            $query_vars[] = 'amp';
            return $query_vars;
        }

        add_filter('template_include', 'amp_page_template', 99);
        function amp_page_template($template)
        {
            global $wp;
            $query_vars = $wp->query_vars;
            if (array_key_exists('amp', $query_vars)) {
                $template = get_template_directory() . '/' . $query_vars['amp'];
            }
            return $template;
        }

        add_action('init', 'amp_page_rewrite');
        function amp_page_rewrite()
        {
            global $wp_rewrite;
            //set up our query variable %test% which equates to index.php?test=
            add_rewrite_tag('%amp%', '([^&]+)');
            //home page
            add_rewrite_rule('^amp\/??', 'index.php?amp=amp-home.php', 'top');
            //recipes page
            add_rewrite_rule('^recipe\/amp\/??', 'index.php?amp=amp-recipes.php&post_type=recipe', 'top');
            // single recipe
            add_rewrite_rule('^recipe\/([^\/]+)(?:\/([0-9]+))?\/amp\/?$', 'index.php?amp=amp-single-recipe.php&post_type=recipe&recipe=$matches[1]&name=$matches[1]', 'top');

            add_rewrite_rule('^productos\/amp\/??', 'index.php?amp=amp-products.php&pagename=productos', 'top');
            add_rewrite_rule('^producto\/(.+?)\/amp\/?$', 'index.php?amp=amp-single-product.php&producto=$matches[1]', 'top');
            //add endpoint, in this case 'test' to satisfy our rewrite rule /test
            add_rewrite_endpoint('amp', EP_PERMALINK | EP_PAGES | EP_ALL | EP_RECIPE);
            //flush rules to get this to work properly (do this once, then comment out)
            $wp_rewrite->flush_rules();
        }
    }

    private function tdrows($elements)
    {
        $str = "";
        foreach ($elements as $element) {
            $str .= $element->nodeValue . ", ";
        }

        return $str;
    }

    private function convert_table_to_array($contents)
    {
        $dom = new DOMDocument;
        $html = $dom->loadHTML($contents);


        //discard white space
        $dom->preserveWhiteSpace = false;

        //the table by its tag name
        $tables = $dom->getElementsByTagName('table');


        //get all rows from the table
        $rows = $tables->item(0)->getElementsByTagName('tr');
        // get each column by tag name
        $cols = $rows->item(0)->getElementsByTagName('th');
        $row_headers = NULL;
        foreach ($cols as $node) {
            //print $node->nodeValue."\n";
            $row_headers[] = $node->nodeValue;
        }

        $table = array();
        //get all rows from the table
        $rows = $tables->item(0)->getElementsByTagName('tr');
        foreach ($rows as $row) {
            // get each column by tag name
            $cols = $row->getElementsByTagName('td');
            $row = array();
            $i = 0;
            foreach ($cols as $node) {
                # code...
                //print $node->nodeValue."\n";
                if ($row_headers == NULL)
                    $row[] = $node->nodeValue;
                else
                    $row[$row_headers[$i]] = $node->nodeValue;
                $i++;
            }
            $table[] = $row;
        }
        return $table;
    }

    public function add_scripts()
    {
        global $wp_query, $post;
        $vars = $wp_query->query_vars;

        ?>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>


        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <?php
        if ($vars['amp'] == 'amp-products.php') {
            ?>
            <script async custom-element="amp-accordion"
                    src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
            <?php
        }

        if ($vars['amp'] == 'amp-products.php' || $vars['amp'] == 'amp-recipes.php') {
            ?>
            <script async custom-element="amp-carousel"
                    src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
            <?php
        }

    }

    public function add_micro_data()
    {
        global $wp_query, $post;
        $vars = $wp_query->query_vars;
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https:" : "http:";
        if ($vars['amp'] == 'amp-single-recipe.php') {
            $recipe = new WPURP_Recipe($post->ID);
            $info_nut = $this->convert_table_to_array(get_field('informacion_nutricional', $recipe->ID));
            $calories = $info_nut[0][1];
            $fat = $info_nut[5][1];
            $fat_trans = [4][1];
            $protein = $info_nut[1][1];
            $carbs = $info_nut[2][1];
            $ingredients = $recipe->ingredients();
            $instructions = $recipe->instructions();

            ?>
            <script type="application/ld+json">
                {
                  "@context": "http://schema.org/",
                  "@type": "Recipe",
                  "name": "<?= $recipe->title() ?> ",
                  "author": "Diana Laurel",
                  "image": "<?= $protocol . get_field('banner_mobile', $post->ID)["sizes"]["banner-mobile"] ?> ",
                  "description": "<?php $recipe->post_content(); ?>",

                  "prepTime": "PT<?= $recipe->cook_time() ?>M",
                  "totalTime": "PT<?= $recipe->cook_time() ?>M",
                  "recipeYield": "<?= $recipe->servings_normalized() ?> porciones",
                  "nutrition": {
                    "@type": "NutritionInformation",
                    "servingSize": "1 Tazón",
                    "calories": "<?= $calories ?> ",
                    "fatContent": "<?= $fat ?>",
                    "carbohydrateContent": "<?= $carbs ?>",
                    "proteinContent": "<?= $protein ?>",
                    "servingSize": "1 Porción",
                    "transFatContent": "<?= $fat_trans ?>"
                  },
                  "recipeIngredient": [
                   <?php
                $i = 0;
                $len = count($ingredients);
                foreach ($ingredients as $ingredient) {
                    $i++;
                    ?>
                            "<?= $ingredient["ingredient"] ?>" <?= ($i != $len) ? ',' : ''; ?>
                    <?php
                }
                ?>
                  ],
                  "recipeInstructions": [
                    <?php
                for ($i = 0; $i < count($instructions); ++$i) {
                    $comma = ($i != count($instructions) - 1) ? ',' : '';
                    echo '"' . $instructions[$i]["description"] . '"' . $comma;
                }
                ?>
                   ]
            }






            </script>


            <?php
        }

        if ($vars['amp'] == "amp-home.php") {
            $url = 'http' . (($_SERVER['HTTPS'] == 'on') ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $url = str_replace('amp/', '', $url);

            ?>
            <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Organization",
              "url": "<?= $url ?>",
              "logo": "<?= get_bloginfo("template_directory") ?>/images/logo.svg",
              "contactPoint": [{
                "@type": "ContactPoint",
                "telephone": "+1-877-746-0909",
                "contactType": "customer service Costa Rica",
                "contactOption": "TollFree",
                "areaServed": "CR"
              },{
                "@type": "ContactPoint",
                "telephone": "+507-800-0000",
                "contactType": "customer service Panama",
                "contactOption": "TollFree",
                "areaServed": "PA"
              },{
                "@type": "ContactPoint",
                "telephone": "+504-800-2220-6666",
                "contactType": "customer service Honduras",
                "contactOption": "TollFree",
                "areaServed": "HN"
              },{
                "@type": "ContactPoint",
                "telephone": "+505-1-800-4000",
                "contactType": "customer service Nicaragua",
                "contactOption": "TollFree",
                "areaServed":"NI"
              },{
                "@type": "ContactPoint",
                "telephone": "+503-800-6179",
                "contactType": "customer service El Salvador",
                "contactOption": "TollFree",
                "areaServed":"SV"
                },{
                "@type": "ContactPoint",
                "telephone": "+502-1-800-2990019",
                "contactType": "customer service Guatemala",
                "contactOption": "TollFree",
                "areaServed": "GT"
              }]
            }


            </script>


            <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Organization",
              "name": "Maggi Centroamerica",
              "url": " <?= $url ?>",
              "sameAs": [
                "https://www.facebook.com/MaggiCentroamerica/",
                "https://www.instagram.com/maggicentroamerica/",
                "https://www.youtube.com/channel/UCla3XXd-Q386Sgr0aNe4rKA"
              ]
            }




            </script>

            <?php
        }

        if ($vars['amp'] == 'amp-single-product.php') {

            $slug = $wp_query->query_vars["producto"];
            $term = get_term_by('slug', $slug, "producto");
            $parent_term = get_term($term->parent, 'producto');
            $product_description = strip_tags(get_field('descripcion', 'producto_' . $term->term_id));
            ?>
            <script type="application/ld+json">
                {
                  "@context": "http://schema.org/",
                  "@type": "Product",
                  "name": "<?= ($term->name); ?>",
                  "image": "<?= get_field('imagen_producto', 'producto_' . $term->term_id)["sizes"]["imagen-producto"] ?>",
                  "description": "<?= $product_description; ?>",
                  "brand": {
                    "@type": "Thing",
                    "name": "Maggi"
                  },
                    "weight": {
                        "@context": "https://schema.org",
                        "@type": "QuantitativeValue",
                        "value": "<?php echo preg_replace("/[^0-9,.]/", "", (get_field('medida', 'producto_' . $term->term_id))); ?>",
                        "unitCode": "G"
                    }
                }


            </script>


            <?php
        }


    }

    public function get_image_with_alt($imagefield, $postID, $imagesize = 'full'){
        $imageID = get_field($imagefield, $postID);
        $src= $imageID['sizes'][$imagesize];
        $alt = $imageID['alt'];
        return $object = (object) ['src' => $src, 'alt'=>$alt];
    }
}

?>