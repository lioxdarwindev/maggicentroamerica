<?php /* Template Name: Favoritas */ ?>
<?php get_header(); ?>
	<div class="content-area">
		<div class="homeCat">
		



	<div id="Container">
	<div id="main">
	<div class="pure-g intruccionesQCH">
	<div class="pure-u-10-24"></div>
	<div class="pure-u-2-24 icon"><img src="<?=  get_bloginfo('template_directory')?>/images/icons/home-icon-qch.svg"/></div>
	<div class="pure-u-10-24"></div>
	<div  class="pure-u-1-1">
		<h2>Recetas Favoritas</h2>
		
	</div>

	</div>
	





	<?php
	$arrayBlog = [];
	$user_id = get_current_user_id();
	   $output .= '<div id="recetasGrid" class="grid">';

	 if( $user_id !== 0 ) {
            $favorites = get_user_meta( $user_id, 'wpurp_favorites', true );
            $favorites = is_array( $favorites ) ? $favorites : array();
           
 		
        
           foreach($favorites as $favorite){
           		   if(get_post_type($favorite) == "blog"){
           		   	$poste = get_post( $favorite );
           				 array_push($arrayBlog, $poste);
           		   }

           }
            $recipes = WPUltimateRecipe::get()->query()->ids( $favorites )->order_by('name')->order('ASC')->get();

            if( count( $favorites ) == 0 || count( $recipes ) == 0 ) {
             
            } else {
           
                foreach ( $recipes as $recipe ) {
                    	

                
					
					 $fav = new WPURP_Favorite_Recipes();
  				
			//print_r($recipe->author());
					
				 $output .= '<a rel ="'.$recipe->ID().'" class="grid-item mix" href="'.$recipe->link().'" data-fav="'.$fav->cantidad_favorites($recipe->ID()).'" data-reciente="'.get_the_date('Ymd').'"><div class="marco"><img class="imagenTemp" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="'.$recipe->alternate_image_url("thumbBig").'" /></div><span> <p class="termName"></p><p class="tituloRes">'.$recipe->title().'</p>
				 	<div class="views_info"><span class="views">'.the_views(false).'</span><span class="fabs">'.$fav->cantidad_favorites($recipe->ID()).'</span>
				 	</div>
				 	</span><div class="info"><img src="'.get_field('thumbnail_saboreador',get_user_by('id', $recipe->get_author_id()))["sizes"]["thumb-producto"].'"/><div class="Aut_info"><p class="author_name">'.$recipe->author().'</p><p class="fecha">'. get_the_date().'</p></div><div class="tiempo pure-u-md-4-24"><img src="'.get_bloginfo('template_directory').'/images/icons/clock.svg">	
            <h2>'.$recipe->cook_time().'</h2><p>min</p></div>
            </div></a>';




               
            }
        }
        
        
	}

	$fav = new WPURP_Favorite_Recipes();
        foreach ($arrayBlog as $poste) {
        	$author_id=$poste->post_author;
    
   	
        
        	//echo(get_field('thumbnail_saboreador',get_user_by('id', $poste->get_author_id()))["sizes"]["thumb-producto"]);
        	$output .= '<a rel ="'.$poste->ID.'" class="grid-item mix" href="'.get_permalink($poste).'"><div class="marco"><img class="imagenTemp" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="'.get_field('thumbnail',$poste->ID)["sizes"]["thumbBig"].'" /></div><span> <p class="termName"></p><p class="tituloRes">'.get_the_title( $poste->ID ).'</p>
				 	<div class="views_info"><span class="views">'.the_views(false).'</span><span class="fabs">'.$fav->cantidad_favorites($post->ID).'</span>
				 	</div>
				 	</span><div class="info"><img src="'.get_field('thumbnail_saboreador',get_user_by('id', $author_id))["sizes"]["thumb-producto"].'"/><div class="Aut_info"><p class="author_name">'.get_the_author_meta( 'display_name', $author_id).'</p><p class="fecha">'. get_the_date().'</p></div>
            </div></a>';
         // echo ($poste->post_title);
        }
	$output .= "</div>";
        echo $output;
	 ?>
	

		
	</div>
	</div>
</div>	

<?php get_footer(); ?>