
		
<?php
/*
Template Name: Password Reset Template by TutorialsTag.com
*/
global $wpdb, $user_ID;

function tg_validate_url() {
	global $post;
	$page_url = esc_url(get_permalink( $post->ID ));
	$urlget = strpos($page_url, "?");
	if ($urlget === false) {
		$concate = "?";
	} else {
		$concate = "&";
	}
	return $page_url.$concate;
}

if (!$user_ID) { //block logged in users

	if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") {
		$reset_key = $_GET['key'];
		$user_login = $_GET['login'];
		$user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));

		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;

		if(!empty($reset_key) && !empty($user_data)) {

			$new_password = wp_generate_password(7, false);
				//echo $new_password; exit();
				wp_set_password( $new_password, $user_data->ID );
				//mailing reset details to the user
			$message = __('Tu nueva contraseña es:') . "\r\n\r\n";
			$message .= get_option('siteurl') . "\r\n\r\n";
			$message .= sprintf(__('Usuario: %s'), $user_login) . "\r\n\r\n";
			$message .= sprintf(__('contraseña: %s'), $new_password) . "\r\n\r\n";
			$message .= __('Puedes ingresar en: ') . get_option('siteurl') . "\r\n\r\n";
			
			if ( $message && !wp_mail($user_email, 'Password Reset Request', $message) ) {
			
				echo "<div id='content' role='main'><div class='recuperar'><div class='error'>El correo no se pudo enviar.</div><div></div>";
		
				exit();
			}
			else {
				get_header();	
				echo("<div id='content' role='main'><div class='recuperar'><p class='pass'>Tu Nueva contraseña:</p>".$new_password."<p>Se envió un correo con sus nuevos datos de ingreso a su correo electrónico.</p><div></div>");
 				get_footer();
				exit();
			}
		} 
		else {
		get_header();
			echo("<div id='content' role='main'><div class='recuperar'><p>Correo dehabilitado, vuelve a intentarlo.<p/></div></div>");
		get_footer();
		exit();
		}
		
	}
	//exit();

	if($_POST['action'] == "tg_pwd_reset"){
		if ( !wp_verify_nonce( $_POST['tg_pwd_nonce'], "tg_pwd_nonce")) {
		  exit("No trick please");
	   }  
		if(empty($_POST['user_input'])) {
			echo("<p>Introduce tu Nombre de usuario o la dirección de correo electrónico que utilizaste para registrarte.</p>");
			
		
		}
		//We shall SQL escape the input
		$user_input = $wpdb->escape(trim($_POST['user_input']));
		
		if ( strpos($user_input, '@') ) {
			$user_data = get_user_by_email($user_input);
			if(empty($user_data) || $user_data->caps[administrator] == 1) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
				echo "<div class='error'>Correo inválido</div>";
				exit();
			}
		}
		else {
			$user_data = get_userdatabylogin($user_input);
			if(empty($user_data) || $user_data->caps[administrator] == 1) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
				echo "<div class='error'>Usuario inválido</div>";
				exit();
			}
		}
		
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;
		
		$key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
		if(empty($key)) {
			//generate reset key
			$key = wp_generate_password(20, false);
			$wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));	
		}
		
		//mailing reset details to the user
		$message = __('Solicitaste recuperar tu contraseña de Maggi.') . "\r\n\r\n";
		
		$message .= sprintf(__('Usuario: %s'), $user_login) . "\r\n\r\n";
		$message .= __('Si esto es un error ignora este mensaje.') . "\r\n\r\n";
		$message .= __('Para recuperar tu contraseña visita:') . "\r\n\r\n";
		$message .= tg_validate_url() . "action=reset_pwd&key=$key&login=" . base64_encode($user_login) . "\r\n";
		
					
		if ( $message && !wp_mail($user_email, 'Password Reset Request', $message) ) {
			

			echo "<div class='error'>El correo no se pudo enviar.</div>";
			exit();
		}
		else {
			echo "<div class='success'>Te enviamos un correo con las instrucciones para recuperar tu contraseña</div>";
			exit();
		}
		
		
	} else { 

get_header(); ?>
<div id="content" role="main">
	<div class="recuperar">
	<?php if ( have_posts() ) : ?>
	
		<?php while ( have_posts() ) : the_post(); ?>
			
			<form class="user_form" id="wp_pass_reset" action="" method="post">
			<p>Introduce tu Nombre de usuario o la dirección de correo electrónico que utilizaste para registrarte.</p>	
			<input type="text" class="text" name="user_input" value="" />
			<input type="hidden" name="action" value="tg_pwd_reset" />
			<input type="hidden" name="tg_pwd_nonce" value="<?php echo wp_create_nonce("tg_pwd_nonce"); ?>" />
			<input type="submit" id="submitbtn" class="reset_password wpcf7-form-control wpcf7-submit" name="submit" value="Recuperar" />					
			</form>
			<div id="result"></div> <!-- To hold validation results -->
			<script type="text/javascript">  
			jQuery(document).ready(function($) {
				$("#wp_pass_reset").submit(function() {
				$('#result').html('<span class="loading">Validating...</span>').fadeIn();
				var input_data = $('#wp_pass_reset').serialize();
				$.ajax({
				type: "POST",
				url:  "<?php echo get_permalink( $post->ID ); ?>",
				data: input_data,
				success: function(msg){
				$('.loading').remove();
				$('<div>').html(msg).appendTo('div#result').hide().fadeIn('slow');
				}
				});
				return false;
				});
			});
				</script>
			
	<?php endwhile; ?>
		
	<?php else : ?>
		
			<h2><?php _e('Not Found'); ?></h1>
			
	<?php endif; ?>
	</div>
</div><!-- content -->
<?php get_footer(); ?>
<?php


	}
}
else {
	wp_redirect( home_url() ); exit;
	//redirect logged in user to home page
}
?>
	 </div>
</div>
