<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>


		

		<ul class="comment-list">
			<?php
			 // $recipe = new WPURP_Recipe($post->ID); 
				
				$comments = get_comments(array(
				'post_id' => $recipe->ID,
				'status' => 'approve' //Change this to the type of comments to be displayed
		));

				//Display the list of comments
				wp_list_comments(array(
					'per_page' => 10, //Allow comment pagination
					'reverse_top_level' => false //Show the latest comments at the top of the list
				), $comments);
			?>
		</ul><!-- .comment-list -->

		

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comentarios Cerrados.' ); ?></p>
	<?php endif; ?>

	<?php 
comment_form( array( 'logged_in_as' => '' , 'label_submit' => 'AÑADIR COMENTARIO'  ) , 1);

	 ?>

</div><!-- .comments-area -->
