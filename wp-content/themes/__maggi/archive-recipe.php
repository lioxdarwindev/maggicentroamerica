<?php get_header(); ?>
<div id="Container">
	<div id="main">

	<div id="banner" class="banner-recipes">

		<span class="hid"> 
			<span class="banrep">
				<img class="desktop owl-lazy imgbanner" data-src="http://www.maggicentroamerica.com/newsite/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-1-960x150.png"/>
				<img class="mobile owl-lazy imgbanner" data-src="http://www.maggicentroamerica.com/newsite/wp-content/uploads/2017/02/Ensalada-de-pollo-y-vinagreta-especial-maggi-2-267x150.jpg"/>
				<span class="overl">	
				</span>
				<img class="owl-lazy ornament" data-src="<?php echo get_bloginfo('template_directory').'/images/ornamento.png' ?>">
			</span>
					
		</span>

	</div>
		<div id="categorias" class="pure-g">
			<div class="pure-u-1-1 pure-u-md-1-1 revisaMensaje">
				<h3 class="boletinTitle">ENCUENTRA AQUÍ DELICIOSAS RECETAS <br> QUE VAN CON TU COCINA</h3>
			</div>
			<?php 

			$cat = $_GET['cat'];
			$mainCategories = get_terms( 'category', array( 'parent' => 0  , 'hide_empty' => false , 'exclude' => '1, 390, 391, 392, 393') );
			?>


			<?php
			//echo '<div class="pure-u-1-2 pure-u-md-1-6"><a class="post-link cat_link"  href="#" rel=""><img src="'.get_bloginfo('template_directory').'/images/flecha_cat.svg"/><p>Categorías</p></a></div>';
			foreach( $mainCategories as $category ) {
				

					 $term_link = get_term_link( $category );
					 $activo = "";
				
						if($cat == $category->slug){

							$activo = "activo";
						}
				 	echo '<div class="pure-u-1-2 pure-u-md-1-5"><a id="'.$category->slug.'" class="post-link '.$activo.'"  href="#" rel="'.esc_url( $term_link ) .'">'.file_get_contents(get_bloginfo('template_directory')."/images/".$category->slug.".svg").'<p>' . $category->name . '</p></a></div>';
			
			}
			?>
			
		</div>
		<div id="subcategorias" class="controls">
			<?php 
			
			
			foreach( $mainCategories as $category ) {
				//var_dump($category->term_id);
				echo '<div class="sub-'.$category->slug.'">';

				
				 $subcategories = get_terms( 'category', array( 'parent' => $category->term_id, 'hide_empty' => true ) );
				
				foreach( $subcategories as $subcategory ) {
				 echo( '<span class="filter" data-filter=".'.$subcategory->slug.'">'.$subcategory->name.'</span>');
			
				}
				// var_dump($subcategory);
				echo '</div>';
				 	//echo '<a href="#" rel="' . $category->slug . '">' . $category->name . '</a>';
			}
			?>

		</div>	

			<a class="busquedaAvanzadaLabel" href="#">Búsqueda Avanzada <p class="arrow down"></p></a>
		<div id="busquedaAvanzada" class="pure-g abierto">

		<div class="pure-u-1 pure-u-md-1-2">
			<!-- SORT -->
			<img class="corazon" src="<?php echo(get_bloginfo('template_directory'));?>/images/icons/corazon.svg">
			<div class="wrapper-dropdown wrapper-sort" tabindex="1">

				<span>Búsqueda Por:</span>
				  <ul class="dropdown">
				  		<li><a rel="reciente:desc" href="#">Recientes</a></li>
						<li><a rel="fav:desc" href="#">Populares</a></li>
				   </ul>
			</div>
		</div>	
		<div class="pure-u-1 pure-u-md-1-2">
			<!-- ingredientes -->
			<img class="corazon" src="<?php echo(get_bloginfo('template_directory'));?>/images/icons/ingredientes.svg">
			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">
				<span class="ingredientesTit">Ingredientes</span>
				  <ul class="dropdown">
				   <li> <a data-clase="ingredientes" rel="" href="#">Todos</a></li>
						<?php 
							$uniquePids = array_unique(array_map(function ($i) { return $i['group']; }, WPURP_Taxonomy_MetaData::get( 'ingredient',"", 'group' )));

					sort($uniquePids, SORT_NATURAL | SORT_FLAG_CASE);
						foreach($uniquePids as $term) {
							
						
						
							if($term){

								echo('<li> <a data-clase="ingredientes" rel=".'.preg_replace('/\s+/', '',$term).'" href="#">'.$term.'</a></li>');
							}
						}
						
						?>
				   </ul>
			</div>
		</div>	

		<div class="pure-u-1 pure-u-md-1-2">
		<img class="corazon" src="<?php echo(get_bloginfo('template_directory'));?>/images/icons/tiempos.svg">
			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">

				<span class="tiempoTit">Tiempos de Comida</span>
				  <ul class="dropdown">
				  <li> <a data-clase="tiempo" rel="" href="#">Todos</a></li>
						<?php 
						$persTerm = get_terms( 'momento', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="tiempo" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
			</div>
		</div>

		<div class="pure-u-1 pure-u-md-1-2">
		<img class="corazon" src="<?php echo(get_bloginfo('template_directory'));?>/images/icons/dificultad.svg">
			<div  class="wrapper-dropdown wrapper-filter" tabindex="1">
			
				<span class="dificultadTit">Dificultad</span>
				  <ul class="dropdown">
				   <li> <a data-clase="dificultad" rel="" href="#">Todas</a></li>
						<?php 
						$persTerm = get_terms( 'course', array(
						    'hide_empty' => true,
						) );
						foreach($persTerm as $term) {
							echo('<li> <a data-clase="dificultad" rel=".'.$term->slug.'" href="#">'.$term->name.'</a></li>');
						}
						?>
				   </ul>
			</div>
		</div>
			
		</div>
					


		<div id="recetasGrid" class="grid">

		<?php

			include  'category.php';
		?>
		</div>
		<div class="pager-list">
						 <!-- Pagination buttons will be generated here -->
		</div>
		<div class="after-pager-line"></div>
		<div id="categorias" class="pure-g">
			<div class="pure-u-1-1 pure-u-md-1-1 revisaMensaje">
				<h3 class="boletinTitle">BUSCAR POR CATEGORÍAS</h3>
			</div>
			<?php 

			$cat = $_GET['cat'];
			$mainCategories = get_terms( 'category', array( 'parent' => 0  , 'hide_empty' => false , 'exclude' => '1, 390, 391, 392, 393') );
			?>


			<?php
			//echo '<div class="pure-u-1-2 pure-u-md-1-6"><a class="post-link cat_link"  href="#" rel=""><img src="'.get_bloginfo('template_directory').'/images/flecha_cat.svg"/><p>Categorías</p></a></div>';
			foreach( $mainCategories as $category ) {
				

					 $term_link = get_term_link( $category );
					 $activo = "";
				
						if($cat == $category->slug){

							$activo = "activo";
						}
				 	echo '<div class="pure-u-1-2 pure-u-md-1-5"><a id="'.$category->slug.'-2" class="post-link '.$activo.'"  href="#" rel="'.esc_url( $term_link ) .'">'.file_get_contents(get_bloginfo('template_directory')."/images/".$category->slug.".svg").'<p>' . $category->name . '</p></a></div>';
			
			}
			?>
			
		</div>

		<?php
		// while ( have_posts() ) : the_post();
		// 	echo($post->post_title);
		// 	//var_dump( wp_get_post_terms( $post->ID, "recipe_description" ));
		// 	 $recipe = new WPURP_Recipe($post->ID);
			 
		// 	 foreach( $recipe->ingredients() as $ingredient ) {
		// 	// 	var_dump($ingredient);
		// 	 }
			
		// endwhile;
		?>
	</div>
	
</div>
<?php get_footer(); ?>