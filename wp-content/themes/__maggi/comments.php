<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
	

		

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 56,
				) );
			?>
		</ul><!-- .comment-list -->

		

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comentarios Cerrados.' ); ?></p>
	<?php endif; ?>

<?php comment_form( 
	array( 
		'logged_in_as' => '' , 
		'label_submit' => 'AÑADIR COMENTARIO',
		'comment_field' => '<p class="comment-form-comment"><label for="comment">Comentario *</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>'
		) 
	); 
?>

</div><!-- .comments-area -->
