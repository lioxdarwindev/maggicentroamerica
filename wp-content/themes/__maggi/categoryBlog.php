				<?php
				// Start the Loop
				$catId = $wp_query->query_vars["cat"];
				$recIndex = 0;
				$arrIngredientes = array();




				if(!$custom){
					
					$custom = $wp_query;
				}

				while ( $custom->have_posts() ) : $custom->the_post();

				$terms = wp_get_post_terms($post->ID,array("category","pais","personalidad","momento","course"));
				$categoria = wp_get_post_terms($post->ID,array("category"));
				$termslugs = '';
				
				foreach($terms as $term) {
					
					$termslugs .= " " . $term->slug;
					 $content = get_the_content();
					
					 if($term->parent == $catId){
					 	$catTitle = $term->name;

					 }
					 //term_is_ancestor_of($catId)
					 
					 		
				}



				//global $wp_query;
					
					
					$recipe = new WPURP_Recipe($post->ID);

			
					foreach( $recipe->ingredients() as $ingredient ) {
                    	$slug = get_term( $ingredient["ingredient_id"],"ingredient")->slug;
                    	$name = get_term( $ingredient["ingredient_id"],"ingredient")->name;

                        $termslugs .= " " .$slug ;
                        // logica para dejar ingredientes en filtros de recetas visibles
                       $arrIngredientes[$slug]["nombre"] = [$name];
                       $arrIngredientes[$slug]["slug"] = [$slug];
                   	   $arrIngredientes[$slug]["ids"][$recipe->ID()] = 1;
                      

                	}
                
					
					$fav = new WPURP_Favorite_Recipes();
  				
					//userphoto(get_user_by('id', $post->post_author)); //la imagen
					//userphoto_thumbnail(get_user_by('id', $post->post_author));// el thumb
			
				 echo('<a rel ="'.$post->ID.'" class="grid-item mix" href="'.get_permalink($post).'"/><div class="marco"><img class="imagenTemp" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="'.get_field('thumbnail',$post->ID)["sizes"]["thumbBig"].'" /></div><span> <p class="termName"></p><p class="tituloRes">'.get_the_title( $post->ID ).'</p>
				 	<div class="views_info"><span class="views">'.the_views(false).'</span><span class="fabs">'.$fav->cantidad_favorites($post->ID).'</span>
				 	</div>
				 	</span><div class="info"><img src="'.get_field('thumbnail_saboreador',get_user_by('id', $author_id))["sizes"]["thumb-producto"].'"/><div class="Aut_info"><p class="author_name">'.get_the_author_meta( 'display_name', $author_id).'</p><p class="fecha">'. get_the_date().'</p></div>
            </div></a>');

				 	$recIndex ++;

	
				endwhile;


				//print_r($arrIngredientes);
				?>
				<script type="text/javascript">
				// pass PHP variable declared above to JavaScript variable
				var arIng = <?php echo json_encode($arrIngredientes) ?>;
				</script>